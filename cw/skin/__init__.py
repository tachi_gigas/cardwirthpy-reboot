#!/usr/bin/env python
# -*- coding: utf-8 -*-

from . import convert
from . import util
from . import win32res

__all__ = ["convert", "util", "win32res"]


def main() -> None:
    pass


if __name__ == "__main__":
    main()
