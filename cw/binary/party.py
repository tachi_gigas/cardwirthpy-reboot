#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import io
import sys

from . import base
from . import adventurer
from . import album

import cw

import typing
from typing import Dict, Iterable, List, Literal, Optional, Tuple, Union


class Party(base.CWBinaryBase):
    """wplファイル(type=2)。パーティの見出しデータ。
    パーティの所持金や名前はここ。
    宿の画像も格納しているが必要ないと思うので破棄。
    F9のためにゴシップと終了印を記憶しているような事は無い
    (その2つはF9で戻らない)。
    """
    def __init__(self, parent: None, f: "cw.binary.cwfile.CWFile", yadodata: bool = False,
                 dataversion: int = 10) -> None:
        from . import util

        base.CWBinaryBase.__init__(self, parent, f, yadodata)
        self.type = 2
        self.fname = self.get_fname()
        if 10 <= dataversion:
            # 1.28以降
            _ = f.word()  # 不明(0)
            _ = f.string()  # 宿名
            f.image()  # 宿の埋め込み画像は破棄
            self.memberslist = []
            for member in cw.util.decodetextlist(f.string(True)):
                if member != "":
                    self.memberslist.append(util.check_filename(member))
            self.name = f.string()
            self.money = f.dword()  # 冒険中の現在値
            self.nowadventuring = f.boolean()
        else:
            # 1.20
            self.memberslist = []
            for member in cw.util.decodetextlist(f.string(True)):
                if member != "":
                    self.memberslist.append(util.check_filename(member))
            _ = f.string()  # データバージョン
            _ = f.string()  # プレイ中のシナリオ名
            f.image()  # 宿の埋め込み画像は破棄
            self.name = ""
            self.money = 0
            self.nowadventuring = f.boolean()

        # 読み込み後に操作
        self.cards: List[BackpackCard] = []
        # データの取得に失敗したカード。変換時に追加する
        self.errorcards: List[BackpackCard] = []

        self.data: Optional[cw.data.CWPyElement] = None

    def get_data(self) -> "cw.data.CWPyElement":
        if self.data is None:
            self.data = cw.data.make_element("Party")

            prop = cw.data.make_element("Property")

            e = cw.data.make_element("Name", self.name)
            prop.append(e)
            e = cw.data.make_element("Money", str(self.money))
            prop.append(e)

            me = cw.data.make_element("Members")
            # メンバーの追加はwpt側で
            prop.append(me)

            self.data.append(prop)

        return self.data

    def create_xml(self, dpath: str) -> str:
        path = base.CWBinaryBase.create_xml(self, dpath)
        root = self.get_root()
        assert root
        yadodb = root.yadodb
        if yadodb:
            yadodb.insert_party(path, commit=False)

        # 荷物袋内のカード
        cdpath = os.path.dirname(path)
        carddb = cw.yadodb.YadoDB(cdpath, cw.yadodb.PARTY)
        self.errorcards = []
        order = 0
        for card in self.cards:
            if card.data:
                card.data.materialbasedir = dpath
                cpath = card.create_xml(cdpath)
                carddb.insert_card(cpath, commit=False, cardorder=order)
                order += 1
            else:
                self.errorcards.append(card)
        carddb.commit()
        carddb.close()

        return path

    @staticmethod
    def unconv(f: "cw.binary.cwfile.CWFileWriter", data: "cw.data.CWPyElement",
               yadoname: str, membertbl: Dict[str, str], scenarioname: str) -> None:
        if scenarioname:
            yadoname = scenarioname
            nowadventuring = True
        else:
            nowadventuring = False
        imgpath = "Resource/Image/Card/COMMAND0"
        imgpath = cw.util.find_resource(cw.util.join_paths(cw.cwpy.skindir, imgpath), cw.cwpy.rsrc.ext_img)
        image = base.CWBinaryBase.import_image(f, imgpath, fullpath=True)
        memberslist = ""
        name = ""
        money = 0

        for e in data:
            if e.tag == "Property":
                for prop in e:
                    if prop.tag == "Name":
                        name = prop.text
                    elif prop.tag == "Money":
                        money = int(prop.text)
                        money = cw.util.numwrap(money, 0, 999999)
                    elif prop.tag == "Members":
                        seq = []
                        for me in prop:
                            if me.tag == "Member" and me.text and me.text in membertbl:
                                seq.append(membertbl[me.text])
                        memberslist = cw.util.encodetextlist(seq)

        f.write_word(0)  # 不明
        f.write_string(yadoname)
        f.write_image(image)
        f.write_string(memberslist, True)
        f.write_string(name)
        f.write_dword(money)
        f.write_bool(nowadventuring)


class PartyMembers(base.CWBinaryBase):
    """wptファイル(type=3)。パーティメンバと
    荷物袋に入っているカードリストを格納している。
    """
    def __init__(self, parent: None, f: "cw.binary.cwfile.CWFile", yadodata: bool = False,
                 dataversion: int = 10) -> None:
        from . import adventurer
        from . import summary
        from . import skill
        from . import item
        from . import beast
        from . import bgimage

        base.CWBinaryBase.__init__(self, parent, f, yadodata)
        self.type = 3
        self.fname = self.get_fname()
        if 10 <= dataversion:
            adventurers_num = f.byte() - 30
        else:
            adventurers_num = f.byte() - 10
        _ = f.byte()  # 不明(0)
        _ = f.byte()  # 不明(0)
        _ = f.byte()  # 不明(0)
        _ = f.byte()  # 不明(5)
        self.adventurers = []
        vanisheds_num = 0
        for i in range(adventurers_num):
            self.adventurers.append(adventurer.AdventurerWithImage(self, f))
            if 10 <= dataversion:
                vanisheds_num = f.byte()  # 最後のメンバが消滅メンバの数を持っている？
            else:
                _ = f.byte()  # 不明(0)
        self.vanisheds = []
        if 0 < vanisheds_num:
            _ = f.dword()  # 不明(0)
            for i in range(vanisheds_num):
                self.vanisheds.append(adventurer.AdventurerWithImage(self, f))
                if i + 1 < vanisheds_num:
                    _ = f.byte()
            self.vanisheds.reverse()
        else:
            _ = f.byte()  # 不明(0)
            _ = f.byte()  # 不明(0)
            _ = f.byte()  # 不明(0)
        if 10 <= dataversion:
            # 1.28以降
            self.name = f.string()  # パーティ名
            # 荷物袋にあるカードリスト
            cards_num = f.dword()
            self.cards = [BackpackCard(self, f) for _cnt in range(cards_num)]
        else:
            # 1.20
            f.seek(-4, io.SEEK_CUR)
            # パーティ名
            self.name = self.adventurers[0].adventurer.name + "一行"
            # 荷物袋にあるカードリスト
            cards_num = f.dword()
            self.cards = []
            for _cnt in range(cards_num):
                ctype = f.byte()
                if ctype == 2:
                    carddata: Union[item.ItemCard, skill.SkillCard, beast.BeastCard] = item.ItemCard(None, f, True)
                elif ctype == 1:
                    carddata = skill.SkillCard(None, f, True)
                elif ctype == 3:
                    carddata = beast.BeastCard(None, f, True)
                else:
                    raise ValueError(self.fname)
                card = BackpackCard(self, None)
                card.fname = carddata.name
                if ctype in (2, 3):
                    card.uselimit = carddata.limit
                else:
                    card.uselimit = 0
                # F9で戻るカードかどうかはレアリティの部分に格納されているため処理不要
                card.mine = True
                card.set_data(carddata)
                self.cards.append(card)

        # 対応する *.wpl
        self.wpl: Optional[Party] = None

        if 10 <= dataversion:
            # *.wplにもあるパーティの所持金(冒険中の現在値)
            self.money = f.dword()

            # ここから先はプレイ中のシナリオの状況が記録されている
            self.money_beforeadventure = f.dword()  # 冒険前の所持金。冒険中でなければ0
            self.nowadventuring = f.boolean()
            if self.nowadventuring:  # 冒険中か
                _ = f.word()  # 不明(0)
                self.scenariopath = f.rawstring()  # シナリオ
                self.areaid = f.dword()
                self.steps = self.split_variables(f.rawstring(), True)
                self.flags = self.split_variables(f.rawstring(), False)
                self.friendcards = self.split_ids(f.rawstring())
                self.infocards = self.split_ids(f.rawstring())
                self.music = f.rawstring()
                bgimgs_num = f.dword()
                self.bgimgs = [bgimage.BgImage(self, f) for _cnt in range(bgimgs_num)]
        else:
            # 1.20以前では個人別に所持金があるためパーティの財布に集める
            self.money = 0
            for adv in self.adventurers:
                self.money += adv.adventurer.money
                adv.adventurer.money = 0
            self.money_beforeadventure = self.money  # 1.20ではF9で所持金が戻らない

            self.nowadventuring = f.boolean()
            if self.nowadventuring:  # 冒険中か
                self.scenariopath = ""
                summarydata = summary.Summary(None, f, True, wpt120=True)
                self.steps = {}
                for step in summarydata.steps:
                    self.steps[step.name] = step.default
                self.flags = {}
                for flag in summarydata.flags:
                    self.flags[flag.name] = flag.default
                self.scenariopath = f.rawstring()  # シナリオ
                if not os.path.isabs(self.scenariopath):
                    filename = f.filename
                    dpath = os.path.dirname(os.path.dirname(os.path.dirname(filename)))
                    self.scenariopath = cw.util.join_paths(dpath, self.scenariopath)
                self.areaid = f.dword()
                self.friendcards = []
                fcardnum = f.dword()
                for _i in range(fcardnum):
                    self.friendcards.append(f.dword())
                self.infocards = []
                infonum = f.dword()
                for _i in range(infonum):
                    self.infocards.append(f.dword())
                self.music = f.rawstring()
                bgimgs_num = f.dword()
                self.bgimgs = [bgimage.BgImage(self, f) for _cnt in range(bgimgs_num)]

    @typing.overload
    def split_variables(self, text: str, step: Literal[True]) -> Dict[str, int]: ...

    @typing.overload
    def split_variables(self, text: str, step: Literal[False]) -> Dict[str, bool]: ...

    def split_variables(self, text: str, step: bool) -> Union[Dict[str, int], Dict[str, bool]]:
        d = {}
        for ln in text.splitlines():
            index = ln.rfind('=')
            if index != -1:
                if step:
                    d[ln[:index]] = int(ln[index+1:])
                else:
                    d[ln[:index]] = bool(int(ln[index+1:]))
        return d

    def split_ids(self, text: str) -> List[int]:
        seq = []
        for ln in text.splitlines():
            if ln:
                seq.append(int(ln))
        return seq

    def create_xml(self, dpath: str) -> str:
        """adventurercardだけxml化する。"""
        assert self.wpl
        wpldata = self.wpl.get_data()
        me = wpldata.find_exists("Property/Members")
        for adv in self.adventurers:
            path = adv.create_xml(dpath)
            text = cw.util.splitext(os.path.basename(path))[0]
            me.append(cw.data.make_element("Member", text))
        return wpldata.fpath

    def create_vanisheds_xml(self, dpath: str) -> None:
        for adv in self.vanisheds:
            data = adv.get_data()
            data.find_exists("Property").set("lost", "True")
            adv.create_xml(dpath)

    @staticmethod
    def join_variables(data: Iterable["cw.data.CWPyElement"]) -> str:
        seq = []
        for e in data:
            name = e.text
            value = e.get("value")
            if value == "True":
                value = "1"
            elif value == "False":
                value = "0"
            seq.append(name + "=" + value)
        if seq:
            seq.append("")
        return "\r\n".join(seq)

    @staticmethod
    def join_ids(data: Iterable["cw.data.CWPyElement"]) -> str:
        seq = []
        for e in data:
            if e.tag == "CastCard":
                seq.append(e.find_exists("Property/Id").text)
            else:
                seq.append(e.text)
        if seq:
            seq.append("")
        return "\r\n".join(seq)

    @staticmethod
    def unconv(f: "cw.binary.cwfile.CWFileWriter", party: "cw.data.Party",
               yadocards: Dict[str, Tuple[str, "cw.data.CWPyElement"]], logdir: str) -> None:
        from . import cwfile
        from . import adventurer
        from . import bgimage

        adventurers = []
        vanisheds = []
        money_beforeadventure = 0
        nowadventuring = False
        scenariopath = ""
        areaid = 0
        steps = ""
        flags = ""
        friendcards = ""
        infocards = ""
        music = ""
        bgimgs: Optional[cw.data.CWPyElement] = None

        for e_member in party.members:
            adventurers.append(e_member.find_exists("."))
        name = party.name

        if logdir:
            # プレイ中のシナリオの状況
            e_log = cw.data.xml2etree(cw.util.join_paths(logdir, "ScenarioLog.xml"))
            e_party = cw.data.xml2etree(cw.util.join_paths(logdir, "Party/Party.xml"))
            money_beforeadventure = e_party.getint("Property/Money", party.money)
            money_beforeadventure = cw.util.numwrap(money_beforeadventure, 0, 999999)
            nowadventuring = True
            scenariopath = e_log.gettext("Property/WsnPath", "")
            areaid = e_log.getint("Property/AreaId", 0)
            steps = PartyMembers.join_variables(e_log.getfind("Steps"))
            flags = PartyMembers.join_variables(e_log.getfind("Flags"))
            friendcards = PartyMembers.join_ids(e_log.getfind("CastCards"))
            infocards = PartyMembers.join_ids(e_log.getfind("InfoCards"))
            music = e_log.gettext("Property/MusicPath", "")
            if not music:
                music = e_log.gettext("Property/MusicPaths/MusicPath", "")
            bgimgs = e_log.find("BgImages")

            for e in e_log.getfind("LostAdventurers"):
                path = cw.util.join_yadodir(e.text)
                vanisheds.append(cw.data.xml2element(path))
            vanisheds.reverse()

        advnumpos = f.tell()
        advnum = 0
        f.write_byte(len(adventurers) + 30)
        f.write_byte(0)  # 不明
        f.write_byte(0)  # 不明
        f.write_byte(0)  # 不明
        f.write_byte(5)  # 不明
        errorlog = []
        for i, member in enumerate(adventurers):
            if logdir:
                fpath = cw.util.join_paths(logdir, "Members", os.path.basename(member.fpath))
                logdata: Optional[cw.data.CWPyElement] = cw.data.xml2element(fpath)
            else:
                logdata = None
            try:
                pos = f.tell()
                adventurer.AdventurerWithImage.unconv(f, member, logdata)
                if i + 1 < len(adventurers):
                    f.write_byte(0)  # 不明
                advnum += 1
            except cwfile.UnsupportedError as ex:
                f.seek(pos)
                if f.write_errorlog:
                    cardname = member.gettext("Property/Name", "")
                    s = "%s の %s は対象エンジンで使用できない機能(%s)を使用しているため、変換しません。\n" % (name, cardname, ex.funcname)
                    errorlog.append(s)
            except Exception:
                cw.util.print_ex(file=sys.stderr)
                f.seek(pos)
                if f.write_errorlog:
                    cardname = member.gettext("Property/Name", "")
                    s = "%s の %s は変換できませんでした。\n" % (name, cardname)
                    errorlog.append(s)

        if advnum == 0:
            s = "%s は全メンバが変換に失敗したため、変換しません。\n" % (name)
            raise cwfile.UnsupportedError(s)

        if f.write_errorlog:
            for s in errorlog:
                f.write_errorlog(s)

        tell = f.tell()
        f.seek(advnumpos)
        f.write_byte(advnum + 30)
        f.seek(tell)

        vannumpos = f.tell()
        vannum = 0
        f.write_byte(len(vanisheds))  # 消滅メンバの数？
        if vanisheds:
            f.write_dword(0)  # 不明
            for i, member in enumerate(vanisheds):
                if logdir:
                    fpath = cw.util.join_paths(logdir, "Members", os.path.basename(member.fpath))
                    logdata = cw.data.xml2element(fpath)
                else:
                    logdata = None
                try:
                    pos = f.tell()
                    adventurer.AdventurerWithImage.unconv(f, member, logdata)
                    if i + 1 < len(vanisheds):
                        f.write_byte(0)  # 不明
                    vannum += 1
                except cwfile.UnsupportedError as ex:
                    f.seek(pos)
                    if f.write_errorlog:
                        cardname = member.gettext("Property/Name", "")
                        s = "%s の %s(消去前データ) は対象エンジンで使用できない機能(%s)を使用しているため、変換しません。\n" % (name, cardname, ex.funcname)
                        f.write_errorlog(s)
                except Exception:
                    cw.util.print_ex(file=sys.stderr)
                    f.seek(pos)
                    if f.write_errorlog:
                        cardname = member.gettext("Property/Name", "")
                        s = "%s の %s(消去前データ) は変換できませんでした。\n" % (name, cardname)
                        f.write_errorlog(s)
            tell = f.tell()
            f.seek(vannumpos)
            f.write_byte(vannum)
            f.seek(tell)

        else:
            f.write_byte(0)  # 不明
            f.write_byte(0)  # 不明
            f.write_byte(0)  # 不明
        f.write_string(name)

        backpacknumpos = f.tell()
        backpacknum = 0
        f.write_dword(len(party.backpack))
        # CardWirthでは削除されたカードはF9でも復活しないので変換不要
        for header in party.backpack:
            # バージョン不一致で不変換のデータもあるので所在チェック
            if header.fpath in yadocards:
                fpath, data = yadocards[header.fpath]
                scenariocard = cw.util.str2bool(data.get("scenariocard", "False"))
                BackpackCard.unconv(f, data, fpath, not scenariocard)
                backpacknum += 1
        tell = f.tell()
        f.seek(backpacknumpos)
        f.write_dword(backpacknum)
        f.seek(tell)

        f.write_dword(cw.util.numwrap(party.money, 0, 999999))  # パーティの所持金(現在値)

        # プレイ中のシナリオの状況
        if nowadventuring:
            f.write_dword(money_beforeadventure)
            f.write_bool(nowadventuring)
            f.write_word(0)  # 不明(0)
            f.write_rawstring(os.path.abspath(scenariopath))
            f.write_dword(areaid)
            f.write_rawstring(steps)
            f.write_rawstring(flags)
            f.write_rawstring(friendcards)
            f.write_rawstring(infocards)
            f.write_rawstring(music)
            if bgimgs is None:
                f.write_dword(0)
            else:
                f.write_dword(len(bgimgs))
                for bgimg in bgimgs:
                    bgimage.BgImage.unconv(f, bgimg)
        else:
            f.write_dword(0)
            f.write_bool(False)


class BackpackCard(base.CWBinaryBase):
    """荷物袋に入っているカードのデータ。
    self.dataにwidファイルから読み込んだカードデータがある。
    """
    from . import skill
    from . import item
    from . import beast

    def __init__(self, parent: PartyMembers, f: Optional["cw.binary.cwfile.CWFile"], yadodata: bool = False) -> None:
        from . import skill
        from . import item
        from . import beast

        base.CWBinaryBase.__init__(self, parent, f, yadodata)
        if f:
            self.fname = f.rawstring()
            self.uselimit = f.dword()
            self.mine = f.boolean()
        else:
            self.fname = ""
            self.uselimit = 0
            self.mine = False
        self.data: Optional[Union[skill.SkillCard, item.ItemCard, beast.BeastCard]] = None

    def set_data(self, data: Union["skill.SkillCard", item.ItemCard, beast.BeastCard]) -> None:
        """widファイルから読み込んだカードデータを関連づける"""
        self.data = data

    def get_data(self) -> "cw.data.CWPyElement":
        assert self.data is not None
        return self.data.get_data()

    def create_xml(self, dpath: str) -> str:
        """self.data.create_xml()"""
        assert self.data is not None
        self.data.limit = self.uselimit
        if not self.mine:
            self.data.set_image_export(False, True)
        data = self.data.get_data()
        if not self.mine:
            data.set("scenariocard", "True")
        return self.data.create_xml(dpath)

    @staticmethod
    def unconv(f: "cw.binary.cwfile.CWFileWriter", data: "cw.data.CWPyElement", fname: str, mine: bool) -> None:
        f.write_rawstring(cw.util.splitext(fname)[0])
        f.write_dword(data.getint("Property/UseLimit", 0))
        f.write_bool(mine)


def load_album120(parent: None,
                  f: "cw.binary.cwfile.CWFile") -> Tuple[List[adventurer.AdventurerCard], List[album.Album]]:
    _ = f.dword()  # 不明
    cardnum = f.dword()  # アルバム人数
    cards = []
    albums = []
    for _i in range(cardnum):
        card = adventurer.AdventurerCard(parent, None, True)
        card.fname = f.filename
        card.adventurer = adventurer.Adventurer(card, f, True, album120=True)
        if card.adventurer.is_dead:
            albumdata = album.Album(parent, None, True)
            albumdata.name = card.adventurer.name
            albumdata.image = card.adventurer.image
            albumdata.level = card.adventurer.level
            albumdata.dex = card.adventurer.dex
            albumdata.agl = card.adventurer.agl
            albumdata.int = card.adventurer.int
            albumdata.str = card.adventurer.str
            albumdata.vit = card.adventurer.vit
            albumdata.min = card.adventurer.min
            albumdata.aggressive = card.adventurer.aggressive
            albumdata.cheerful = card.adventurer.cheerful
            albumdata.brave = card.adventurer.brave
            albumdata.cautious = card.adventurer.cautious
            albumdata.trickish = card.adventurer.trickish
            albumdata.avoid = card.adventurer.avoid
            albumdata.resist = card.adventurer.resist
            albumdata.defense = card.adventurer.defense
            albumdata.description = card.adventurer.description
            albumdata.coupons = card.adventurer.coupons

            albums.append(albumdata)
        else:
            cards.append(card)

    return cards, albums


def main() -> None:
    pass


if __name__ == "__main__":
    main()
