#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import itertools
import time
import datetime
import threading
import traceback
import shutil
import re
import math
import wx
import pygame
import pygame.surface
from pygame import MOUSEBUTTONDOWN, MOUSEBUTTONUP, KEYDOWN, KEYUP, USEREVENT

import cw
from cw.util import synclock

import typing
from typing import Tuple, Callable, Dict, Iterable, List, Literal, Optional, Sequence, Set, TypeVar, Union

_Arg1 = TypeVar("_Arg1")
_Arg2 = TypeVar("_Arg2")
_Arg3 = TypeVar("_Arg3")
_Arg4 = TypeVar("_Arg4")
_Arg5 = TypeVar("_Arg5")
_Arg6 = TypeVar("_Arg6")
_Arg7 = TypeVar("_Arg7")
_Arg8 = TypeVar("_Arg8")
_Arg9 = TypeVar("_Arg9")
_Result = TypeVar("_Result")

# build_exe.pyによって作られる一時モジュール
# cw.versioninfoからビルド時間の情報を得る
try:
    import versioninfo
except ImportError:
    class DummyVersionInfo(object):
        def __init__(self) -> None:
            self.build_datetime = ""

        def __bool__(self) -> bool:
            return False

    versioninfo = DummyVersionInfo()

init_rsrc: threading.Lock = threading.Lock()


class CWPyRunningError(Exception):
    pass


_dlg_mutex = threading.Lock()


class CWPy(threading.Thread):
    areaid: int
    skindir: str
    starttick: int

    mousemotion: bool
    mousepos: Tuple[int, int]
    wxmousepos: Tuple[int, int]
    wheelmode_cursorpos: Tuple[int, int]
    mousein: Tuple[bool, bool, bool]
    cursor: str

    event: cw.event.EventInterface
    music: List[cw.util.MusicInterface]

    override_dealspeed: int
    force_dealspeed: int
    cardgrp: pygame.sprite.LayeredDirty
    topgrp: pygame.sprite.LayeredDirty
    sbargrp: pygame.sprite.LayeredDirty
    backloggrp: pygame.sprite.LayeredDirty
    index: int

    advlog: "cw.advlog.AdventurerLogger"

    card_takenouttemporarily_personal_index: int

    pcards: List["cw.sprite.card.PlayerCard"]

    file_updates_bg: bool
    settingtab: int

    _yesnoresult: int

    msgs: cw.setting.MsgDict

    def __init__(self) -> None:
        threading.Thread.__init__(self)

        self._init = False
        self._running = False

        # イメージ・音声・フォント等のリソース
        self.rsrc = cw.setting.Resource(None)

        # シナリオデータorシステムデータ
        self.sdata: Union[cw.data.SystemData, cw.data.ScenarioData] = cw.data.SystemData(init=False)
        # クラシックなシナリオの再生中であればそのデータ
        self.classicdata: Optional[cw.binary.cwscenario.CWScenario] = None

        # 背景
        self.background = cw.sprite.background.BackGround()
        # ステータスバー
        self.statusbar = cw.sprite.statusbar.StatusBar()

        # 互換性データベース
        self.sct = cw.setting.ScenarioCompatibilityTable()

        self.setting = cw.setting.Setting()
        self.skinsounds = cw.setting.ResourceTable[str, cw.util.SoundInterface]("")

    def init(self, setting: cw.setting.Setting, frame: Optional["cw.frame.Frame"] = None) -> None:
        self._init = True
        if frame and not hasattr(self, "frame"):
            self.frame = frame   # 親フレーム
            # バージョン判定等で使用するシステムクーポン
            self.syscoupons = cw.setting.SystemCoupons()
            # 宿データ
            self.ydata: Optional[cw.data.YadoData] = None
            self.sct.init()
            self.init_pygame(setting)

    def __bool__(self) -> bool:
        return self._init

    def init_pygame(self, setting: cw.setting.Setting) -> None:
        """使用変数等はここ参照。"""
        self.setting = setting  # 設定
        self.status = "Title"
        self.update_titlebar()
        self.expand_mode = setting.expandmode  # 画面拡大条件
        self.is_processing = False  # シナリオ読込中か
        self.is_debuggerprocessing = False  # デバッガの処理が進行中か(宿の再ロードなど)
        self.is_decompressing = False  # アーカイブ展開中か
        self.is_updating_skin = False  # スキン切替中か

        self.update_scaling = False  # 画面スケール変更中か

        # pygame初期化
        fullscreen = self.setting.is_expanded and self.setting.expandmode == "FullScreen"
        self.scr, self.scr_draw, self.scr_fullscreen, self.clock =\
            cw.util.init(cw.SIZE_GAME, "", fullscreen, self.setting.soundfonts,
                         fullscreensize=self.frame.get_displaysize(),
                         sdlmixer_enabled=self.setting.sdlmixer_enabled)
        if fullscreen:
            self.set_fullscreen(True)
        # マウスポインタが合っているタイル
        self.pointed_tile: Optional[cw.sprite.touchbutton.PointableTile] = None
        # キー入力捕捉用インスタンス(キー入力は全てwx側で捕捉)
        self.keyevent = cw.eventrelay.KeyEventRelay()
        # Diceインスタンス(いろいろなランダム処理に使う)
        self.dice = cw.dice.Dice()
        # 選択中宿のパス
        self.yadodir = ""
        self.tempdir = ""
        # BattleEngineインスタンス
        self.battle: Optional[cw.battle.BattleEngine] = None
        # 勝利時イベント時エリアID
        self.winevent_areaid: Optional[int] = None
        # メインループ中に各種入力イベントがあったかどうかフラグ
        self.has_inputevent = False
        # アニメーションカットフラグ
        self.cut_animation = False
        # 入力があるまでメニューカード表示を待つ
        self.wait_showcards = False
        # ダイアログ表示階層
        self._showingdlg = 0
        # カーテンスプライト表示中フラグ
        self._curtained = False
        # カードの選択可否
        self.is_pcardsselectable = False
        self.is_mcardsselectable = True
        # 現在カードの表示・非表示アニメ中フラグ
        self._dealing = False
        # カード自動配置フラグ
        self._autospread = True
        # ゲームオーバフラグ(イベント終了処理時にチェック)
        self._gameover = False
        self._forcegameover = False
        # 現在選択中スプライト(SelectableSprite)
        self.selection: Optional[cw.sprite.base.SelectableSprite] = None
        self.lazy_selection: Optional[cw.sprite.base.SelectableSprite] = None
        # Trueの間は選択中のスプライトのクリックを行えない
        self.lock_menucards = False
        # 選択中のメンバ以外の戦闘行動が表示されている時はTrue
        self._show_allselectedcards = False
        # パーティカード表示中フラグ
        self.is_showparty = False
        # バックログ表示中フラグ
        self._is_showingbacklog = False
        # 同行キャスト表示中フラグ
        self._is_showingfcards = False
        # カード操作用データ
        self.selectedheader: Optional[cw.header.CardHeader] = None
        # デバッグモードかどうか
        self.debug = self.setting.debug
        # 選択中スキンのディレクトリ
        self.skindir = self.setting.skindir
        # 宿ロード直後であればTrue
        self._clear_changed = False
        # MusicInterfaceインスタンス
        self.music = [cw.util.MusicInterface(i, int(self.setting.vol_master*100))
                      for i in range(cw.bassplayer.MAX_BGM_CHANNELS)]
        # 最後に再生した効果音(システム・シナリオの2種)
        self.lastsound_scenario: List[Optional[cw.util.SoundInterface]] = [None] * cw.bassplayer.MAX_SOUND_CHANNELS
        self.lastsound_system: Optional[cw.util.SoundInterface] = None
        # EventInterfaceインスタンス
        self.event = cw.event.EventInterface()
        # Spriteグループ
        self.cardgrp = pygame.sprite.LayeredDirty()
        self.pcards: List[cw.sprite.card.PlayerCard] = []
        self.mcards: List[Union[cw.sprite.card.MenuCard, cw.sprite.card.EnemyCard, cw.sprite.card.FriendCard]] = []
        self.mcards_expandspchars: Set[Union[cw.sprite.card.MenuCard,
                                             cw.sprite.card.EnemyCard,
                                             cw.sprite.card.FriendCard]] = set()
        self.pricesprites: List[cw.sprite.background.PriceOfCard] = []
        self.curtains: List[cw.sprite.background.Curtain] = []
        self.topgrp = pygame.sprite.LayeredDirty()
        self.backloggrp = pygame.sprite.LayeredDirty()
        self.sbargrp = pygame.sprite.LayeredDirty()
        # 使用中カード
        self.inusecards: List[cw.sprite.background.InuseCardImage] = []
        self.guardcards: List[cw.sprite.background.InuseCardImage] = []
        # 一時的に荷物袋から取り出して使用中のカード
        self.card_takenouttemporarily: Optional[cw.header.CardHeader] = None
        self.card_takenouttemporarily_personal_owner: Optional[cw.sprite.card.PlayerCard] = None
        self.card_takenouttemporarily_personal_index = -1
        # エリアID
        self.areaid = 1
        # 特殊エリア移動前に保持しておく各種データ
        self.pre_areaids: List[Tuple[int, Optional[cw.data.CWPyElement]]] = []
        self.pre_mcards: List[List[Union[cw.sprite.card.MenuCard,
                                         cw.sprite.card.EnemyCard,
                                         cw.sprite.card.FriendCard]]] = []
        self.pre_dialogs: List[Tuple[str, cw.sprite.card.CWPyCard, Tuple[int, int], float]] = []
        # 各種入力イベント
        self.mousein = (False, False, False)
        self.mousepos = (-1, -1)
        self.wxmousepos = (-1, -1)
        self.mousemotion = False
        self.keyin: Tuple[int, ...] = ()
        self.events: List[pygame.event.Event] = []
        # list, index(キーボードでのカード選択に使う)
        self.list: List[cw.sprite.base.SelectableSprite] = []
        self.index = -1
        # 方向キーやマウスホイールで選択が変更された瞬間のカーソルの位置
        self.wheelmode_cursorpos = (-1, -1)
        # メニューカードのフラグごとの辞書
        self._mcardtable: Dict[str, List[Union[cw.sprite.card.EnemyCard, cw.sprite.card.MenuCard]]] = {}
        # イベント終了時にメニューカードのリストを
        # 更新する必要がある場合はTrue
        self._after_update_mcardlist = False
        # イベントハンドラ
        self.eventhandler: cw.eventhandler.EventHandler = cw.eventhandler.EventHandler()
        self.interrupt_eventhandler: Optional[cw.eventhandler.EventHandler] = None
        # メッセージログ表示中のハンドラ
        self._log_handler: Optional[Union[cw.eventhandler.EventHandlerForMessageWindow,
                                          cw.eventhandler.EventHandlerForBacklog]] = None
        # 設定ダイアログのタブ位置
        self.settingtab = 0
        # 保存用のパーティ記録
        # 解散エリアに入った時点で生成される
        self._stored_partyrecord: Optional[StoredParty] = None
        # 対象消去によってメンバの位置を再計算する必要があるか
        self._need_disposition = False
        # シナリオごとのブレークポイント情報
        self.breakpoint_table: Dict[Tuple[str, str], Set[str]] = {}
        self._load_breakpoints()
        # アニメーション中のスプライト
        self.animations: Set[cw.sprite.base.CWPySprite] = set()
        # 最小化によるアニメーションの停止時間
        self._stop_animations: Optional[float] = None
        # 一時的に速度設定の無いカード速度を上書きする
        # -1の時は無効
        self.override_dealspeed = -1
        # 一時的に全てのカード速度を上書きする
        # -1の時は無効
        self.force_dealspeed = -1
        # 空白時間の直後の時刻(Ticks)
        self.starttick = pygame.time.get_ticks()

        # JPDC撮影などで表示内容が変化するべきスプライト
        self.file_updates: Set[cw.sprite.base.CWPySprite] = set()
        # 背景の更新が発生しているか
        self.file_updates_bg = False

        # シナリオ選択ダイアログで選択されたシナリオ
        self.selectedscenario: Optional[cw.header.ScenarioHeader] = None

        # アーカイヴを展開中のシナリオ
        self.expanding: str = ""
        # 展開の進捗情報
        self.expanding_min: int = 0
        self.expanding_max: int = 100
        self.expanding_cur: int = 0
        # 現在のカーソル名
        self.cursor = ""
        self.change_cursor(force=True)

        # テキストログ
        self.advlog = cw.advlog.AdventurerLogger()

        # 遅延再描画を行う場合はTrue
        self._lazy_draw = False
        # 次の描画処理で再描画するべき領域
        self._lazy_clip: Optional[pygame.rect.Rect] = None

        # 時間経過処理中か
        self._elapse_time = False
        # 強制ロード処理中か
        self._reloading = False

        # self.call_yesno()の実行結果
        self._yesnoresult = wx.ID_CANCEL

        # ゲーム状態を"Title"にセット
        self.exec_func(self.startup, True)

    def set_fullscreen(self, fullscreen: bool) -> None:
        """wx側ウィンドウのフルスクリーンモードを切り替える。"""
        def func() -> None:
            if self.frame.IsFullScreen() == fullscreen:
                return
            if sys.platform == "win32":
                self.frame.ShowFullScreen(fullscreen)
            else:
                self.frame.SetMaxSize((-1, -1))
                self.frame.SetMinSize((-1, -1))
                self.frame.ShowFullScreen(fullscreen)
                if fullscreen:
                    dsize = self.frame.get_displaysize()
                    self.frame.SetClientSize(dsize)
                    self.frame.SetMaxSize(self.frame.GetBestSize())
                    self.frame.SetMinSize(self.frame.GetBestSize())
                    self.frame.panel.SetSize(dsize)
                else:
                    self.frame.SetClientSize(cw.wins(cw.SIZE_GAME))
                    self.frame.SetMaxSize(self.frame.GetBestSize())
                    self.frame.SetMinSize(self.frame.GetBestSize())
                    self.frame.panel.SetSize(cw.wins(cw.SIZE_GAME))
                self.frame.SetSize(self.frame.GetBestSize())

        self.frame.exec_func(func)

    def set_clientsize(self, size: Tuple[int, int]) -> None:
        """wx側ウィンドウの表示域サイズを設定する。"""
        def func() -> None:
            if sys.platform != "win32":
                self.frame.SetMaxSize((-1, -1))
                self.frame.SetMinSize((-1, -1))
                ws = self.frame.GetSize()[0] - self.frame.GetClientSize()[0]
                hs = self.frame.GetSize()[1] - self.frame.GetClientSize()[1]
            self.frame.SetClientSize(size)
            self.frame.panel.SetSize(size)
            if sys.platform != "win32":
                if self.frame.IsFullScreen():
                    dsize = self.frame.get_displaysize()
                    self.frame.SetMaxSize(dsize)
                    self.frame.SetMinSize(dsize)
                else:
                    # BUG: BestSizeが変化しない wxPython 4.0.1
                    # assert size[0] <= self.frame.GetBestSize()[0]
                    # assert size[1] <= self.frame.GetBestSize()[1]
                    size2 = size[0]+ws, size[1]+hs
                    self.frame.SetMaxSize(size2)
                    self.frame.SetMinSize(size2)
                    self.frame.SetSize(size2)
                self.exec_func(self.draw)

        self.frame.exec_func(func)

    def _load_breakpoints(self) -> None:
        """シナリオごとのブレークポイント情報をロードする。
        """
        if cw.fsync.is_waiting("Breakpoints.xml"):
            cw.fsync.sync()
        if not os.path.isfile("Breakpoints.xml"):
            return

        data = cw.data.xml2element("Breakpoints.xml")
        for e_sc in data:
            if e_sc.tag != "Breakpoints":
                continue

            scenario = e_sc.get("scenario", "")
            author = e_sc.get("author", "")
            key = (scenario, author)
            bps = set()
            for e in e_sc:
                if e.tag != "Breakpoint":
                    continue
                if e.text:
                    bps.add(e.text)
            self.breakpoint_table[key] = bps

    def _save_breakpoints(self) -> None:
        """シナリオごとのブレークポイント情報を保存する。
        """
        if isinstance(self.sdata, cw.data.ScenarioData):
            self.sdata.save_breakpoints()

        element = cw.data.make_element("AllBreakpoints")

        for key, bps in self.breakpoint_table.items():
            scenario, author = key
            e_sc = cw.data.make_element("Breakpoints", attrs={"scenario": scenario,
                                                              "author": author})
            for bp in bps:
                if bp:
                    e = cw.data.make_element("Breakpoint", bp)
                    e_sc.append(e)
            if len(e_sc):
                element.append(e_sc)

        path = "Breakpoints.xml"
        if cw.fsync.is_waiting(path):
            cw.fsync.sync()
        if len(element):
            etree = cw.data.xml2etree(element=element)
            etree.write_file(path)
        elif os.path.isfile(path):
            cw.util.remove(path)

    def _init_resources(self) -> bool:
        try:
            """スキンが関わるリソースの初期化"""
            self.init_fullscreenparams()

            # リソース(辞書)
            locked = init_rsrc.locked()
            if not locked:
                init_rsrc.acquire()
            if self.rsrc:
                self.rsrc.dispose()
            rsrc = self.rsrc
            self.rsrc = cw.setting.Resource(self.setting)
            # システム効果音(辞書)
            self.sounds = self.rsrc.sounds
            # その他のスキン付属効果音(辞書)
            self.skinsounds = self.rsrc.skinsounds
            # システムメッセージ(辞書)
            self.msgs = self.rsrc.msgs
            # アクションカードのデータ(CardHeader)
            # スケールのみの変更ではリセットしない
            if rsrc:
                self.rsrc.actioncards = rsrc.get_actioncards()
                self.rsrc.backpackcards = rsrc.get_backpackcards()
            else:
                self.rsrc.actioncards = self.rsrc.get_actioncards()
                self.rsrc.backpackcards = self.rsrc.get_backpackcards()
            if not locked:
                init_rsrc.release()
            # 背景スプライト
            if not self.background.is_initialized():
                self.background.init()
            self._update_clip()
            # ステータスバースプライト
            if not self.statusbar.is_initialized():
                self.statusbar.init()
                # ステータスバークリップ
                self.sbargrp.set_clip(self.statusbar.rect)

            for ccard in itertools.chain(self.get_pcards(), self.get_ecards(), self.get_fcards()):
                assert isinstance(ccard, cw.character.Character)
                ccard.deck.update_actioncards(ccard)

            self.update_fullscreenbackground()
            self.frame.exec_func(self.frame.update_dialogparams)

            return True
        except cw.setting.NoFontError:
            def func() -> None:
                s = ("CardWirthPyの実行に必要なフォントがありません。\n"
                     "Data/Font以下にIPAフォントをインストールしてください。")
                wx.MessageBox(s, "メッセージ", wx.OK | wx.ICON_ERROR, cw.cwpy.frame)
                cw.cwpy.frame.Destroy()
            cw.cwpy.frame.exec_func(func)
            return False

    def init_sounds(self) -> None:
        """スキン付属の効果音を再読込する。"""
        self.rsrc.init_sounds()
        # システム効果音(辞書)
        self.sounds = self.rsrc.sounds
        # その他のスキン付属効果音(辞書)
        self.skinsounds = self.rsrc.skinsounds

    def _update_clip(self) -> None:
        clip = pygame.rect.Rect(cw.s((0, 0)), cw.s(cw.SIZE_AREA))
        self.cardgrp.set_clip(clip)
        self.topgrp.set_clip(clip)
        self.backloggrp.set_clip(clip)

    def update_skin(self, skindirname: str, changearea: bool = True, restartop: bool = True,
                    afterfunc: Optional[Callable[[], None]] = None, switch_skin: bool = False,
                    switch_yado: bool = False) -> None:
        if not switch_yado:
            self.is_updating_skin = True

        self.file_updates.clear()
        self.sdata.path_cache.clear()
        if self.status == "Title" and restartop:
            changearea = False
            self.cardgrp.remove(*self.mcards)
            self.mcards = []
            self.mcards_expandspchars.clear()
            self.background.bgs = []
        elif self.status == "GameOver":
            changearea = False
        elif (self.status == "Yado" or (self.is_playingscenario() and self.areaid in cw.AREAS_SP)) and\
                self.setting.skindirname != skindirname and not switch_yado:
            if self.is_runningevent():
                self.exec_func(self.update_skin, skindirname, changearea, restartop, afterfunc, switch_skin,
                               switch_yado)
                if self.is_showingmessage():
                    mwin = self.get_messagewindow()
                    assert mwin
                    mwin.result = cw.event.EffectBreakError()
                else:
                    self.event.stoped = True
                return
            if self.status == "Yado":
                assert self.ydata
                self.stop_allsounds()
                # 時限クーポン削除
                for pcard in self.get_pcards():
                    pcard.remove_timedcoupons()
                    pcard.set_fullrecovery()
                    pcard.update_image()
                if cw.SKIN_AREAS_MIN <= self.areaid <= cw.SKIN_AREAS_MAX:
                    # スキン固有のエリアから離脱
                    changearea = True
                    if self.ydata.party:
                        self.areaid = 2
                    else:
                        self.areaid = 1

        if self.status == "Yado" and self.pre_areaids:
            oldareaid: Optional[int] = self.areaid
            selectedheader = self.selectedheader
            pre_dialogs = self.pre_dialogs[:]
            self.clean_specials(redraw=False, silent=True)
        else:
            oldareaid = None

        if self.ydata and self.ydata.party and not self.is_playingscenario():
            self.ydata.party.remove_numbercoupon()

        changed = bool(self.ydata and self.ydata.is_changed())
        scedir = self.setting.get_scedir()
        oldskindirname = self.setting.skindirname
        self.setting.skindirname = skindirname
        self.setting.init_skin()
        if self.ydata:
            self.ydata.set_skinname(skindirname, self.setting.skintype)
        self.skindir = self.setting.skindir
        oldskindir = cw.util.join_paths("Data/Skin", oldskindirname)
        newskindir = cw.util.join_paths("Data/Skin", skindirname)
        self.background.update_skin(oldskindir, newskindir)

        if self.ydata and self.ydata.party and not self.is_playingscenario():
            self.ydata.party.set_numbercoupon()

        def repl_cardimg(sprite: cw.sprite.card.PlayerCard) -> None:
            if sprite.has_cardimg():
                for path in sprite.cardimg.paths:
                    if path.path.startswith(oldskindir):
                        path.path = path.path.replace(oldskindir, newskindir)
        for pcard in self.get_pcards():
            repl_cardimg(pcard)

        if self.sdata:
            self.sdata.update_skin()

        if not self.is_battlestatus() and changearea and not (self.status == "Title" and self.topgrp.sprites()):
            for sprite in self.mcards[:]:
                if not isinstance(sprite, cw.sprite.card.FriendCard):
                    self.cardgrp.remove(sprite)
                    self.mcards.remove(sprite)
                    self.mcards_expandspchars.discard(sprite)
                    self.file_updates.discard(sprite)
            if self.is_playingscenario():
                self.sdata.change_data(self.areaid, data=self.sdata.data)
            else:
                self.sdata.change_data(self.areaid, data=None)
            self.set_mcards(self.sdata.get_mcarddata(data=self.sdata.data), False, True, setautospread=True)
            self.deal_cards()
            if self.is_playingscenario():
                self.background.reload(doanime=False, ttype=("None", "None"), redraw=False, nocheckvisible=True)
            else:
                self.background.load(self.sdata.get_bgdata(), False, ("None", "None"), redraw=False)
            if not self.is_playingscenario():
                self.sdata.start_event(keynum=1, redraw=False)

        self.clear_selection()
        init_rsrc.acquire()
        if self.rsrc:
            self.rsrc.dispose()

        def func() -> None:
            init_rsrc.release()
            assert self.rsrc
            if afterfunc:
                afterfunc()

            if self.is_battlestatus() and self.battle:
                for ccard in self.get_pcards("unreversed"):
                    ccard.deck.set(ccard)
                    if self.battle.is_ready():
                        ccard.decide_action()
                for ecard in self.get_ecards():
                    ecard.update_skin()
                    if not ecard.is_reversed():
                        ecard.deck.set(ecard)
                        if self.battle.is_ready():
                            ecard.decide_action()
                for fcard in self.get_fcards():
                    fcard.deck.set(fcard)
                    if self.battle.is_ready():
                        fcard.decide_action()

            self.update_titlebar()

            if scedir != self.setting.get_scedir():
                self.setting.lastscenario = []
                self.setting.lastscenariopath = ""
                self.setting.lastfindresult = []

            if self.ydata:
                self.ydata._changed = changed

            if self.status == "Title" and restartop:
                # タイトル画面にいる場合はロゴ表示前まで戻す
                self.is_updating_skin = False
                if self.topgrp.sprites():
                    # アニメーション中なら中止してから戻す
                    self.exec_func(self.startup, False)
                    raise cw.event.EffectBreakError()
                else:
                    self.startup(loadyado=False)
            else:
                if not switch_yado:
                    for music in self.music:
                        if switch_skin:
                            fpath = music.get_path(music.path, music.inusecard)
                            fpath = self.rsrc.get_filepath(fpath)
                            if os.path.isfile(fpath):
                                music.play(music.path, updatepredata=False)
                        else:
                            music.play(music.path, updatepredata=False)

                if oldareaid is not None:
                    self.selectedheader = selectedheader
                    self.pre_dialogs = pre_dialogs
                    self.change_specialarea(oldareaid, silent=True)
                    self.statusbar.change(showbuttons=True)

                if self.areaid in cw.AREAS_TRADE and self.selectedheader:
                    self.show_numberofcards(self.selectedheader.type)

                self.is_updating_skin = False

        self.update_scale(cw.UP_WIN, changearea, rsrconly=True, afterfunc=func)

    def update_yadoinitial(self) -> None:
        if not self.ydata or self.ydata.party or self.is_playingscenario():
            return
        if self.ydata.is_empty() and not self.ydata.is_changed():
            if self.areaid == 1:
                self.change_area(3)
        else:
            if self.areaid == 3:
                self.change_area(1)

    def update_titlebar(self) -> None:
        """タイトルバー文字列を更新する。"""
        self.set_titlebar(self.create_title())

    def create_title(self) -> str:
        """タイトルバー文字列を生成する。"""
        s = self.setting.titleformat
        d = self.get_titledic()
        assert isinstance(d, dict)
        return cw.util.format_title(s, d)

    def get_titledic(self, with_datetime: bool = False,
                     for_fname: bool = False) -> Union[Dict[str, str], Tuple[Dict[str, str], Dict[str, str]]]:
        """タイトルバー文字列生成用の情報を辞書で取得する。"""
        seq = []
        for v in cw.APP_VERSION:
            seq.append(str(v))
        vstr = ".".join(seq)

        d = {"application": cw.APP_NAME, "skin": self.setting.skinname, "version": vstr}

        if versioninfo:
            d["build"] = versioninfo.build_datetime

        if self.ydata:
            d["yado"] = self.ydata.name
            if self.ydata.party:
                d["party"] = self.ydata.party.name

        if self.status.startswith("Scenario") or self.status == "GameOver":
            sdata = self.ydata.losted_sdata if self.ydata and self.ydata.losted_sdata else self.sdata
            d["scenario"] = sdata.name
            d["author"] = sdata.author
            d["path"] = sdata.fpath
            d["file"] = os.path.basename(sdata.fpath)
            versionhint = sdata.get_versionhint()
            d["compatibility"] = self.sct.to_basehint(versionhint)

        if with_datetime:
            date = datetime.datetime.today()
            d["date"] = date.strftime("%Y-%m-%d")
            d["year"] = date.strftime("%Y")
            d["month"] = date.strftime("%m")
            d["day"] = date.strftime("%d")
            d["time"] = date.strftime("%H:%M:%S")
            d["hour"] = date.strftime("%H")
            d["minute"] = date.strftime("%M")
            d["second"] = date.strftime("%S")
            d["millisecond"] = date.strftime("%f")[:3]

        if for_fname:
            d2 = {}
            for key, value in d.items():
                value = value.replace(" ", "_")
                value = value.replace(":", ".")
                d2[key] = cw.binary.util.check_filename(value).strip()
            return (d, d2)
        else:
            return d

    def update_scale(self, scale: float, changearea: bool = True, rsrconly: bool = False, udpatedrawsize: bool = True,
                     displaysize: Optional[Tuple[int, int]] = None,
                     afterfunc: Optional[Callable[[], None]] = None) -> None:
        """画面の表示倍率を変更する。
        scale: 倍率。1は拡大しない。2で縦横2倍サイズの表示になる。
        """
        fullscreen = self.is_expanded() and self.setting.expandmode == "FullScreen"
        if displaysize is None and fullscreen:
            def func() -> None:
                dsize = self.frame.get_displaysize()

                def func() -> None:
                    self.update_scale(scale, changearea, rsrconly, udpatedrawsize, dsize)
                    if afterfunc:
                        afterfunc()
                self.exec_func(func)
            self.frame.exec_func(func)
            return

        self.update_scaling = True

        if self.ydata:
            changed = self.ydata.is_changed()
        else:
            changed = False

        if not rsrconly:
            cw.UP_SCR = scale
            flags = 0
            if fullscreen:
                dsize = displaysize
                assert dsize
                self.scr_fullscreen = pygame.display.set_mode((dsize[0], dsize[1]), flags)
                self.scr = pygame.surface.Surface(cw.s(cw.SIZE_GAME)).convert()
                self.scr_draw = self.scr
            else:
                self.scr_fullscreen = None
                self.scr = pygame.display.set_mode(cw.wins(cw.SIZE_GAME), flags)
                if cw.UP_SCR == cw.UP_WIN:
                    self.scr_draw = self.scr
                else:
                    self.scr_draw = pygame.surface.Surface(cw.s(cw.SIZE_GAME)).convert()

        if udpatedrawsize:
            self._init_resources()

            self.statusbar.update_scale()
            self.sbargrp.set_clip(self.statusbar.rect)
            if self.sdata:
                self.sdata.update_scale()
                if self.pre_mcards:
                    mcarddata = self.sdata.get_mcarddata(self.pre_areaids[-1][0], data=self.pre_areaids[-1][1])
                    mcards: List[Union[cw.sprite.card.MenuCard,
                                       cw.sprite.card.EnemyCard,
                                       cw.sprite.card.FriendCard]] = []
                    mcards.extend(self.set_mcards(mcarddata, False, False, splayer=False))
                    self.pre_mcards[-1] = mcards
                if self.pre_areaids:
                    for i, (preareaid, _predata) in enumerate(self.pre_areaids[:]):
                        self.pre_areaids[i] = (preareaid, self.sdata.get_areadata(preareaid))
            self._update_clip()

            cw.sprite.message.MessageWindow.clear_selections()
            for sprite in self.cardgrp.sprites():
                assert isinstance(sprite, cw.sprite.base.CWPySprite)
                if sprite.is_initialized() and not isinstance(sprite, (cw.sprite.background.BackGround,
                                                                       cw.sprite.background.BgCell))\
                        and not isinstance(sprite, cw.sprite.background.Curtain):
                    sprite.update_scale()
            for sprite in self.topgrp.sprites():
                assert isinstance(sprite, cw.sprite.base.CWPySprite)
                sprite.update_scale()
            for sprite in self.backloggrp.sprites():
                assert isinstance(sprite, cw.sprite.base.CWPySprite)
                sprite.update_scale()
            for sprite in self.get_fcards():
                sprite.update_scale()

            for sprite in self.cardgrp.sprites():
                assert isinstance(sprite, cw.sprite.base.CWPySprite)
                if sprite.is_initialized() and isinstance(sprite, (cw.sprite.background.BackGround,
                                                                   cw.sprite.background.BgCell))\
                        and not isinstance(sprite, cw.sprite.background.Curtain):
                    sprite.update_scale()
            for sprite in self.cardgrp.sprites():
                if isinstance(sprite, cw.sprite.background.Curtain) and\
                        isinstance(sprite.target, cw.sprite.card.CWPyCard):
                    sprite.update_scale()

            self._update_clip()
            for music in self.music:
                music.update_scale()
        else:
            self.init_fullscreenparams()
            self.update_fullscreenbackground()
            self.frame.exec_func(self.rsrc.update_winscale)
            self.frame.exec_func(self.frame.update_dialogparams)

        if self.ydata:
            self.ydata._changed = changed

        self.clear_selection()
        self.mousepos = (-1, -1)

        if not self.is_showingdlg():
            # 一度マウスポインタを画面外へ出さないと
            # フォーカスを失うことがある
            pos = pygame.mouse.get_pos()
            if -1 < pos[0] and -1 < pos[1] and pygame.mouse.get_focused():
                pygame.mouse.set_pos([-1, -1])
                pygame.mouse.set_pos(pos)
        self.change_cursor(self.cursor, force=True)

        self.update_scaling = False
        if udpatedrawsize and not self.background.reload_jpdcimage and self.background.has_jpdcimage:
            self.background.reload(False, ttype=("None", 0))

        if afterfunc:
            afterfunc()

        self.add_lazydraw(cw.s(pygame.rect.Rect((0, 0), cw.SIZE_GAME)))

        if changearea:
            def func() -> None:
                self.update()
            self.exec_func(func)

        if not rsrconly and not (self.setting.expandmode == "FullScreen" and self.is_expanded()):
            def func() -> None:
                self.set_clientsize(cw.wins(cw.SIZE_GAME))
            self.exec_func(func)

    def update_messagestyle(self) -> None:
        """メッセージの描画形式の変更を反映する。"""
        cw.sprite.message.MessageWindow.clear_selections()
        for sprite in itertools.chain(self.cardgrp.get_sprites_from_layer(cw.layer_val(cw.LAYER_MESSAGE)),
                                      self.cardgrp.get_sprites_from_layer(cw.layer_val(cw.LAYER_SPMESSAGE))):
            assert isinstance(sprite, cw.sprite.base.CWPySprite), sprite
            sprite.update_scale()
        if isinstance(self._log_handler, cw.eventhandler.EventHandlerForBacklog):
            self._log_handler.update_sprites(clearcache=True)

    def update_vocation120(self, vocation120: bool) -> None:
        """適性表示を1.20に合わせる設定を変更する。"""
        if self.setting.vocation120 != vocation120:
            self.setting.vocation120 = vocation120
            for sprite in self.cardgrp.sprites():
                if isinstance(sprite, cw.sprite.background.InuseCardImage) or\
                        (isinstance(sprite, (cw.sprite.card.PlayerCard,
                                             cw.sprite.card.EnemyCard,
                                             cw.sprite.card.FriendCard)) and sprite.is_initialized() and
                         sprite.test_aptitude):
                    sprite.update_scale()

    def update_curtainstyle(self) -> None:
        """カーテンの描画形式の変更を反映する。"""
        for sprite in itertools.chain(self.cardgrp.sprites(),
                                      self.backloggrp.sprites(),
                                      self.sbargrp.sprites()):
            if isinstance(sprite, cw.sprite.message.BacklogCurtain):
                sprite.color = self.setting.blcurtaincolour
                sprite.update_scale()
            elif isinstance(sprite, cw.sprite.background.Curtain):
                sprite.color = self.setting.curtaincolour
                sprite.update_scale()

    def set_debug(self, debug: bool) -> None:
        self.setting.debug = debug
        self.setting.debug_saved = debug
        self.debug = debug
        self.statusbar.change(not self.is_runningevent())

        if self.is_battlestatus():
            if self.battle:
                self.battle.update_debug()
            else:
                for sprite in self.get_mcards():
                    sprite.update_scale()

        if not debug and self.is_showingdebugger():
            def func() -> None:
                def func() -> None:
                    if self.frame and self.frame.debugger:
                        self.frame.debugger.Close()
                self.frame.exec_func(func)
            self.exec_func(func)

        if not self.is_decompressing:
            self.remove_pricesprites()
            cw.data.redraw_cards(debug)
            if self.selectedheader:
                self.set_testaptitude(self.selectedheader)

        if isinstance(self.selection, cw.character.Character) and self.selection.is_reversed() and not debug:
            self.clear_selection()
        if self.is_showingmessage():
            mwin = self.get_messagewindow()
            assert mwin
            self.list = []
            self.list.extend(mwin.selections)
        else:
            self.list = []
            self.list.extend(self.get_mcards("selectable"))
        self.index = -1
        self.change_selection(self.selection)
        self.add_lazydraw(clip=cw.s(pygame.rect.Rect((0, 0), cw.SIZE_GAME)))

        if not debug and self.is_curtained() and 0 <= self.areaid and self.selectedheader:
            owner = self.selectedheader.get_owner()
            if isinstance(owner, (cw.character.Enemy, cw.character.Friend)):
                # ターゲット選択エリアを解除する
                if not (isinstance(owner, cw.character.Enemy) and owner.is_analyzable()):
                    assert 0 < len(self.pre_dialogs)
                    self.pre_dialogs.pop()
                assert isinstance(owner, cw.sprite.card.CWPyCard)
                cw.cwpy.clear_inusecardimg(owner)
                cw.cwpy.clear_targetarrow()
                cw.cwpy.clear_specialarea()

    def update_infocard(self) -> None:
        """デバッガ等から所有情報カードの変更を
        行った際に呼び出される。
        """
        self.sdata.notice_infoview = True
        showbuttons = not self.is_playingscenario() or (
                self.areaid not in cw.AREAS_TRADE and self.areaid in cw.AREAS_SP
        )
        if self.is_battlestatus() and self.battle and not self.battle.is_ready():
            showbuttons = False
        self.statusbar.change(showbuttons)

        if self.areaid == cw.AREA_CAMP and self.is_playingscenario():
            cw.data.redraw_cards(cw.cwpy.sdata.has_infocards())

    def run(self) -> None:
        try:
            try:
                self._run()
            except CWPyRunningError:
                self.quit()

            self._quit()
        except Exception:
            self.is_processing = False
            self._running = False
            if self.advlog:
                self.advlog.force_quit()
            cw.fsync.quit()
            # エラーログを出力
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_exception(exc_type, exc_value, exc_traceback, file=sys.stderr)
            sys.stderr.write("\n")

            # 画面にエラーメッセージのスプライトを貼り付ける
            self.play_sound("error")
            s1 = "処理中に内部エラーが発生したため、ゲームを停止しました。"
            s2 = "このエラーは想定されたものではなく、バグによってしか発生しません。"
            s3 = "エラーログ(CardWirthPy.exe.log)の内容を開発者までお知らせください。"
            font = cw.imageretouch.Font("", cw.wins(18), True, False)
            sz1 = font.size(s1)
            sz2 = font.size(s2)
            sz3 = font.size(s3)
            bmp = pygame.surface.Surface((max(sz1[0], sz2[0], sz3[0])+cw.wins(10),
                                          sz1[1]+sz2[1]+sz3[1]+cw.wins(10))).convert()
            bmp.fill((255, 255, 255))
            subimg = font.render(s1, True, (0, 0, 0))
            bmp.blit(subimg, cw.wins((5, 5)))
            subimg = font.render(s2, True, (0, 0, 0))
            bmp.blit(subimg, (cw.wins(5), cw.wins(5)+sz1[1]))
            subimg = font.render(s3, True, (0, 0, 0))
            bmp.blit(subimg, (cw.wins(5), cw.wins(5)+sz1[1]+sz2[1]))
            if self.scr_fullscreen:
                def func() -> None:
                    self.frame.ShowFullScreen(False)
                    self.frame.SetSize(self.frame.get_displaysize())
                cw.cwpy.frame.exec_func(func)
                scr = self.scr_fullscreen
            else:
                scr = self.scr
            rect = pygame.rect.Rect((scr.get_width()-bmp.get_width())//2, (scr.get_height()-bmp.get_height())//2,
                                    bmp.get_width(), bmp.get_height())
            scr.blit(bmp, rect.topleft)
            pygame.display.update(rect)
        finally:
            cw.util.clear_mutex()

    def _run(self) -> None:
        self._running = True

        while self._running:
            self.main_loop(True)

    def get_eventhandler(self) -> cw.eventhandler.EventHandler:
        if self.interrupt_eventhandler:
            return self.interrupt_eventhandler
        else:
            return self.eventhandler

    def main_loop(self, update: bool) -> None:
        if pygame.event.peek((USEREVENT, cw.FORCE_USEREVENT)):
            self.input()              # 各種入力イベント取得
            self.get_eventhandler().run()   # イベントを消化
        else:
            self.tick_clock()         # FPS調整
            self.starttick = pygame.time.get_ticks()
            self.input()              # 各種入力イベント取得
            self.get_eventhandler().run()   # イベントハンドラ
            if not pygame.event.peek(USEREVENT):
                if update:
                    self.update()         # スプライトの更新
                else:
                    self.proc_animation()
                self.draw(True)           # スプライトの描画

        if not self.is_runningevent() and self._clear_changed:
            if self.ydata:
                self.ydata._changed = False
            self._clear_changed = False

    def quit(self) -> None:
        # トップフレームから閉じて終了。cw.frame.OnDestroy参照。
        if not self.frame:
            return
        event = wx.PyCommandEvent(wx.wxEVT_DESTROY)
        self.frame.AddPendingEvent(event)

    def quit2(self) -> None:
        self.ydata = None
        if not self.frame:
            return
        event = wx.PyCommandEvent(wx.wxEVT_CLOSE_WINDOW)
        self.frame.AddPendingEvent(event)

    def _quit(self) -> None:
        self.advlog.end_scenario(False, False)
        if self.sdata:
            self.sdata.sleep_timekeeper()
        self.stop_allsounds()
        if self.lastsound_system:
            self.lastsound_system.stop(False)
            self.lastsound_system = None
        pygame.quit()
        cw.util.remove_temp()
        self._save_breakpoints()
        self.setting.write()
        if self.rsrc:
            self.rsrc.clear_systemfonttable()

    def tick_clock(self, framerate: int = 0) -> None:
        self.proc_animation()
        self.lazy_draw()
        if framerate:
            self.clock.tick(framerate)
        else:
            self.clock.tick(self.setting.fps)

    def wait_frame(self, count: int, canskip: bool = True, stoptheworld: Optional["cw.sprite.base.StopTheWorld"] = None,
                   framerate: int = 0) -> bool:
        """countフレーム分待機する。"""
        self.lazy_draw()
        if not framerate:
            framerate = self.setting.fps
        if self.starttick is not None:
            # 直前の処理に時間がかかっていた場合はフレームを飛ばす
            tick = pygame.time.get_ticks()
            t = (self.starttick + 1.0 / framerate * count * 1000) - tick
            if t <= 0:
                count = 0
        self.event.eventtimer = 0
        skip = False
        i = 0
        if count == 0:
            self.input(inputonly=True)
            self.get_eventhandler().run()
        while i < count:
            if canskip:
                # リターンキー長押し, マウスボタンアップ, キーダウンで処理中断
                if self.keyevent.is_keyin(pygame.K_RETURN) or self.keyevent.is_mousein():
                    skip = True
                    break

            self.update_groups((self.sbargrp,))
            if canskip:
                breakflag = self.get_breakflag(handle_wheel=cw.cwpy.setting.can_skipwait_with_wheel)
            self.input(inputonly=True)
            self.get_eventhandler().run()
            if canskip and breakflag:
                skip = True
                break

            if stoptheworld:
                stoptheworld.is_waiting()
            self.tick_clock(framerate)
            if not (self.setting.stop_the_world_with_iconized and self.frame.is_iconized):
                i += 1
        self.starttick = pygame.time.get_ticks()
        return skip

    def get_breakflag(self, handle_wheel: bool = True) -> bool:
        """待機時間を飛ばすべき入力がある場合にTrueを返す。"""
        if self.is_playingscenario() and self.sdata.in_f9:
            return True
        breakflag = False
        self.keyevent.peek_mousestate()
        events = pygame.event.get((pygame.MOUSEBUTTONUP, pygame.KEYUP))
        for e in events:
            if e.type in (pygame.MOUSEBUTTONUP, pygame.MOUSEBUTTONDOWN) and hasattr(e, "button"):
                if not handle_wheel and e.button in (4, 5):
                    # ホイールによる空白時間スキップ無効の設定
                    continue
                breakflag = True
            elif e.type == pygame.KEYUP:
                if e.key in (pygame.K_RETURN, pygame.K_SPACE, pygame.K_UP, pygame.K_DOWN, pygame.K_LEFT, pygame.K_RIGHT,
                             pygame.K_PAGEUP, pygame.K_PAGEDOWN, pygame.K_HOME, pygame.K_END):
                    breakflag = True
            cw.thread.post_pygameevent(e)
        return breakflag

    def get_nextevent(self) -> Optional[pygame.event.Event]:
        # BUG: 稀にbuttonのないMOUSEBUTTONUPが発生するらしい(環境による？)
        #      そのため、buttonのないマウスイベントやkeyのないキーイベントが
        #      発生していないかここでチェックし、そうしたイベントを無視する
        while True:
            if self.events:
                e = self.events[0]
                self.events = self.events[1:]
                # ---
                if e.type in (MOUSEBUTTONDOWN, MOUSEBUTTONUP) and not hasattr(e, "button"):
                    continue
                elif e.type in (KEYDOWN, KEYUP) and not hasattr(e, "key"):
                    continue
                elif e.type not in (USEREVENT, cw.FORCE_USEREVENT) and self.is_showingdlg():
                    continue
                # ---
                return e
            else:
                return None

    def clear_inputevents(self) -> None:
        self.keyevent.peek_mousestate()
        pygame.event.clear((MOUSEBUTTONDOWN, MOUSEBUTTONUP, KEYDOWN, KEYUP))
        events = []
        for e in self.events:
            if e.type not in (MOUSEBUTTONDOWN, MOUSEBUTTONUP, KEYDOWN, KEYUP):
                events.append(e)
        self.events = events

    def input(self, eventclear: bool = False, inputonly: bool = False, noinput: bool = False) -> None:
        if eventclear:
            self.clear_inputevents()
            return

        self.keyevent.peek_mousestate()
        self.proc_animation()

        if not self.is_showingdlg():
            if sys.platform == "win32":
                self.mousein = pygame.mouse.get_pressed(3)[:3]
            mousepos = self.mousepos
            mousemotion2 = False
            if self.update_mousepos():
                # カーソルの移動を検出
                mousemotion2 = self.mousepos != mousepos
                if self.wheelmode_cursorpos != (-1, -1) and self.mousepos != (-1, -1) and mousepos != (-1, -1):
                    # 方向キーやホイールで選択を変更中は、マウスが多少ぶれても移動を検出しないようにする
                    # (元々の位置からの半径で検出)
                    ax, ay = self.wheelmode_cursorpos
                    bx, by = self.mousepos
                    self.mousemotion = self.setting.radius_notdetectmovement < abs(math.hypot(ax-bx, ay-by))
                    if self.mousemotion:
                        self.wheelmode_cursorpos = (-1, -1)
                else:
                    self.mousemotion = mousemotion2
                    self.wheelmode_cursorpos = (-1, -1)

            if self.mousemotion:
                for i in range(len(self.keyevent.mousein)):
                    if not self.keyevent.mousein[i] in (0, -1):
                        # マウスポインタが動いた場合は連打開始までの待ち時間を延期する
                        # (-1はすでに連打状態)
                        self.keyevent.mousein[i] = pygame.time.get_ticks()

            if mousemotion2:
                self.update_allselectedcards()

            self.keyin = self.keyevent.get_pressed()

        if inputonly:
            seq = []
            for e in self.events:
                if e.type in (MOUSEBUTTONDOWN, MOUSEBUTTONUP, KEYDOWN, KEYUP, cw.FORCE_USEREVENT):
                    seq.append(e)
                else:
                    cw.thread.post_pygameevent(e)
            events = pygame.event.get((MOUSEBUTTONDOWN, MOUSEBUTTONUP, KEYDOWN, KEYUP))
            if events:
                events = [events[-1]]
            seq.extend(events)
            seq.extend(pygame.event.get((cw.FORCE_USEREVENT,)))
            del self.events[:]
            self.events.extend(seq)
        else:
            if noinput:
                seq = []
                for e in self.events:
                    if e.type not in (MOUSEBUTTONDOWN, MOUSEBUTTONUP, KEYDOWN, KEYUP, cw.FORCE_USEREVENT):
                        seq.append(e)
                del self.events[:]
                self.events.extend(seq)
                pygame.event.clear((MOUSEBUTTONDOWN, MOUSEBUTTONUP, KEYDOWN, KEYUP))
            self.events.extend(pygame.event.get())

    def update_allselectedcards(self) -> None:
        """
        マウスカーソルの位置に応じて
        全員の戦闘行動を表示するか、消去する。
        """
        if self.setting.show_allselectedcards and not self.is_runningevent() and self.is_battlestatus() and\
                self.battle and self.battle.is_ready() and\
                self._show_allselectedcards != self._in_partyarea(self.mousepos):
            self._show_allselectedcards = True
            self.change_selection(self.selection)
            for inusecard in self.inusecards:
                self.add_lazydraw(clip=inusecard.rect)

    def _in_partyarea(self, mousepos: Tuple[int, int]) -> bool:
        """
        全員分の戦闘行動表示の有無を切り替えるため、
        マウスカーソルがプレイヤーエリアないし同行キャストエリアにあるかを判定する。
        """
        for pcard in self.get_pcards():
            rect = pygame.rect.Rect(pcard.rect)
            rect.left -= cw.s(9)
            rect.width += cw.s(18)
            rect.height += cw.s(5)
            if rect.collidepoint(*mousepos):
                return True
        for fcard in self.get_fcards():
            rect = pygame.rect.Rect(fcard.rect)
            rect.left -= cw.s(9)
            rect.width += cw.s(18)
            rect.top -= cw.s(5)
            rect.height += cw.s(5)
            if rect.collidepoint(*mousepos):
                return True
        return False

    def update_mousepos(self) -> bool:
        if sys.platform != "win32":
            self.mousepos = self.wxmousepos
            return True
        if pygame.mouse.get_focused() or sys.platform != "win32":
            if self.scr_fullscreen:
                mousepos = pygame.mouse.get_pos()
                x = int((mousepos[0] - self.scr_pos[0]) / self.scr_scale)
                y = int((mousepos[1] - self.scr_pos[1]) / self.scr_scale)
                self.mousepos = (x, y)
            else:
                self.mousepos = cw.mwin2scr_s(pygame.mouse.get_pos())
        else:
            self.mousepos = (-1, -1)
        return True

    def update(self, *args: typing.Any, **kwargs: typing.Any) -> None:
        if not self.statusbar:
            return

        assert not self.event.in_cardeffectmotion

        # 状態の補正
        if not self.statusbar.showbuttons:
            # 通常エリアで操作可能な状態であればステータスバーのボタンを表示
            if not self.is_runningevent() and self.areaid not in cw.AREAS_TRADE and not self.selectedheader:
                self.statusbar.change()

        if self.lock_menucards:
            # 操作可能であればメニューカードのロックを解除
            if not self.is_runningevent() and not self.is_showingdlg():
                self.lock_menucards = False

        cw.cwpy.frame.check_killlist()

        if not self.lock_menucards:
            # 一時カードはダイアログを開き直す直前に荷物袋へ戻すが、
            # 戦闘突入等でダイアログを開き直せなかった場合はここで戻す
            self.return_takenoutcard()
            # JPDC撮影などで更新されたメニューカードと背景を更新する
            self.fix_updated_file()
            # パーティが非表示であれば表示する
            if not self.is_runningevent():
                if not self.is_showparty:
                    self.show_party()

                clip: Optional[pygame.rect.Rect] = None
                if cw.cwpy.sdata.infocards_beforeevent is not None:
                    # このブロックはイベント終了直後に一回だけ実行される
                    for _i in [i for i in cw.cwpy.sdata.get_infocards(False)
                               if i not in cw.cwpy.sdata.infocards_beforeevent]:
                        # イベント開始前には持っていなかった情報カードを入手している
                        cw.cwpy.sdata.notice_infoview = True
                        cw.cwpy.statusbar.change()
                        clip = pygame.rect.Rect(cw.cwpy.statusbar.rect)
                        break

                    clip = self.update_statusimgs(False, clip=clip)

                    cw.cwpy.sdata.infocards_beforeevent = None
                    if clip:
                        self.add_lazydraw(clip=clip)

                if self._need_disposition:
                    self.disposition_pcards()

                self._reloading = False

        self.update_groups((self.cardgrp, self.topgrp, self.sbargrp))

    def update_statusimgs(self, is_runningevent: bool,
                          clip: Optional[pygame.rect.Rect] = None) -> Optional[pygame.rect.Rect]:
        """
        キャラクターのステータス時間の表示の更新が必要であれば更新する。
        """
        if cw.cwpy.setting.show_statustime == "NotEventTime":
            clip = pygame.rect.Rect(cw.cwpy.statusbar.rect)
            for ccard in itertools.chain(cw.cwpy.get_pcards("unreversed"), cw.cwpy.get_ecards("unreversed"),
                                         cw.cwpy.get_fcards("unreversed")):
                assert isinstance(ccard, (cw.sprite.card.PlayerCard,
                                          cw.sprite.card.EnemyCard,
                                          cw.sprite.card.FriendCard))
                if ccard.is_analyzable():
                    clip2 = ccard.update_image(update_statusimg=True, is_runningevent=is_runningevent)
                    if clip2:
                        if clip:
                            clip.union_ip(clip2)
                        else:
                            clip = pygame.rect.Rect(clip2)
            return clip
        else:
            return None

    def update_groups(self, groups: Iterable[pygame.sprite.LayeredDirty]) -> None:
        self.lazy_selection = None
        pointed_tile = self.pointed_tile

        for group in groups:
            group.update()

        if pointed_tile and self.pointed_tile is None:
            self.index = -1
            self.statusbar.update_tiles()

        if not self.pointed_tile and self.lazy_selection and self.selection != self.lazy_selection:
            self.change_selection(self.lazy_selection)
        self.lazy_selection = None

    def return_takenoutcard(self, checkevent: bool = True) -> None:
        # 一時的に荷物袋から出したカードを戻す(消滅していなければ)
        if self.card_takenouttemporarily and not self.selectedheader and\
                (not checkevent or not self.is_runningevent()) and not self.is_battlestatus():
            assert self.ydata
            assert self.ydata.party
            owner = self.card_takenouttemporarily.get_owner()
            if owner and isinstance(owner, cw.character.Character) and self.sdata.party_environment_backpack:
                assert isinstance(owner, cw.sprite.card.CWPyCard)
                self.clear_inusecardimg(owner)
                header = self.card_takenouttemporarily
                self.trade("BACKPACK", header=header, from_event=False, parentdialog=None,
                           sound=False, call_predlg=False, sort=False)
                if self.card_takenouttemporarily_personal_owner:
                    index = self.card_takenouttemporarily_personal_index
                    self.card_takenouttemporarily_personal_owner.add_personalpocket(header, index)
                self.ydata.party.sort_backpack()
            self.card_takenouttemporarily = None
            self.card_takenouttemporarily_personal_owner = None
            self.card_takenouttemporarily_personal_index = -1

    def fix_updated_file(self, force: bool = False) -> None:
        # JPDC撮影などで更新されたメニューカードと背景を更新する
        if not force and (not self.is_playingscenario() or self.is_runningevent()):
            return

        if self.sdata.ex_cache:
            if self.background.use_excache:
                self.background.use_excache = False
                self.file_updates_bg = True
            for mcard in self.get_mcards():
                if not mcard.is_initialized():
                    continue
                if mcard.cardimg.use_excache:
                    self.file_updates.add(mcard)
                mcard.cardimg.use_excache = False

            for path in self.sdata.ex_cache.keys():
                path = cw.util.get_keypath(cw.util.get_symlinktarget(path))

            self.sdata.path_cache.clear()
            self.sdata.resource_cache.clear()
            self.sdata.resource_cache_size = 0

            self.sdata.ex_cache.clear()

        if self.background.pc_cache:
            self.background.pc_cache.clear()

        if self.is_curtained():
            self.background.reload_jpdcimage = True
        else:
            if self.file_updates_bg:
                self.background.reload(False)
                self.file_updates_bg = False
            elif not self.background.reload_jpdcimage and self.background.has_jpdcimage:
                self.background.reload(False, ttype=("None", 0))
            self.background.reload_jpdcimage = True

        if self.file_updates:
            for mcard in self.get_mcards("visible"):
                if mcard in self.file_updates:
                    cw.animation.animate_sprite(mcard, "hide")
                    mcard.cardimg.fix_pcimage_updated()
                    mcard.cardimg.clear_cache()
                    mcard.update_image()
                    cw.animation.animate_sprite(mcard, "deal")
            self.file_updates.clear()

    def proc_animation(self) -> None:
        if not self.animations:
            return
        if self.setting.stop_the_world_with_iconized:
            if self.frame.is_iconized:
                if self._stop_animations is None:
                    self._stop_animations = pygame.time.get_ticks()
                return
            elif self._stop_animations is not None:
                for sprite in self.animations:
                    sprite.start_animation += pygame.time.get_ticks() - self._stop_animations
                self._stop_animations = None

        removes = set()
        clip = None
        for sprite in self.animations:
            if not clip:
                clip = pygame.rect.Rect(sprite.rect)
            clip.union_ip(sprite.rect)

            if sprite.status != sprite.anitype:
                removes.add(sprite)
                continue  # アニメーション終了

            ticks = pygame.time.get_ticks()
            if ticks < sprite.start_animation:
                sprite.start_animation = ticks
            frame = int((ticks - sprite.start_animation) / 1000.0 * 60.0)
            if frame <= sprite.frame:
                continue  # フレーム進行無し
            sprite.frame = frame
            method = getattr(sprite, "update_" + sprite.status, None)
            if method:
                method()
            else:
                removes.add(sprite)
                continue  # アニメーション中止
            clip.union_ip(sprite.rect)
            if sprite.status != sprite.anitype:
                removes.add(sprite)  # アニメーション終了

        if clip:
            self.add_lazydraw(clip=clip)

        for sprite in removes:
            self.stop_animation(sprite)

    def stop_animation(self, sprite: "cw.sprite.base.CWPySprite") -> None:
        if sprite in self.animations:
            sprite.anitype = ""
            sprite.start_animation = 0.0
            sprite.frame = 0
            self.animations.remove(sprite)

    def draw_to(self, scr: pygame.surface.Surface, draw_desc: bool) -> Optional[pygame.rect.Rect]:
        clip: Optional[pygame.rect.Rect] = None
        for rect in itertools.chain(cw.sprite.background.layered_draw_ex(self.cardgrp, scr),
                                    self.topgrp.draw(scr),
                                    self.backloggrp.draw(scr)):
            if clip is None:
                clip = rect.copy()
            else:
                clip.union_ip(rect)

        for music in self.music:
            if music.movie_scr:
                scr.blit(music.movie_scr, (0, 0))
        clip2 = scr.get_clip()
        scr.set_clip(None)
        for rect in self.statusbar.layered_draw_ex(self.sbargrp, scr, draw_desc):
            if clip is None:
                clip = rect.copy()
            else:
                clip.union_ip(rect)
        scr.set_clip(clip2)

        return clip

    def lazy_draw(self) -> None:
        if self._lazy_draw:
            self.draw()

    def add_lazydraw(self, clip: pygame.rect.Rect) -> None:
        if not clip:
            return
        if self._lazy_clip:
            self._lazy_clip.union_ip(clip)
        else:
            self._lazy_clip = pygame.rect.Rect(clip)
        self._lazy_draw = True

    def stop_the_world_with_iconized(self) -> None:
        # 一時停止中はゲーム再開までブロックする
        while self.setting.stop_the_world_with_iconized and self.frame.is_iconized and self.is_running():
            self.input(inputonly=True)
            self.get_eventhandler().run()
            self.clock.tick(1000)

    def draw(self, mainloop: bool = False, clip: Optional[pygame.rect.Rect] = None) -> None:
        self.stop_the_world_with_iconized()
        if not (clip or self._lazy_draw):
            return

        if self.has_inputevent or not mainloop or self._lazy_draw:
            # SpriteGroup描画
            # FIXME: 描画領域を絞り込むと時々カードの描画中に
            #        次に表示される背景が映り込んでしまう
            if clip:
                if self._lazy_clip:
                    clip = self._lazy_clip.union_ip(clip)
            else:
                clip = self._lazy_clip
            if clip:
                if self.scr_fullscreen:
                    clip = clip.clip(self._get_fullclip())
                else:
                    clip = clip.clip(cw.s(pygame.rect.Rect(0, 0, cw.SIZE_GAME[0], cw.SIZE_GAME[1])))
            self.scr_draw.set_clip(clip)
            self.cardgrp.set_clip(clip)
            self.topgrp.set_clip(clip)
            self.backloggrp.set_clip(clip)
            self.sbargrp.set_clip(clip)

            self._lazy_clip = None
            self._lazy_draw = False

            dirty_rect = self.draw_to(self.scr_draw, True)

            simple_scale = not self.setting.smoothexpand or cw.UP_SCR % cw.UP_WIN == 0.0 or cw.UP_WIN % cw.UP_SCR == 0.0

            def update_clip(scale: float) -> pygame.rect.Rect:
                assert clip
                clx = int(clip.left * scale) - 2
                cly = int(clip.top * scale) - 2
                clw = int(clip.width * scale) + 5
                clh = int(clip.height * scale) + 5
                return pygame.rect.Rect(clx, cly, clw, clh)

            # 画面更新
            if self.scr_fullscreen:
                if simple_scale:
                    scr = pygame.transform.scale(self.scr_draw, self.scr_size)
                else:
                    scr = cw.image.smoothscale(self.scr_draw, self.scr_size)
                if clip:
                    clip2 = update_clip(self.scr_scale)
                    clip3 = pygame.rect.Rect(clip2.left + self.scr_pos[0], clip2.top + self.scr_pos[1],
                                             clip2.width, clip2.height)
                    self.scr_fullscreen.blit(scr, clip3.topleft, clip2)
                    pygame.display.update(clip3)
                else:
                    self.scr_fullscreen.blit(scr, self.scr_pos)
                    pygame.display.update()
            elif self.scr_draw != self.scr:
                if simple_scale:
                    scr = pygame.transform.scale(self.scr_draw, self.scr.get_size())
                else:
                    scr = cw.image.smoothscale(self.scr_draw, self.scr.get_size())
                if clip:
                    clip2 = update_clip(float(cw.UP_WIN) / cw.UP_SCR)
                    self.scr.blit(scr, clip2.topleft, clip2)
                    pygame.display.update(clip2)
                else:
                    self.scr.blit(scr, (0, 0))
                    pygame.display.update()
            else:
                if clip:
                    pygame.display.update(clip)
                elif dirty_rect:
                    pygame.display.update(dirty_rect)

            pos = cw.s((0, 0))
            size = cw.s(cw.SIZE_AREA)
            self.scr_draw.set_clip(pygame.rect.Rect(pos, size))
            self._update_clip()
            size = cw.s(cw.SIZE_GAME)
            self.sbargrp.set_clip(pygame.rect.Rect(pos, size))

            self.event.eventtimer = 0

    def init_fullscreenparams(self) -> None:
        """フルスクリーン表示用のパラメータを計算する。"""
        if self.scr_fullscreen:
            fsize = self.scr_fullscreen.get_size()
            ssize = cw.s(cw.SIZE_GAME)
            a = float(fsize[0]) / ssize[0]
            b = float(fsize[1]) / ssize[1]
            scale = min(a, b)
            size = (int(ssize[0] * scale), int(ssize[1] * scale))
            x = (fsize[0] - size[0]) // 2
            y = (fsize[1] - size[1]) // 2
            self.scr_size = size
            self.scr_scale = scale
            self.scr_pos = (x, y)

            ssize = cw.SIZE_GAME
            a = float(fsize[0]) / ssize[0]
            b = float(fsize[1]) / ssize[1]
            scale = min(a, b)
            # FIXME: シナリオ選択ダイアログの縦幅が画面解像度を
            #        超えてしまうので若干小さめにする
            cw.UP_WIN = scale * 0.9
            cw.UP_WIN_M = scale

        else:
            self.scr_size = self.scr.get_size()
            self.scr_scale = 1.0
            self.scr_pos = (0, 0)

    def update_fullscreenbackground(self) -> None:
        if self.scr_fullscreen:
            # 壁紙
            if self.setting.fullscreenbackgroundtype == 0:
                self.scr_fullscreen.fill((0, 0, 0))
                fname = ""
            elif self.setting.fullscreenbackgroundtype == 1:
                self.scr_fullscreen.fill((255, 255, 255))
                fname = self.setting.fullscreenbackgroundfile
            elif self.setting.fullscreenbackgroundtype == 2:
                self.scr_fullscreen.fill((255, 255, 255))
                fname = self.setting.fullscreenbackgroundfile
                fname = cw.util.find_resource(cw.util.join_paths(self.skindir, fname), self.rsrc.ext_img)
            else:
                self.scr_fullscreen.fill((0, 0, 0))
                fname = ""

            if fname:
                back = cw.util.load_image(fname, can_loaded_scaledimage=True)
                if back.get_width():
                    if self.setting.fullscreenbackgroundtype == 2:
                        back = cw.wins(back)
                    padsize = back.get_size()
                    fsize = self.scr_fullscreen.get_size()
                    for x in range(0, fsize[0], padsize[0]):
                        for y in range(0, fsize[1], padsize[1]):
                            self.scr_fullscreen.blit(back, (x, y))

            width = 16
            x = self.scr_pos[0] - width//2-1
            y = self.scr_pos[1] - width//2-1
            w = self.scr_size[0] + width+1
            h = self.scr_size[1] + width+1
            sur = pygame.surface.Surface((w, h)).convert_alpha()
            sur.fill((255, 255, 255, 192))
            self.scr_fullscreen.blit(sur, (x, y))
            self.add_lazydraw(clip=self._get_fullclip())

    def _get_fullclip(self) -> pygame.rect.Rect:
        assert self.scr_fullscreen
        return cw.win2scr_s(pygame.rect.Rect((-self.scr_pos[0], -self.scr_pos[1]), (self.scr_fullscreen.get_size())))

    def change_cursor(self, name: str = "arrow", force: bool = False) -> None:
        """マウスカーソルを変更する。
        name: 変更するマウスカーソルの名前。
        (arrow, diamond, broken_x, tri_left, tri_right, mouse)"""
        if not force and self.cursor == name:
            return

        self.cursor = name

        if self.selection and self.selection.is_statusctrl:
            name = "arrow"

        if name == "arrow":
            if 2 <= cw.dpi_level:
                # 48x48
                s = [
                  "###                                             ",
                  "####                                            ",
                  "##.##                                           ",
                  "##..##                                          ",
                  "##...##                                         ",
                  "##....##                                        ",
                  "##.....##                                       ",
                  "##......##                                      ",
                  "##.......##                                     ",
                  "##........##                                    ",
                  "##.........##                                   ",
                  "##..........##                                  ",
                  "##...........##                                 ",
                  "##............##                                ",
                  "##.............##                               ",
                  "##..............##                              ",
                  "##...............##                             ",
                  "##................##                            ",
                  "##.................##                           ",
                  "##..................##                          ",
                  "##...................##                         ",
                  "##....................##                        ",
                  "##...........############                       ",
                  "##...........#############                      ",
                  "##.......##...##                                ",
                  "##......###...##                                ",
                  "##.....#####...##                               ",
                  "##....### ##...##                               ",
                  "##...###   ##...##                              ",
                  "##..###    ##...##                              ",
                  "##.###      ##...##                             ",
                  "#####       ##...##                             ",
                  "####         ##...##                            ",
                  "###          ##...##                            ",
                  "##            ##...##                           ",
                  "              ##..###                           ",
                  "               #####                            ",
                  "               ###                              ",
                  "                                                ",
                  "                                                ",
                  "                                                ",
                  "                                                ",
                  "                                                ",
                  "                                                ",
                  "                                                ",
                  "                                                ",
                  "                                                ",
                  "                                                ",
                ]
            else:
                # 24x24
                s = [
                  "##                      ",
                  "#.#                     ",
                  "#..#                    ",
                  "#...#                   ",
                  "#....#                  ",
                  "#.....#                 ",
                  "#......#                ",
                  "#.......#               ",
                  "#........#              ",
                  "#.........#             ",
                  "#..........#            ",
                  "#......#####            ",
                  "#...#..#                ",
                  "#.####..#               ",
                  "##   #..#               ",
                  "      #..#              ",
                  "      #..#              ",
                  "       #.#              ",
                  "       ##               ",
                  "                        ",
                  "                        ",
                  "                        ",
                  "                        ",
                  "                        ",
                ]

            if self.setting.cursor_type == cw.setting.CURSOR_WHITE:
                cursor = pygame.cursors.compile(s, "#", ".", "o")
            else:
                cursor = pygame.cursors.compile(s, ".", "#", "o")
            pygame.mouse.set_cursor((len(s[0]), len(s)), (0, 0), *cursor)
            # pygame.mouse.set_cursor(*pygame.cursors.arrow)
        elif name == "diamond":
            pygame.mouse.set_cursor(*pygame.cursors.diamond)
        elif name == "broken_x":
            pygame.mouse.set_cursor(*pygame.cursors.broken_x)
        elif name == "tri_left":
            pygame.mouse.set_cursor(*pygame.cursors.tri_left)
        elif name == "tri_right":
            pygame.mouse.set_cursor(*pygame.cursors.tri_right)
        elif name == "mouse":
            if 2 <= cw.dpi_level:
                # 48x48
                s = [
                  "          ##..##  ####################          ",
                  "          ##..##  ####################          ",
                  "          ##..##  ##................##          ",
                  "          ##..##  ##................##          ",
                  "          ##..##  ##........##......##          ",
                  "          ##..##  ##.......###......##          ",
                  "     ################.....###.......##          ",
                  "   ####################...##........##          ",
                  "  ####......##......####............##          ",
                  " ###........##........###...........##          ",
                  " ##.........##.........##...#####...##          ",
                  "###.........##.........###..#####...##          ",
                  "##..........##..........##..........##          ",
                  "##..........##..........##..........##          ",
                  "##..........##..........##..#####...##          ",
                  "##..........##..........##..#####...##          ",
                  "##..........##..........##..........##          ",
                  "##..........##..........##..........##          ",
                  "##..........##..........##..........##          ",
                  "##..........##..........##..........##          ",
                  "##########################..........##          ",
                  "############..############..........##          ",
                  "##......................##..........##          ",
                  "##......................##..........##          ",
                  "##......................##..........##          ",
                  "##......................##..........##          ",
                  "##......................##..........##          ",
                  "##......................##..........##          ",
                  "##......................##############          ",
                  "##......................##############          ",
                  "##......................##                      ",
                  "##......................##                      ",
                  "###....................###                      ",
                  " ##....................##                       ",
                  " ###..................###                       ",
                  "  ###................###                        ",
                  "    ###################   ###### ####   #####   ",
                  "   ####################  ############  ######   ",
                  "  ##.....###..##  ##..####.....###..# ##..##    ",
                  " ##.......##..##  ##..###.......##..###..##     ",
                  "##...#######..##  ##..##...#######..##..##      ",
                  "##..########..##  ##..##..########.....##       ",
                  "##..########..######..##..########..##..##      ",
                  "##...#######..######..##...#######..###..##     ",
                  " ##.......##......##..###.......##..# ##..##    ",
                  "  ##.....###......##..####.....###..#  ##..##   ",
                  "   ####################  ############   #####   ",
                  "    ###### ############   ###### ####    ####   ",
                ]
                point = (14, 14)
            else:
                # 24x24
                s = [
                  "     #.# ##########     ",
                  "     #.# #........#     ",
                  "     #.# #....#...#     ",
                  "  #########..#....#     ",
                  " #....#....#......#     ",
                  "#.....#.....#.##..#     ",
                  "#.....#.....#.....#     ",
                  "#.....#.....#.##..#     ",
                  "#.....#.....#.....#     ",
                  "#.....#.....#.....#     ",
                  "######.######.....#     ",
                  "#...........#.....#     ",
                  "#...........#.....#     ",
                  "#...........#.....#     ",
                  "#...........#######     ",
                  "#...........#           ",
                  "#...........#           ",
                  " #.........#            ",
                  "  ########## ### #  #   ",
                  " #...#.# #.##...#.##.#  ",
                  "#.####.# #.#.####...#   ",
                  "#.####.###.#.####.#.#   ",
                  " #...#...#.##...#.##.#  ",
                  "  #########  ### #  #   ",
                ]
                point = (7, 7)

            if self.setting.cursor_type == cw.setting.CURSOR_WHITE:
                cursor = pygame.cursors.compile(s, "#", ".", "o")
            else:
                cursor = pygame.cursors.compile(s, ".", "#", "o")
            pygame.mouse.set_cursor((len(s[0]), len(s)), point, *cursor)

        elif name == "wait":
            if 2 <= cw.dpi_level:
                # 48x48
                s = [
                    "                                                ",
                    "                                                ",
                    "        ################################        ",
                    "        ################################        ",
                    "        #.............................##        ",
                    "        #.............................##        ",
                    "        ################################        ",
                    "        ###                          ###        ",
                    "         ##                          ##         ",
                    "         ##                          ##         ",
                    "         ##                          ##         ",
                    "          ##                        ##          ",
                    "          ##                        ##          ",
                    "           ##                      ##           ",
                    "           ##                      ##           ",
                    "            ##                    ##            ",
                    "             ##                  ##             ",
                    "             ###                ###             ",
                    "              ###              ###              ",
                    "               ###            ###               ",
                    "                ###          ###                ",
                    "                 ###        ###                 ",
                    "                   ##      ##                   ",
                    "                    ##    ##                    ",
                    "                    ##    ##                    ",
                    "                   ##      ##                   ",
                    "                 ###        ###                 ",
                    "                ###          ###                ",
                    "               ###            ###               ",
                    "              ###              ###              ",
                    "             ###                ###             ",
                    "             ## ################ ##             ",
                    "            ## .................. ##            ",
                    "           ## .................... ##           ",
                    "           ## .................... ##           ",
                    "          ## ...................... ##          ",
                    "          ## ...................... ##          ",
                    "         ## ........................ ##         ",
                    "         ## ........................ ##         ",
                    "         ## ........................ ##         ",
                    "        ###                          ###        ",
                    "        ################################        ",
                    "        ##............................##        ",
                    "        ##............................##        ",
                    "        ################################        ",
                    "        ################################        ",
                    "                                                ",
                    "                                                ",
                ]
                point = (23, 23)
            else:
                # 24x24
                s = [
                    "                        ",
                    "    ################    ",
                    "    #..............#    ",
                    "    ################    ",
                    "    #              #    ",
                    "     #            #     ",
                    "     #            #     ",
                    "      #          #      ",
                    "       #        #       ",
                    "        #      #        ",
                    "         #    #         ",
                    "          #  #          ",
                    "          #  #          ",
                    "         #    #         ",
                    "        #      #        ",
                    "       # ###### #       ",
                    "      # ........ #      ",
                    "     # .......... #     ",
                    "     # .......... #     ",
                    "    # ............ #    ",
                    "    ################    ",
                    "    #..............#    ",
                    "    ################    ",
                    "                        ",
                ]
                point = (11, 11)

            if self.setting.cursor_type == cw.setting.CURSOR_WHITE:
                cursor = pygame.cursors.compile(s, "#", ".", "o")
            else:
                cursor = pygame.cursors.compile(s, ".", "#", "o")
            pygame.mouse.set_cursor((len(s[0]), len(s)), point, *cursor)

        if not force and not self.is_showingdlg():
            # FIXME: 一度マウスポインタを移動しないと変更されない
            pos = pygame.mouse.get_pos()
            if -1 < pos[0] and -1 < pos[1] and pygame.mouse.get_focused():
                x = pos[0] - 1 if 0 < pos[0] else pos[0] + 1
                y = pos[1] - 1 if 0 < pos[1] else pos[1] + 1
                pygame.mouse.set_pos(x, y)
                pygame.mouse.set_pos(pos)

    def call_dlg(self, name: str, **kwargs: typing.Any) -> int:
        """ダイアログを開く。
        name: ダイアログ名。cw.frame参照。
        """
        if name not in self.frame.dlgeventtypes:
            cw.cwpy.call_dlg("ERROR", text="ダイアログ「%s」は存在しません。" % name)
            return 0
        self.lock_menucards = True
        stack: int = self.add_showingdlg()
        self.input(eventclear=True)
        self.statusbar.hide_touchbuttons()
        self.statusbar.clear_volumebar()
        self.add_lazydraw(clip=cw.s(pygame.rect.Rect((0, 0), cw.SIZE_GAME)))
        if self.selection and self.selection.is_statusctrl:
            # 表示が乱れる場合があるので
            # ステータスバーのボタンからフォーカスを外しておく
            self.mousepos = (-1, -1)
        self.keyevent.clear()  # キー入力初期化
        event = wx.PyCommandEvent(self.frame.dlgeventtypes[name])
        event.args = kwargs
        if threading.currentThread() == self:
            if not self.frame:
                return stack

            def func() -> None:
                # BUG: シナリオインストールダイアログを開いたあとで
                #      フィルタイベントの挙動がおかしくなる
                pass
            self.frame.exec_func(func)
            self.frame.AddPendingEvent(event)
        else:
            # BUG: シナリオインストールダイアログを開いたあとで
            #      フィルタイベントの挙動がおかしくなる
            self.frame.ProcessEvent(event)
        return stack

    def call_modaldlg(self, name: str, **kwargs: typing.Any) -> None:
        """ダイアログを開き、閉じるまで待機する。
        name: ダイアログ名。cw.frame参照。
        """
        stack = self.call_dlg(name, **kwargs)

        if threading.currentThread() == self:
            while self.is_running() and stack < self._showingdlg:
                self.main_loop(False)

    def call_predlg(self) -> None:
        """直前に開いていたダイアログを再び開く。"""
        self.return_takenoutcard(checkevent=False)
        if self.pre_dialogs:
            pre_info = self.pre_dialogs[-1]
            callname = pre_info[0]

            if 0 <= self.areaid and self.is_playingscenario() and callname in ("CARDPOCKET", "CARDPOCKETB"):
                # ゲームオーバーになった場合は開かない
                if cw.cwpy.is_gameover() and cw.cwpy.sdata.can_gameover():
                    self.pre_dialogs.pop()
                    return

                # 手札カードダイアログの選択者が行動不能か
                # 対象消去されている場合は開かない
                index2 = pre_info[1]
                if isinstance(index2, cw.character.Character) and\
                        (index2.is_vanished() or ((not index2.is_active() and self.areaid not in cw.AREAS_TRADE))):
                    self.pre_dialogs.pop()
                    self.lock_menucards = False
                    return

            self.update_statusimgs(is_runningevent=False)
            self.call_modaldlg(callname)

        else:
            self.lock_menucards = False

    def kill_showingdlg(self) -> None:
        if self._kill_showingdlg() <= 0:
            if not self.is_runningevent():
                self.exec_func(self.clear_selection)

    @synclock(_dlg_mutex)
    def _kill_showingdlg(self) -> int:
        oldval = self._showingdlg
        self._showingdlg -= 1
        return oldval

    @synclock(_dlg_mutex)
    def add_showingdlg(self) -> int:
        oldval = self._showingdlg
        self._showingdlg += 1
        return oldval

    @typing.overload
    def exec_func(self, func: Callable[[], None]) -> None: ...

    @typing.overload
    def exec_func(self, func: Callable[[_Arg1], None], arg1: _Arg1) -> None: ...

    @typing.overload
    def exec_func(self, func: Callable[[_Arg1, _Arg2], None], arg1: _Arg1, arg2: _Arg2) -> None: ...

    @typing.overload
    def exec_func(self, func: Callable[[_Arg1, _Arg2, _Arg3], None],
                  arg1: _Arg1, arg2: _Arg2, arg3: _Arg3) -> None: ...

    @typing.overload
    def exec_func(self, func: Callable[[_Arg1, _Arg2, _Arg3, _Arg4], None],
                  arg1: _Arg1, arg2: _Arg2, arg3: _Arg3, arg4: _Arg4) -> None: ...

    @typing.overload
    def exec_func(self, func: Callable[[_Arg1, _Arg2, _Arg3, _Arg4, _Arg5], None],
                  arg1: _Arg1, arg2: _Arg2, arg3: _Arg3, arg4: _Arg4, arg5: _Arg5) -> None: ...

    @typing.overload
    def exec_func(self, func: Callable[[_Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6], None],
                  arg1: _Arg1, arg2: _Arg2, arg3: _Arg3, arg4: _Arg4, arg5: _Arg5, arg6: _Arg6) -> None: ...

    @typing.overload
    def exec_func(self, func: Callable[[_Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7], None],
                  arg1: _Arg1, arg2: _Arg2, arg3: _Arg3, arg4: _Arg4, arg5: _Arg5, arg6: _Arg6,
                  arg7: _Arg7) -> None: ...

    @typing.overload
    def exec_func(self, func: Callable[[_Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7, _Arg8], None],
                  arg1: _Arg1, arg2: _Arg2, arg3: _Arg3, arg4: _Arg4, arg5: _Arg5, arg6: _Arg6, arg7: _Arg7,
                  arg8: _Arg8) -> None: ...

    @typing.overload
    def exec_func(self, func: Callable[[_Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7, _Arg8, _Arg9], None],
                  arg1: _Arg1, arg2: _Arg2, arg3: _Arg3, arg4: _Arg4, arg5: _Arg5, arg6: _Arg6, arg7: _Arg7,
                  arg8: _Arg8, arg9: _Arg9) -> None: ...

    def exec_func(self, func: Callable[..., None], *args: typing.Any, **kwargs: typing.Any) -> None:
        """CWPyスレッドで指定したファンクションを実行する。
        func: 実行したいファンクションオブジェクト。
        """
        event = pygame.event.Event(pygame.USEREVENT, func=func, args=args,
                                   kwargs=kwargs)
        post_pygameevent(event)

    @typing.overload
    def force_exec_func(self, func: Callable[[], None]) -> None: ...

    @typing.overload
    def force_exec_func(self, func: Callable[[_Arg1], None], arg1: _Arg1) -> None: ...

    @typing.overload
    def force_exec_func(self, func: Callable[[_Arg1, _Arg2], None], arg1: _Arg1, arg2: _Arg2) -> None: ...

    @typing.overload
    def force_exec_func(self, func: Callable[[_Arg1, _Arg2, _Arg3], None],
                        arg1: _Arg1, arg2: _Arg2, arg3: _Arg3) -> None: ...

    @typing.overload
    def force_exec_func(self, func: Callable[[_Arg1, _Arg2, _Arg3, _Arg4], None],
                        arg1: _Arg1, arg2: _Arg2, arg3: _Arg3, arg4: _Arg4) -> None: ...

    @typing.overload
    def force_exec_func(self, func: Callable[[_Arg1, _Arg2, _Arg3, _Arg4, _Arg5], None],
                        arg1: _Arg1, arg2: _Arg2, arg3: _Arg3, arg4: _Arg4, arg5: _Arg5) -> None: ...

    @typing.overload
    def force_exec_func(self, func: Callable[[_Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6], None],
                        arg1: _Arg1, arg2: _Arg2, arg3: _Arg3, arg4: _Arg4, arg5: _Arg5, arg6: _Arg6) -> None: ...

    @typing.overload
    def force_exec_func(self, func: Callable[[_Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7], None],
                        arg1: _Arg1, arg2: _Arg2, arg3: _Arg3, arg4: _Arg4, arg5: _Arg5, arg6: _Arg6,
                        arg7: _Arg7) -> None: ...

    @typing.overload
    def force_exec_func(self, func: Callable[[_Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7, _Arg8], None],
                        arg1: _Arg1, arg2: _Arg2, arg3: _Arg3, arg4: _Arg4, arg5: _Arg5, arg6: _Arg6, arg7: _Arg7,
                        arg8: _Arg8) -> None: ...

    @typing.overload
    def force_exec_func(self, func: Callable[[_Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7, _Arg8, _Arg9], None],
                        arg1: _Arg1, arg2: _Arg2, arg3: _Arg3, arg4: _Arg4, arg5: _Arg5, arg6: _Arg6, arg7: _Arg7,
                        arg8: _Arg8, arg9: _Arg9) -> None: ...

    def force_exec_func(self, func: Callable[..., None], *args: typing.Any, **kwargs: typing.Any) -> None:
        """CWPyスレッドで指定したファンクションを実行する。
        ファンクションはゲームのイベント処理の間に割り込んで実行される。
        func: 実行したいファンクションオブジェクト。
        """
        event = pygame.event.Event(cw.FORCE_USEREVENT, func=func, args=args, kwargs=kwargs)
        post_pygameevent(event)

    @typing.overload
    def sync_exec(self, func: Callable[[], _Result]) -> _Result: ...

    @typing.overload
    def sync_exec(self, func: Callable[[_Arg1], _Result], arg1: _Arg1) -> _Result: ...

    @typing.overload
    def sync_exec(self, func: Callable[[_Arg1, _Arg2], _Result], arg1: _Arg1, arg2: _Arg2) -> _Result: ...

    @typing.overload
    def sync_exec(self, func: Callable[[_Arg1, _Arg2, _Arg3], _Result],
                  arg1: _Arg1, arg2: _Arg2, arg3: _Arg3) -> _Result: ...

    @typing.overload
    def sync_exec(self, func: Callable[[_Arg1, _Arg2, _Arg3, _Arg4], _Result],
                  arg1: _Arg1, arg2: _Arg2, arg3: _Arg3, arg4: _Arg4) -> _Result: ...

    @typing.overload
    def sync_exec(self, func: Callable[[_Arg1, _Arg2, _Arg3, _Arg4, _Arg5], _Result],
                  arg1: _Arg1, arg2: _Arg2, arg3: _Arg3, arg4: _Arg4, arg5: _Arg5) -> _Result: ...

    @typing.overload
    def sync_exec(self, func: Callable[[_Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6], _Result],
                  arg1: _Arg1, arg2: _Arg2, arg3: _Arg3, arg4: _Arg4, arg5: _Arg5, arg6: _Arg6) -> _Result: ...

    @typing.overload
    def sync_exec(self, func: Callable[[_Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7], _Result],
                  arg1: _Arg1, arg2: _Arg2, arg3: _Arg3, arg4: _Arg4, arg5: _Arg5, arg6: _Arg6,
                  arg7: _Arg7) -> _Result: ...

    @typing.overload
    def sync_exec(self, func: Callable[[_Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7, _Arg8], _Result],
                  arg1: _Arg1, arg2: _Arg2, arg3: _Arg3, arg4: _Arg4, arg5: _Arg5, arg6: _Arg6, arg7: _Arg7,
                  arg8: _Arg8) -> _Result: ...

    @typing.overload
    def sync_exec(self, func: Callable[[_Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7, _Arg8, _Arg9], _Result],
                  arg1: _Arg1, arg2: _Arg2, arg3: _Arg3, arg4: _Arg4, arg5: _Arg5, arg6: _Arg6, arg7: _Arg7,
                  arg8: _Arg8, arg9: _Arg9) -> _Result: ...

    def sync_exec(self, func: Callable[..., _Result], *args: typing.Any, **kwargs: typing.Any) -> Optional[_Result]:
        """CWPyスレッドで指定したファンクションを実行し、
        終了を待ち合わせる。ファンクションの戻り値を返す。
        func: 実行したいファンクションオブジェクト。
        """
        if threading.currentThread() == self:
            return func(*args, **kwargs)
        else:
            result: List[Optional[_Result]] = [None]

            class Running(object):
                def __init__(self) -> None:
                    self.isrun = True
            running = Running()

            def func2(running: Running, result: List[Optional[_Result]], func: Callable[..., _Result],
                      *args: typing.Any, **kwargs: typing.Any) -> None:
                result[0] = func(*args, **kwargs)
                running.isrun = False
            self.exec_func(func2, running, result, func, *args, **kwargs)
            while running.isrun and self.frame.IsEnabled() and self.is_running():
                time.sleep(0.001)
            return result[0]

    def set_expanded(self, flag: bool, expandmode: str = "", force: bool = False,
                     displaysize: Optional[Tuple[int, int]] = None) -> None:
        """拡大表示する。すでに拡大表示されている場合は解除する。
        flag: Trueなら拡大表示、Falseなら解除。
        """
        if not force and self.is_expanded() == flag:
            return

        if not expandmode:
            expandmode = self.expand_mode if self.is_expanded() else self.setting.expandmode

        if displaysize is None and expandmode == "FullScreen" and flag:
            def func() -> None:
                dsize = self.frame.get_displaysize()
                self.exec_func(self.set_expanded, flag, expandmode, force, dsize)
            self.frame.exec_func(func)
            return

        updatedrawsize = force or self.setting.expanddrawing != 1.0

        if expandmode == "None":
            if force:
                self.set_fullscreen(False)
                self.expand_mode = "None"
                self.setting.is_expanded = False
                cw.UP_WIN = 1.0
                cw.UP_WIN_M = cw.UP_WIN
                self.update_scale(1, True, False, updatedrawsize)
                self.clear_inputevents()
            else:
                return

        elif expandmode == "FullScreen":
            # フルスクリーン
            if self.is_showingdebugger() and flag:
                self.play_sound("error")
                s = "デバッガ表示中はフルスクリーン化できません。"
                self.call_modaldlg("MESSAGE", text=s)
            else:
                if not self.is_showingdlg():
                    pos = pygame.mouse.get_pos()
                    if -1 < pos[0] and -1 < pos[1] and pygame.mouse.get_focused():
                        pygame.mouse.set_pos([-1, -1])

                self.setting.is_expanded = flag
                if flag:
                    assert displaysize is not None
                    self.expand_mode = expandmode
                    dsize = displaysize
                    self.scr_fullscreen = pygame.display.set_mode((dsize[0], dsize[1]), 0)
                    self.scr = pygame.surface.Surface(cw.s(cw.SIZE_GAME)).convert()
                    self.scr_draw = self.scr
                    self.set_fullscreen(True)
                    self.update_scale(self.setting.expanddrawing, True, False, updatedrawsize)
                else:
                    cw.UP_WIN = 1.0
                    cw.UP_WIN_M = cw.UP_WIN
                    self.expand_mode = "None"
                    self.scr_fullscreen = None
                    self.scr = pygame.display.set_mode(cw.wins(cw.SIZE_GAME), 0)
                    self.scr_draw = self.scr
                    self.set_fullscreen(False)
                    self.update_scale(1, True, False, updatedrawsize)

                while not self.frame.IsFullScreen() == flag:
                    pass

                if not self.is_showingdlg():
                    # 一度マウスポインタを画面外へ出さないと
                    # フォーカスを失うことがある
                    if -1 < pos[0] and -1 < pos[1] and pygame.mouse.get_focused():
                        pygame.mouse.set_pos(pos)
                    self.clear_inputevents()

        else:
            # 拡大
            try:
                def func() -> None:
                    self.set_fullscreen(False)
                self.frame.exec_func(func)
                scale = float(expandmode)
                scale = max(scale, 0.5)
                self.setting.is_expanded = flag
                if flag:
                    self.expand_mode = expandmode
                    cw.UP_WIN = scale
                    cw.UP_WIN_M = cw.UP_WIN
                    self.update_scale(self.setting.expanddrawing, True, False, updatedrawsize)
                else:
                    self.expand_mode = "None"
                    cw.UP_WIN = 1.0
                    cw.UP_WIN_M = cw.UP_WIN
                    self.update_scale(1, True, False, updatedrawsize)
                self.clear_inputevents()

            except Exception:
                cw.util.print_ex()

        self.has_inputevent = True

    def show_message(self, mwin: "cw.sprite.message.MessageWindow") -> int:
        """MessageWindowを表示し、次コンテントのindexを返す。
        mwin: MessageWindowインスタンス。
        """
        eventhandler = cw.eventhandler.EventHandlerForMessageWindow(mwin)
        ie = self.interrupt_eventhandler
        self.interrupt_eventhandler = eventhandler
        self.clear_selection()
        locks = self.lock_menucards
        self.lock_menucards = False

        if self.is_showingdebugger() and self.event and self.event.is_stepexec():
            self.event.refresh_tools()

        self.statusbar.update_tiles()
        self.statusbar.change(False)
        waited = False
        clickable_sprites = self.topgrp.remove_sprites_of_layer(cw.LAYER_CLICKABLE_SPRITES)
        try:
            self.event.refresh_activeitem()
            self.input()
            is_drawing = True
            while self.is_running() and mwin.result is None:
                self.input()
                eventhandler.run()
                if not (self.is_running() and mwin.result is None):
                    break
                self.update()
                if mwin.result is None and is_drawing:
                    is_drawing = mwin.is_drawing

                self.wait_frame(1)
                waited = True
        finally:
            if clickable_sprites:
                cw.add_layer(self.topgrp, *clickable_sprites, layer=cw.LAYER_CLICKABLE_SPRITES)
            self.interrupt_eventhandler = ie

        if not waited:
            self.wait_frame(1)

        self.clear_selection()
        self.lock_menucards = locks

        # バックログの保存
        if self.is_playingscenario() and self.setting.backlogmax and isinstance(mwin.result, int) and\
                not isinstance(mwin, cw.sprite.message.MemberSelectWindow):
            if self.setting.backlogmax <= len(self.sdata.backlog):
                self.sdata.backlog.pop(0)
            self.sdata.backlog.append(cw.sprite.message.BacklogData(mwin))

        self.advlog.show_message(mwin)

        self.statusbar.change(False)

        # cwpylist, index 初期化
        self.list = []
        self.list.extend(self.get_mcards("selectable"))
        self.index = -1
        # スプライト削除
        seq = []
        seq.extend(self.cardgrp.remove_sprites_of_layer(cw.layer_val(cw.LAYER_MESSAGE)))
        seq.extend(self.cardgrp.remove_sprites_of_layer(cw.layer_val(cw.LAYER_SPMESSAGE)))
        seq.extend(self.cardgrp.remove_sprites_of_layer(cw.layer_val(cw.LAYER_SELECTIONBAR_1)))
        seq.extend(self.cardgrp.remove_sprites_of_layer(cw.layer_val(cw.LAYER_SPSELECTIONBAR_1)))
        seq.extend(self.cardgrp.remove_sprites_of_layer(cw.layer_val(cw.LAYER_SELECTIONBAR_2)))
        seq.extend(self.cardgrp.remove_sprites_of_layer(cw.layer_val(cw.LAYER_SPSELECTIONBAR_2)))
        seq.extend(self.sbargrp.remove_sprites_of_layer(cw.sprite.statusbar.LAYER_MESSAGE))

        # 互換性マーク削除
        if self.is_playingscenario():
            self.sdata.set_versionhint(cw.HINT_MESSAGE, None)

        # 次のアニメーションの前に再描画を行う
        for sprite in seq:
            assert sprite.rect is not None
            self.add_lazydraw(sprite.rect)

        # メッセージ表示中にシナリオ強制終了(F9)などを行った場合、
        # イベント強制終了用のエラーを送出する。
        assert mwin.result is not None
        if isinstance(mwin.result, Exception):
            raise mwin.result
        else:
            return mwin.result

    def has_backlog(self) -> bool:
        """表示可能なメッセージログがあるか。"""
        return bool(self.sdata.backlog)

    def show_backlog(self, n: int = 0) -> None:
        """直近から過去に遡ってn回目のメッセージを表示する。
        n: 遡る量。0なら最後に閉じたメッセージ。
        もっとも古いメッセージよりも大きな値の場合は
        もっとも古いメッセージを表示する。
        """
        if not self.has_backlog():
            return

        length = len(self.sdata.backlog)
        if length <= n:
            n = length - 1
        index = length - 1 - n

        eventhandler = cw.eventhandler.EventHandlerForBacklog(self.sdata.backlog, index)
        ie = self.interrupt_eventhandler
        self.interrupt_eventhandler = eventhandler
        cursor = self.cursor
        self.change_cursor()
        self._log_handler = eventhandler
        try:
            while self.is_running() and eventhandler.is_showing() and\
                    cw.cwpy.sdata.is_playing and self._is_showingbacklog:
                self.update_groups((self.sbargrp,))
                eventhandler.stw.is_waiting()
                self.wait_frame(1)
                self.input()
                eventhandler.run()
                if len(self.sdata.backlog) < length:
                    # 最大数の設定変更によりログ数が減った場合
                    if not self.sdata.backlog:
                        break
                    eventhandler.index -= length - len(self.sdata.backlog)
                    length = len(self.sdata.backlog)
                    if eventhandler.index < 0:
                        eventhandler.index = 0
                    eventhandler.update_sprites()
        finally:
            self._log_handler = None
            self.interrupt_eventhandler = ie
            self.change_cursor(cursor)
            # 表示終了
            eventhandler.exit_backlog(playsound=False)

    def set_backlogmax(self, backlogmax: int) -> None:
        """メッセージログの最大数を設定する。
        """
        self.setting.backlogmax = backlogmax
        if not self.has_backlog():
            return
        if backlogmax < len(self.sdata.backlog):
            del self.sdata.backlog[0:len(self.sdata.backlog)-backlogmax]

    def set_titlebar(self, s: str) -> None:
        """タイトルバーテキストを設定する。
        s: タイトルバーテキスト。
        """
        self.frame.exec_func(self.frame.SetTitle, s)

    def get_yesnoresult(self) -> int:
        """call_yesno()の戻り値を取得する。"""
        return self._yesnoresult

# ------------------------------------------------------------------------------
# ゲーム状態遷移用メソッド
# ------------------------------------------------------------------------------

    def set_status(self, name: str) -> None:
        isbattle = "ScenarioBattle" in (name, self.status)
        quickhide = (self.setting.all_quickdeal and not isbattle)
        self.status = name
        force_dealspeed = self.force_dealspeed
        if quickhide:
            self.force_dealspeed = self.setting.dealspeed if self.setting.quickdeal else -1
        try:
            self.hide_cards(True, quickhide=quickhide)
        finally:
            self.force_dealspeed = force_dealspeed
        self.pre_areaids = []
        self.pre_dialogs = []
        self.pre_mcards = []

    def startup(self, loadyado: bool = True) -> None:
        """起動時のアニメーションを表示してから
        タイトル画面へ遷移する。"""
        seq = []
        for event in self.events:
            if event.type in (pygame.USEREVENT, cw.FORCE_USEREVENT):
                seq.append(event)
        self.events = seq
        self.cut_animation = False
        self.wait_showcards = False
        self.sdata = cw.data.SystemData()

        # 必要なスプライトの読み込み
        if not self._init_resources():
            self._running = False
            return

        for music in self.music:
            music.stop()

        optyado = cw.OPTIONS.getstr("yado")
        cw.OPTIONS.setstr("yado", "")
        if not optyado and self.setting.startupscene == cw.setting.OPEN_LAST_BASE and loadyado:
            optyado = self.setting.lastyado
            cw.OPTIONS.setstr("party", "")
            cw.OPTIONS.setstr("scenario", "")

        if optyado:
            if os.path.isabs(optyado):
                optyado = cw.util.relpath(optyado, "Yado")
            optyado = cw.util.join_paths("Yado", optyado)
            env = cw.util.join_paths(optyado, "Environment.xml")
            if os.path.isfile(env):
                self.set_status("Title")
                self._init_attrs()
                if self.load_yado(optyado):
                    cw.OPTIONS.setstr("force_skin", "")
                    return
                else:
                    name = cw.header.GetName(env).name
                    s = "「%s」は他の起動で使用中です。" % (name)
                    self.call_modaldlg("MESSAGE", text=s)

        # 起動オプションでの宿の指定に失敗した場合は
        # これらのオプションは無効
        cw.OPTIONS.setstr("party", "")
        cw.OPTIONS.setstr("scenario", "")
        cw.OPTIONS.setstr("force_skin", "")

        self.statusbar.change()

        try:
            fpath = cw.util.join_paths(self.skindir, "Resource/Xml/Animation/Opening.xml")
            anime = cw.sprite.animationcell.AnimationCell(fpath, cw.SIZE_AREA, (0, 0), self.topgrp, cw.LAYER_TITLE)
            cw.animation.animate_sprite(anime, "animation", clearevent=False)

            # スプライトを解除する
            self.topgrp.remove_sprites_of_layer(cw.LAYER_TITLE)

            if self.cut_animation:
                ttype = ("Default", "Default")
                self.cut_animation = False
            else:
                ttype = ("None", "None")
                self.wait_showcards = True
            self.set_title(ttype=ttype)

        except cw.event.EffectBreakError:
            # 他のスキンへの切り替えなどで中止
            self.topgrp.remove_sprites_of_layer(cw.LAYER_TITLE)

    def set_title(self, init: bool = True, ttype: Tuple[str, Union[str, int]] = ("Default", "Default")) -> None:
        """タイトル画面へ遷移。"""
        del self.pre_dialogs[:]
        del self.pre_areaids[:]
        if self.ydata and self.ydata.losted_sdata:
            self.ydata.losted_sdata.end(failure=True)
            self.ydata.losted_sdata = None
            self.load_party(None, chgarea=False)
        elif self.ydata and self.ydata.party:
            self.load_party(None, chgarea=False)
        if isinstance(self.sdata, cw.data.SystemData):
            self.sdata.save_variables()
        self.set_status("Title")
        self._init_attrs()
        self.update_titlebar()
        self.statusbar.change()
        self.change_area(1, ttype=ttype)

    def _init_attrs(self) -> None:
        self.background.clear_background()
        self.stop_allsounds()
        cw.util.remove_temp()
        self.yadodir = ""
        self.tempdir = ""
        self.setting.scenario_narrow = ""
        self.setting.lastscenario = []
        self.setting.lastscenariopath = ""
        self.setting.lastfindresult = []
        self.ydata = None
        self.sdata = cw.data.SystemData()
        cw.tempdir = cw.tempdir_init
        cw.util.release_mutex()

    def set_yado(self) -> None:
        """宿画面へ遷移。"""
        # ゲームオーバーしたパーティの破棄処理を行う
        if self.ydata and self.ydata.losted_sdata:
            assert self.ydata.party
            self.ydata.party.lost()
            self.ydata.losted_sdata.end()
            self.ydata.losted_sdata = None
            self.load_party(None, chgarea=False)

        self.set_status("Yado")
        assert self.ydata
        self.sdata.sleep_timekeeper()
        self.background.clear_background()
        msglog = self.sdata.backlog
        debuglog = self.sdata.debuglog
        self.sdata = cw.data.SystemData()
        self.sdata.load_variables()
        self.sdata.backlog = msglog
        self.sdata.debuglog = debuglog
        self.sdata.notice_debuglog = 1
        self.update_titlebar()
        # 冒険の中断やF9時のためにカーテン消去
        self.clear_curtain()
        self.statusbar.change()

        if self.ydata.party:
            # パーティを選択中
            areaid = 2
            self.ydata.party.remove_numbercoupon()
            for pcard in self.get_pcards():
                pcard.clear_action()

            # 全員対象消去による戦闘の敗北から
            # シナリオクリアへ直結した場合の処置
            if not self.ydata.party.members:
                self.dissolve_party()
                areaid = 1

        elif not self.ydata.is_empty() or self.ydata.is_changed():
            # パーティを選択中でない
            areaid = 1
        else:
            # 初期状態
            areaid = 3

        def change_area() -> None:
            self.change_area(areaid, force_updatebg=True)
            self.is_pcardsselectable = bool(self.ydata and self.ydata.party)

        if self.ydata.skindirname != cw.cwpy.setting.skindirname:
            self.update_skin(self.ydata.skindirname, changearea=False, switch_yado=True, afterfunc=change_area)
        else:
            change_area()
        self.ydata._loading = False

    def start_scenario(self) -> None:
        """
        シナリオ選択ダイアログで選択されたシナリオがあればスタートする。
        """
        if self.selectedscenario:
            self.set_scenario(self.selectedscenario, manualstart=True)
            self.selectedscenario = None

    def set_scenario(self, header: Optional["cw.header.ScenarioHeader"] = None,
                     lastscenario: Optional[List[str]] = None, lastscenariopath: str = "", resume: bool = False,
                     manualstart: bool = False) -> None:
        """シナリオ画面へ遷移。
        header: ScenarioHeader
        """
        if lastscenario is None:
            lastscenario = []
        assert self.ydata
        if self.battle:
            # バトルエリア解除の時
            self.is_processing = True
        self.set_status("Scenario")
        self.battle = None

        if self.ydata.skindirname != cw.cwpy.setting.skindirname:
            lastscenario2 = lastscenario

            def func() -> None:
                self._set_scenario_impl(header, lastscenario2, lastscenariopath, resume, manualstart)
            self.update_skin(self.ydata.skindirname, changearea=False, afterfunc=func)
        else:
            self._set_scenario_impl(header, lastscenario, lastscenariopath, resume, manualstart)

    def _set_scenario_impl(self, header: Optional["cw.header.ScenarioHeader"], lastscenario: List[str],
                           lastscenariopath: str, resume: bool, manualstart: bool) -> None:
        if header and not isinstance(self.sdata, cw.data.ScenarioData):
            def load_failure(showerror: bool) -> None:
                # 読込失敗(帰還)
                self.is_processing = False
                if showerror:
                    s = "シナリオの読み込みに失敗しました。"
                    self.call_modaldlg("ERROR", text=s)
                if isinstance(self.sdata, cw.data.ScenarioData):
                    self.sdata.end(failure=True)
                self.set_yado()
                if self.is_showingdebugger() and self.event:
                    self.event.refresh_variablelist()
            try:
                assert self.ydata
                assert self.ydata.party
                if isinstance(self.sdata, cw.data.SystemData) and not self.ydata.is_loading():
                    self.sdata.save_variables()
                self.sdata = cw.data.ScenarioData(header)
                self.ydata.changed()
                self.statusbar.change(False)

                if not resume:
                    for pcard in self.get_pcards():
                        pcard.remove_timedcoupons()
                        pcard.set_fullrecovery()
                        pcard.update_image()

                loaded, musicpaths = self.sdata.set_log(force_create=not resume)
                self.sdata.start()
                self.update_titlebar()
                areaid = self.sdata.startid
                if lastscenario or lastscenariopath:
                    self.ydata.party.set_lastscenario(lastscenario, lastscenariopath)

                def func(loaded: bool, musicpaths: Optional[Iterable[Tuple[str, int, int, bool, str]]],
                         areaid: int) -> None:
                    assert self.ydata
                    assert header
                    self.is_processing = False
                    quickdeal = resume and self.setting.all_quickdeal
                    try:
                        assert isinstance(self.sdata, cw.data.ScenarioData)
                        name = self.sdata.get_areaname(self.sdata.startid)
                        if name is None:
                            if resume:
                                # 再開時に読込失敗
                                load_failure(True)
                                return
                            # 開始エリアが存在しない(帰還)
                            s = "シナリオに開始エリアが設定されていません。"
                            self.call_modaldlg("ERROR", text=s)
                            self.check_level(True)
                            self.sdata.end(failure=True)
                            self.set_yado()
                            return

                        if manualstart and not resume:
                            assert self.sdata.summary
                            dataversion = self.sdata.summary.getattr(".", "dataVersion", "")
                            if dataversion not in cw.SUPPORTED_WSN:
                                s = "対応していないWSNバージョン(%s)のシナリオです。\n正常に動作しない可能性がありますが、開始しますか？" % (dataversion)
                                self.call_modaldlg("YESNO", text=s)
                                if self.get_yesnoresult() != wx.ID_OK:
                                    self.sdata.end(failure=True)
                                    self.set_yado()
                                    return

                        if musicpaths:
                            for i, (musicpath, _subvolume, _loopcount, inusecard, fullpath) in enumerate(musicpaths):
                                music = self.music[i]
                                if music.path != music.get_path(musicpath, inusecard):
                                    music.stop()

                        if resume:
                            self.sdata.resume_timekeeper()
                        else:
                            bill = cw.sprite.bill.Bill(header)
                            if cw.cwpy.setting.display_bill_in_messagelog:
                                self.sdata.backlog.append(bill)
                                self.sdata.start_timekeeper()

                        force_dealspeed = self.force_dealspeed
                        if quickdeal:
                            self.force_dealspeed = self.setting.dealspeed if self.setting.quickdeal else -1
                        try:
                            self.change_area(areaid, not loaded, loaded, quickdeal=quickdeal, doanime=not resume)
                        finally:
                            self.force_dealspeed = force_dealspeed

                        if musicpaths:
                            for i, (musicpath, subvolume, loopcount, inusecard, fullpath) in enumerate(musicpaths):
                                music = self.music[i]
                                music.play(musicpath, subvolume=subvolume, loopcount=loopcount, inusecard=inusecard,
                                           fullpath=fullpath)

                        if self.is_showingdebugger() and self.event:
                            self.event.refresh_variablelist()

                        clip = cw.cwpy.update_statusimgs(is_runningevent=False)
                        if clip:
                            cw.cwpy.add_lazydraw(clip=clip)

                        if not self.setting.lastscenariopath:
                            self.setting.lastscenariopath = header.get_fpath()
                        self.ydata._loading = False

                    except CWPyRunningError:
                        raise

                    except Exception:
                        # 読込失敗(帰還)
                        cw.util.print_ex()
                        self.exec_func(load_failure, True)
                self.clear_inputevents()
                self.exec_func(func, loaded, musicpaths, areaid)
            except cw.event.EffectBreakError:
                # 手動で中止
                if not self.is_runningstatus():
                    return
                self.exec_func(load_failure, False)
            except Exception:
                if not self.is_runningstatus():
                    return
                # 読込失敗(帰還)
                cw.util.print_ex()
                self.exec_func(load_failure, True)
        elif self.is_processing:
            self.statusbar.change(False)
            self.is_processing = False

        self.is_pcardsselectable = bool(self.ydata and self.ydata.party)

    def set_battle(self) -> None:
        """シナリオ戦闘画面へ遷移。"""
        self.set_status("ScenarioBattle")

    def set_gameover(self) -> None:
        """ゲームオーバー画面へ遷移。"""
        assert self.ydata
        assert isinstance(self.sdata, cw.data.ScenarioData)
        self.advlog.gameover()
        self.sdata.sleep_timekeeper()
        self.hide_party()
        self.set_status("GameOver")
        del self.pre_dialogs[:]
        del self.pre_areaids[:]
        self._gameover = False
        self._forcegameover = False
        self.battle = None
        self.card_takenouttemporarily = None
        self.clear_inputevents()
        pygame.event.clear((MOUSEBUTTONDOWN, MOUSEBUTTONUP, KEYDOWN, KEYUP, USEREVENT))
        if self._need_disposition:
            self.disposition_pcards()

        del self.sdata.friendcards[:]
        self.sdata.sleep_timekeeper()

        self.stop_allsounds()

        self.ydata.losted_sdata = self.sdata
        self.sdata = cw.data.SystemData()
        if isinstance(self.sdata, cw.data.SystemData):
            self.sdata.load_variables()
        assert self.ydata.losted_sdata
        self.sdata.backlog = self.ydata.losted_sdata.backlog
        self.update_titlebar()
        self.statusbar.change()
        self.change_area(1, nocheckvisible=True)

    def set_gameoverstatus(self, gameover: bool, force: bool = True) -> None:
        """パーティの状態に係わらず
        現状のゲームオーバー状態を設定する。
        """
        self._gameover = gameover
        if force:
            self._forcegameover = gameover

    def stop_allsounds(self, skinfileonly: bool = False) -> None:
        for music in self.music:
            if not skinfileonly or cw.util.join_paths(music.fpath).startswith("Data/Skin/"):
                music.stop()
        for i in range(len(self.lastsound_scenario)):
            sound = self.lastsound_scenario[i]
            if sound:
                if not skinfileonly or cw.util.join_paths(sound.get_path()).startswith("Data/Skin/"):
                    sound.stop(True)
                    self.lastsound_scenario[i] = None

    def f9(self, load_failure: bool = False, loadyado: bool = False) -> None:
        """cw.data.ScenarioDataのf9()から呼び出され、
        緊急避難処理の続きを行う。
        """
        if not load_failure and not self.is_playingscenario():
            return
        if self.sdata.in_endprocess:
            return

        self.clean_specials()

        def func() -> None:
            if cw.cwpy.is_runningevent():
                self.exec_func(self._f9impl, False, loadyado)
                raise cw.event.EffectBreakError()
            else:
                self._f9impl(False, loadyado)
        self.exec_func(func)

    def _f9impl(self, startotherscenario: bool = False, loadyado: bool = False) -> None:
        assert self.ydata
        assert self.ydata.party
        if self.sdata.in_endprocess:
            return

        cw.fsync.sync()

        self.sdata.in_endprocess = True

        self.advlog.f9()
        self.sdata.sleep_timekeeper()
        self.sdata.is_playing = False
        self.statusbar.change(False)
        self.pre_dialogs = []

        self.clear_inusecardimg()
        self.clear_guardcardimg()
        self.statusbar.change(False)
        self.return_takenoutcard(checkevent=False)

        # 対象選択画面でF9しても、中止ボタンを宿まで持ち越さないように
        self.selectedheader = None

        # 特殊文字の辞書が変更されていたら、元に戻す
        if self.rsrc.specialchars_is_changed:
            self.rsrc.specialchars = self.rsrc.get_specialchars()

        # battle
        if self.battle and self.battle.is_running:
            # バトルを強制終了
            self.battle.end(True, True)
            self.battle = None

        # party copy
        fname = os.path.basename(self.ydata.party.data.fpath)
        dname = os.path.basename(os.path.dirname(self.ydata.party.data.fpath))
        path = cw.util.join_paths(cw.tempdir, "ScenarioLog/Party", fname)
        dstpath = cw.util.join_paths(self.ydata.tempdir, "Party", dname, fname)
        dpath = os.path.dirname(dstpath)

        if not os.path.isdir(dpath):
            os.makedirs(dpath)

        shutil.copy2(path, dstpath)
        # member copy
        dpath = cw.util.join_paths(cw.tempdir, "ScenarioLog/Members")

        for name in os.listdir(dpath):
            path = cw.util.join_paths(dpath, name)

            if os.path.isfile(path) and path.endswith(".xml"):
                dstpath = cw.util.join_paths(self.ydata.tempdir, "Adventurer", name)

                dstdir = os.path.dirname(dstpath)

                if not os.path.isdir(dstdir):
                    os.makedirs(dstdir)

                shutil.copy2(path, dstpath)

        # ゴシップ復元を無効にするモード(互換動作)でなければ
        # 追加されたゴシップを削除し、削除されたゴシップを追加し直す
        if not self.sct.disable_gossiprestration(self.sdata.get_versionhint(cw.HINT_SCENARIO)):
            # gossips
            for key, value in self.sdata.gossips.items():
                if value:
                    self.ydata.remove_gossip(key)
                else:
                    self.ydata.set_gossip(key)

        # 終了印復元を無効にするモード(互換動作)でなければ
        # 追加された終了印を削除し、削除された終了印を追加し直す
        if not self.sct.disable_compstamprestration(self.sdata.get_versionhint(cw.HINT_SCENARIO)):
            # completestamps
            for key, value in self.sdata.compstamps.items():
                if value:
                    self.ydata.remove_compstamp(key)
                else:
                    self.ydata.set_compstamp(key)

        # scenario
        self.ydata.party.set_lastscenario([], "")

        # members
        self.ydata.party.data = cw.data.yadoxml2etree(self.ydata.party.data.fpath)
        self.ydata.party.reload()

        # 荷物袋のデータを戻す
        path = cw.util.join_paths(cw.tempdir, "ScenarioLog/Backpack.xml")
        etree = cw.data.xml2etree(path)
        backpacktable = {}
        yadodir = self.ydata.party.get_yadodir()
        tempdir = self.ydata.party.get_tempdir()

        for header in itertools.chain(self.ydata.party.backpack, self.ydata.party.backpack_moved):
            if header.scenariocard:
                header.contain_xml()
                header.remove_importedmaterials()
                continue

            if header.fpath.lower().startswith("yado"):
                fpath = cw.util.relpath(header.fpath, yadodir)
            else:
                fpath = cw.util.relpath(header.fpath, tempdir)
            fpath = cw.util.join_paths(fpath)
            backpacktable[fpath] = header

        self.ydata.party.backpack = []
        self.ydata.party.backpack_moved = []
        rename_tbl = {}

        for i, e in enumerate(etree.getfind(".")):
            try:
                header = backpacktable[e.text]
                del backpacktable[e.text]
                if header.moved != 0:
                    # 削除フラグを除去
                    # 荷物袋から移動された場合は使用されている
                    # 可能性があるので上書き
                    if header.carddata is None:
                        etree = cw.data.yadoxml2etree(header.fpath)
                        etree.remove("Property", attrname="moved")
                        etree.write_xml()
                    else:
                        fname1 = os.path.basename(header.fpath)
                        etree = cw.data.yadoxml2etree(path=header.fpath)
                        etree.remove("Property", attrname="moved")
                        header2 = cw.header.CardHeader(carddata=etree.getroot())
                        header2.fpath = header.fpath
                        header2.write()
                        header = header2
                        fname2 = os.path.basename(header2.fpath)
                        if fname1 != fname2:
                            rename_tbl[fname1] = fname2
                    header.moved = 0
                self.ydata.party.backpack.append(header)
                header.order = i
                header.set_owner("BACKPACK")
                # 荷物袋にある場合はcarddata無し、特殊技能の使用回数無し
                header.carddata = None
                header.flags = {}
                header.steps = {}
                header.variants = {}
                if header.type == "SkillCard":
                    header.maxuselimit = 0
                    header.uselimit = 0
            except Exception:
                cw.util.print_ex()

        # 一度荷物袋から取り出されてから戻された
        # カードはbackpacktableに残る
        for fpath, header in backpacktable.items():
            self.ydata.deletedpaths.add(header.fpath)

        if not self.areaid >= 0:
            self.areaid = self.pre_areaids[0][0]

        # スプライトを作り直す
        pcards = self.get_pcards()
        showparty = bool(self.pcards)
        if showparty:
            for music in self.music:
                music.stop()

        logpath = cw.util.join_paths(cw.tempdir, "ScenarioLog/Face/Log.xml")
        if os.path.isfile(logpath):
            elog: Optional[cw.data.CWPyElementTree] = cw.data.xml2etree(logpath)
        else:
            elog = None

        if loadyado and not self.is_showparty:
            # シナリオ読込失敗で宿のロードと同時に帰還する場合に限り
            # パーティ出現アニメーションを行う
            status = "hidden"
        else:
            status = "normal"
        personalcard_tbl: Dict[str, cw.header.CardHeader] = {}
        for idx, data in enumerate(self.ydata.party.members):
            if idx < len(pcards):
                pcard = pcards[idx]
                pcard.hide()
                self.cardgrp.remove(pcard)
                self.pcards.remove(pcard)

            pos_noscale = (95 * idx + 9 * (idx + 1), 285)
            pcard = cw.sprite.card.PlayerCard(data, pos_noscale=pos_noscale, status=status, index=idx)

            # カード画像が変更されているPCは戻す
            if elog is not None:
                name = cw.util.splitext(os.path.basename(data.fpath))[0]

                for eimg in elog.getfind(".", raiseerror=False):
                    if eimg.get("member", "") == name:
                        prop = data.find_exists("Property")
                        for ename in ("ImagePath", "ImagePaths"):
                            e_imgs = prop.find(ename)
                            if e_imgs is not None:
                                prop.remove(e_imgs)

                        if eimg.tag == "ImagePath":
                            # 旧バージョン(～0.12.3)
                            fname = eimg.get("path", "")
                            if fname:
                                face = cw.util.join_paths(cw.tempdir, "ScenarioLog/Face", fname)
                            else:
                                face = ""
                            # 変更後のイメージを削除するためにここで再設定する
                            # (set_images()内で削除される)
                            prop.append(cw.data.make_element("ImagePath", eimg.text))
                            if os.path.isfile(face):
                                postype = eimg.get("positiontype", "Default")
                                pcard.set_images([cw.image.ImageInfo(face, postype=postype)])
                            else:
                                pcard.set_images([])
                        elif eimg.tag == "ImagePaths":
                            # 新バージョン(複数イメージ対応後)
                            seq = cw.image.get_imageinfos(eimg)
                            for info in seq:
                                info.path = cw.util.join_paths(cw.tempdir, "ScenarioLog/Face", info.path)
                            # 変更後のイメージを削除するためにここで再設定する
                            # (set_images()内で削除される)
                            e = cw.data.make_element("ImagePaths")
                            prop.append(e)
                            for e2 in eimg:
                                if e2.tag == "NewImagePath":
                                    e.append(cw.data.make_element("ImagePath", e2.text))
                            pcard.set_images(seq)
                        break

            pcard.set_pos_noscale(pos_noscale)
            pcard.set_fullrecovery()

            # 荷物袋内の私有カードのファイル名が変更されていた場合は参照先を差し替える
            e_personals = data.find("PersonalCards")
            if e_personals is not None:
                for e_personal in e_personals:
                    e_personal.text = rename_tbl.get(e_personal.text, e_personal.text)
            pcard.refresh_personalpocket(personalcard_tbl)

            pcard.update_image()

        self.ydata.party.sort_backpack()

        self.sdata.remove_log(None)

        self.ydata.party._loading = False

        if not self.is_showparty and not loadyado:
            self._show_party()

        self.background.clear_background()
        self.stop_allsounds()
        if self.lastsound_system:
            self.lastsound_system.stop(False)
            self.lastsound_system = None

        if not startotherscenario:
            self.set_yado()

    def reload_yado(self) -> None:
        """現在の宿をロード。"""
        self.is_debuggerprocessing = True
        # 特殊エリア・ログ表示等を解除
        self.clean_specials(redraw=False, silent=True)
        # イベントを中止
        self.event.stoped = True
        self.event.breakwait = True
        self.lock_menucards = True
        self._reloading = True
        del self.pre_dialogs[:]
        del self.pre_areaids[:]

        def return_title() -> None:
            def func() -> None:
                self.stop_allsounds()
                self.set_status("Title")
                self.sdata = cw.data.SystemData()
                cw.util.remove_temp()
                self.load_yado(self.yadodir, createmutex=False)

                def func() -> None:
                    def func() -> None:
                        self.is_debuggerprocessing = False
                        if self.is_showingdebugger() and self.event:
                            self.event.refresh_tools()
                    self.frame.exec_func(func)
                    self.lock_menucards = False
                self.exec_func(func)
            self.exec_func(func)

        def init_resources() -> None:
            self.event.clear()
            self._init_resources()

            self.frame.exec_func(return_title)

        def end_scenario() -> None:
            # シナリオを強制終了
            if self.ydata and self.ydata.losted_sdata:
                self.ydata.losted_sdata.end(failure=True)
                self.ydata.losted_sdata = None
            elif self.is_playingscenario():
                self.sdata.end(failure=True)
            self.sdata.is_playing = False

            self.exec_func(init_resources)

            if self.is_decompressing:
                raise cw.event.EffectBreakError()

        def func1() -> None:
            if self.is_showingmessage():
                mwin = self.get_messagewindow()
                assert mwin
                mwin.result = cw.event.EffectBreakError()
                self.event.stoped = True
            elif self.is_runningevent():
                self.event.stoped = True

            # バトルを強制終了
            if self.battle and self.battle.is_running:
                self.battle.end(True, True)

            self.exec_func(end_scenario)

        self.exec_func(func1)

    def load_yado(self, yadodir: str, createmutex: bool = True) -> bool:
        """指定されたディレクトリの宿をロード。"""
        try:
            return self._load_yado(yadodir, createmutex)
        except Exception as ex:
            cw.util.print_ex(file=sys.stderr)
            cw.tempdir = cw.tempdir_init
            self.yadodir = ""
            self.tempdir = ""
            self.setting.scenario_narrow = ""
            self.setting.lastscenario = []
            self.setting.lastscenariopath = ""
            self.setting.lastfindresult = []
            self.ydata = None
            self.sdata = cw.data.SystemData()
            raise ex

    def _load_yado(self, yadodir: str, createmutex: bool) -> bool:
        if createmutex:
            if cw.util.create_mutex("Yado"):
                if cw.util.create_mutex(yadodir):
                    try:
                        cw.tempdir = cw.util.join_paths("Data/Temp/Local", yadodir)
                        return self._load_yado2(yadodir)
                    finally:
                        cw.util.release_mutex(-2)
                else:
                    cw.util.release_mutex()
                    cw.cwpy.play_sound("error")
                    return False
            else:
                cw.cwpy.play_sound("error")
                return False
        else:
            return self._load_yado2(yadodir)

    def _load_yado2(self, yadodir: str) -> bool:
        del self.pre_dialogs[:]
        del self.pre_areaids[:]

        optscenario = cw.OPTIONS.getstr("scenario")
        cw.OPTIONS.setstr("scenario", "")

        yadodirname = os.path.basename(yadodir)
        self.yadodir = yadodir.replace("\\", "/")
        self.tempdir = self.yadodir.replace("Yado", cw.util.join_paths(cw.tempdir, "Yado"), 1)
        self.stop_allsounds()
        if self.ydata and isinstance(self.sdata, cw.data.SystemData):
            self.sdata.save_variables()
        self.ydata = cw.data.YadoData(self.yadodir, self.tempdir)
        self.setting.lastyado = yadodirname
        self.setting.insert_yadoorder(yadodirname)

        if self.ydata.party:
            header = self.ydata.party.get_sceheader()
            resume = True

            if optscenario:
                if os.path.isabs(optscenario):
                    scedir = optscenario
                else:
                    scedir = cw.cwpy.setting.get_scedir()
                    scedir = cw.util.join_paths(scedir, optscenario)
                db = self.frame.open_scenariodb()
                if db:
                    header2 = db.search_path(scedir)
                else:
                    header2 = None
                if header2:
                    if header:
                        scepath1 = header.get_fpath()
                        scepath1 = cw.util.get_keypath(scepath1)
                        scepath2 = header2.get_fpath()
                        scepath2 = cw.util.get_keypath(scepath2)
                        if header and scepath1 != scepath2:
                            self.sdata.set_log()
                            self.ydata.party.lastscenario = []
                            self.ydata.party.lastscenariopath = optscenario
                            self.setting.lastscenario = []
                            self.setting.lastscenariopath = optscenario
                            self.setting.lastfindresult = []
                            self._f9impl(startotherscenario=True)
                            resume = False
                    else:
                        personalcard_tbl: Dict[str, cw.header.CardHeader] = {}
                        for idx, data in enumerate(self.ydata.party.members):
                            pos_noscale = (95 * idx + 9 * (idx + 1), 285)
                            pcard = cw.sprite.card.PlayerCard(data, pos_noscale=pos_noscale, status="normal", index=idx)
                            pcard.set_pos_noscale(pos_noscale)
                            pcard.set_fullrecovery()
                            pcard.refresh_personalpocket(personalcard_tbl)
                            pcard.update_image()
                        self.ydata.party.sort_backpack()
                        self.ydata.party._loading = False
                        self.ydata.party.lastscenario = []
                        self.ydata.party.lastscenariopath = optscenario
                        self.setting.lastscenario = []
                        self.setting.lastscenariopath = optscenario
                        self.setting.lastfindresult = []
                        self._show_party()
                        resume = False
                    header = header2
                else:
                    resume = False

            # シナリオプレイ途中から再開
            if header:
                def set_scenario(header: Optional[cw.header.ScenarioHeader], resume: bool) -> None:
                    assert self.ydata
                    assert self.ydata.party
                    self.ydata.party.set_numbercoupon()
                    self.set_scenario(header, resume=resume)
                self.exec_func(set_scenario, header, resume)
            # シナリオロードに失敗
            elif self.ydata.party.is_adventuring():
                self.play_sound("error")
                s = (cw.cwpy.msgs["load_scenario_failure"])
                self.call_modaldlg("YESNO", text=s)

                if self.get_yesnoresult() == wx.ID_OK:
                    def func() -> None:
                        self.sdata.set_log()
                    self.exec_func(func)
                    self.exec_func(self.f9, True, True)
                else:
                    self.exec_func(self.ydata.load_party, None)
                    self.exec_func(self.set_yado)
            else:
                self.exec_func(self.set_yado)

            debugger = self.is_showingdebugger()
            if debugger:
                func = debugger.refresh_tools
                self.frame.exec_func(func)

        else:
            self.exec_func(self.set_yado)

        self._clear_changed = True
        return True

# ------------------------------------------------------------------------------
# エリアチェンジ関係メソッド
# ------------------------------------------------------------------------------

    def deal_cards(self, quickdeal: bool = False, updatelist: bool = True, flag: str = "", startbattle: bool = False,
                   silent: bool = False) -> None:
        """hidden状態のMenuCard(対応フラグがFalseだったら表示しない)と
        PlayerCardを全て表示する。
        quickdeal: 全カードを同時に表示する。
        """
        if not (self.setting.quickdeal or self.setting.all_quickdeal):
            quickdeal = False
            self.force_dealspeed = -1
        self._dealing = True

        if self.is_autospread():
            mcardsinv = self.get_mcards("invisible")
        else:
            mcardsinv = self.get_mcards("invisible", flag=flag)

        # エネミーカードは初期化されていない場合がある
        for mcard in mcardsinv[:]:
            if isinstance(mcard, cw.sprite.card.EnemyCard):
                if mcard.is_flagtrue():
                    if not mcard.initialize():
                        mcardsinv.remove(mcard)

        # カード自動配置の配置位置を再設定する
        if self.is_autospread():
            mcards = self.get_mcards("flagtrue")
            flag_value = bool(self.areaid == cw.AREA_CAMP and self.sdata.friendcards)
            if self.is_battlestatus():
                self.set_autospread(mcards, 6, flag_value, anime=False)
            else:
                self.set_autospread(mcards, 7, flag_value, anime=False)

        deals = []
        for mcard in mcardsinv:
            if mcard.is_flagtrue():
                self._fix_updateimage(mcard)

                if quickdeal and not silent:
                    deals.append(mcard)
                else:
                    if silent:
                        mcard.deal()
                    else:
                        cw.animation.animate_sprite(mcard, "deal")
                    if self.is_playingscenario() and self.sdata.in_f9:
                        # カード描画中にF9された場合はここへ来る
                        return

        if deals and quickdeal:
            cw.animation.animate_sprites(deals, "deal")

        # list, indexセット
        if updatelist:
            self._update_mcardlist()
        else:
            self._after_update_mcardlist = True

        self.input(True)
        self._dealing = False
        self.wait_showcards = False

    def hide_cards(self, hideall: bool = False, hideparty: bool = True, quickhide: bool = False,
                   updatelist: bool = True, flag: str = "", silent: bool = False) -> None:
        """
        カードを非表示にする(表示中だったカードはhidden状態になる)。
        各カードのhidecards()の最後に呼ばれる。
        hideallがTrueだった場合、全てのカードを非表示にする。
        """
        if not (self.setting.quickdeal or self.setting.all_quickdeal):
            quickhide = False
            self.force_dealspeed = -1
        self._dealing = True
        if updatelist:
            # 選択を解除する
            self.clear_selection()

        # メニューカードを下げる
        if self.is_autospread():
            mcards = self.get_mcards("visible")
        else:
            mcards = self.get_mcards("visible", flag=flag)
        hide = False
        for mcard in mcards:
            if hideall or not mcard.is_flagtrue():
                self._fix_updateimage(mcard)

                if mcard.inusecardimg:
                    self.clear_inusecardimg(mcard)
                if quickhide and not silent:
                    hide = True
                else:
                    if silent:
                        mcard.hide()
                    else:
                        cw.animation.animate_sprite(mcard, "hide")
                if isinstance(mcard, cw.character.Character):
                    mcard.clear_action()
        if hide:
            cw.animation.animate_sprites(mcards, "hide")

        # プレイヤカードを下げる
        if self.ydata and hideparty:
            if not self.ydata.party or self.ydata.party.is_loading():
                self.statusbar.change(False)
                self.hide_party()

        # list, indexセット
        if updatelist:
            self._update_mcardlist()
        else:
            self._after_update_mcardlist = True

        self.input(True)
        self._dealing = False

    def _fix_updateimage(self, mcard: Union["cw.sprite.card.MenuCard",
                                            "cw.sprite.card.EnemyCard",
                                            "cw.sprite.card.FriendCard"]) -> None:
        if mcard.is_initialized():
            if mcard.cardimg.use_excache:
                mcard.cardimg.clear_cache()
            mcard.cardimg.fix_pcimage_updated()
            self.file_updates.discard(mcard)
        mcard.cardimg.use_excache = False

    def vanished_card(self, mcard: "cw.sprite.card.CWPyCard") -> None:
        """mcardの対象消去を通知する。"""
        if isinstance(mcard, (cw.sprite.card.MenuCard, cw.sprite.card.EnemyCard)) and mcard.flag:
            defvalue: List[Union[cw.sprite.card.EnemyCard, cw.sprite.card.MenuCard]] = []
            seq = self._mcardtable.get(mcard.flag, defvalue)
            if seq and mcard in seq:
                seq.remove(mcard)
                if not seq:
                    del self._mcardtable[mcard.flag]

        if isinstance(mcard, cw.sprite.card.PlayerCard):
            self._need_disposition = True

        self.update_mcardnames()

    def update_mcardlist(self) -> None:
        """必要であればメニューカードのリストを更新する。
        """
        if self._after_update_mcardlist:
            self._update_mcardlist()

    def _update_mcardlist(self) -> None:
        self._mcardtable = {}
        mcards = self.get_mcards()
        for mcard in mcards:
            if mcard.flag:
                assert not isinstance(mcard, cw.sprite.card.FriendCard)
                defvalue: List[Union[cw.sprite.card.EnemyCard, cw.sprite.card.MenuCard]] = []
                seq = self._mcardtable.get(mcard.flag, defvalue)
                seq.append(mcard)
                if len(seq) == 1:
                    self._mcardtable[mcard.flag] = seq
        if not self.is_showingmessage():
            self.update_selectablelist()

    def update_pcimage(self, pcnumber: int,
                       deal: bool) -> List[Union["cw.sprite.card.MenuCard",
                                                 "cw.sprite.card.EnemyCard",
                                                 "cw.sprite.card.FriendCard"]]:
        if not self.file_updates_bg or deal:
            for bgtype, d in self.background.bgs:
                if bgtype == cw.sprite.background.BG_PC:
                    assert d
                    bgpcnumber = d[0]
                    assert isinstance(bgpcnumber, int)
                    if bgpcnumber == pcnumber:
                        if deal:
                            self.background.reload(False)
                        else:
                            self.file_updates_bg = True
                        break

        updates = []
        pcards = self.get_pcards()
        pcard = pcards[pcnumber-1] if pcnumber-1 < len(pcards) else None
        for mcard in self.get_mcards():
            if not mcard.is_initialized():
                continue
            imgpaths = []
            can_loaded_scaledimages = []
            can_loaded_scaledimage = pcard.data.getbool(".", "scaledimage", False) if pcard else True
            update = False
            cardimg = mcard.cardimg
            if isinstance(cardimg, cw.image.CharacterCardImage) and cardimg.is_override_image:
                assert cardimg.override_images is not None
                cardimg_paths = cardimg.override_images[0]
                cardimg_can_loaded_scaledimage: Union[bool, List[bool]] = cardimg.override_images[1]
            else:
                cardimg_paths = cardimg.paths
                cardimg_can_loaded_scaledimage = cardimg.can_loaded_scaledimage
            for i, info in enumerate(cardimg_paths):
                # PC画像を更新
                if info.pcnumber == pcnumber:
                    if pcard:
                        for base in pcard.imgpaths:
                            imgpaths.append(cw.image.ImageInfo(base.path, pcnumber, info.base,
                                                               basecardtype="LargeCard"))
                            can_loaded_scaledimages.append(can_loaded_scaledimage)
                    else:
                        imgpaths.append(cw.image.ImageInfo(pcnumber=pcnumber, base=info.base,
                                                           basecardtype="LargeCard"))
                        can_loaded_scaledimages.append(False)
                    update = True
                else:
                    imgpaths.append(info)
                    if isinstance(cardimg_can_loaded_scaledimage, (list, tuple)):
                        can_loaded_scaledimages.append(cardimg_can_loaded_scaledimage[i])
                    else:
                        assert isinstance(cardimg_can_loaded_scaledimage, bool)
                        can_loaded_scaledimages.append(cardimg_can_loaded_scaledimage)
            if not update:
                continue
            if isinstance(cardimg, cw.image.CharacterCardImage) and cardimg.is_override_image:
                cardimg.override_images_upd = (imgpaths, can_loaded_scaledimages)
            else:
                cardimg.paths_upd = imgpaths
                cardimg.can_loaded_scaledimage_upd = can_loaded_scaledimages
            if deal:
                cardimg.fix_pcimage_updated()
                cardimg.clear_cache()
            updates.append(mcard)
        if deal:
            if cw.cwpy.setting.all_quickdeal:
                force_dealspeed = self.force_dealspeed
                self.force_dealspeed = self.setting.dealspeed if self.setting.quickdeal else -1
                try:
                    cw.animation.animate_sprites(updates, "hide")
                    for mcard in updates:
                        mcard.update_image()
                    cw.animation.animate_sprites(updates, "deal")
                finally:
                    self.force_dealspeed = force_dealspeed
            else:
                for mcard in updates:
                    cw.animation.animate_sprite(mcard, "hide")
                    mcard.update_image()
                    cw.animation.animate_sprite(mcard, "deal")
        if cw.cwpy.background.has_jpdcimage:
            cw.cwpy.file_updates_bg = True
        return updates

    def show_party(self) -> None:
        """非表示のPlayerCardを再表示にする。"""
        pcards = [i for i in self.get_pcards() if i.status == "hidden"]

        if pcards:
            seq: List[cw.sprite.base.CWPySprite] = []
            for pcard in pcards:
                seq.append(pcard)
                if pcard.inusecardimg and not pcard.inusecardimg.center:
                    seq.append(pcard.inusecardimg)
            cw.animation.animate_sprites(seq, "shiftup")

        self._show_party()

    def _show_party(self) -> None:
        if self.ydata and self.ydata.party and not self.ydata.party.is_loading():
            self.is_showparty = True
            self.input(True)
            self.event.refresh_showpartytools()

    def hide_party(self) -> None:
        """PlayerCardを非表示にする。"""
        pcards = [i for i in self.get_pcards() if not i.status == "hidden"]

        if pcards:
            seq: List[cw.sprite.base.CWPySprite] = []
            for pcard in pcards:
                seq.append(pcard)
                if pcard.inusecardimg and not pcard.inusecardimg.center:
                    seq.append(pcard.inusecardimg)
            cw.animation.animate_sprites(seq, "shiftdown")

        self.is_showparty = False
        self.selection = None
        self.input(True)
        self.event.refresh_showpartytools()

    def set_pcards(self, newparty: bool = False) -> None:
        # プレイヤカードスプライト作成
        if self.ydata and self.ydata.party and not self.get_pcards():
            personalcard_tbl: Dict[str, cw.header.CardHeader] = {}
            for idx, e in enumerate(self.ydata.party.members):
                pos_noscale = 95 * idx + 9 * (idx + 1), 285
                pcard = cw.sprite.card.PlayerCard(e, pos_noscale=pos_noscale, index=idx)
                if newparty:
                    pcard.restore_personalpocket()
                else:
                    pcard.refresh_personalpocket(personalcard_tbl)
            if not newparty:
                self.ydata.party.sort_backpack()

            # 番号クーポン設定
            self.ydata.party._loading = False

    def set_sprites(self, dealanime: bool = True, bginhrt: bool = False,
                    ttype: Tuple[str, Union[str, int]] = ("Default", "Default"), doanime: bool = True,
                    data: Optional[cw.data.CWPyElement] = None, nocheckvisible: bool = False, silent: bool = False,
                    newparty: bool = False) -> None:
        """エリアにスプライトをセットする。
        bginhrt: Trueの時は背景継承。
        """
        # メニューカードスプライトグループの中身を削除
        self.clear_selection()
        self.cardgrp.remove(*self.mcards)
        self.mcards = []
        self.mcards_expandspchars.clear()
        self.file_updates.clear()

        # プレイヤカードスプライトグループの中身を削除
        if self.ydata:
            if not self.ydata.party or self.ydata.party.is_loading():
                self.cardgrp.remove(*self.pcards)
                self.pcards = []

        # 背景スプライト作成
        if not bginhrt:
            try:
                self.background.load(self.sdata.get_bgdata(), doanime, ttype, nocheckvisible=nocheckvisible)
            except cw.event.EffectBreakError:
                # JPY1の処理がF9等で中止された
                assert doanime
                return

        # 特殊エリア(キャンプ・メンバー解散)だったら背景にカーテンを追加。
        if self.areaid in (cw.AREA_CAMP, cw.AREA_BREAKUP):
            self.set_curtain(curtain_all=True, redraw=not silent)

        # メニューカードスプライト作成
        self.set_mcards(self.sdata.get_mcarddata(data=data), dealanime)

        # プレイヤカードスプライト作成
        self.set_pcards(newparty=newparty)

        # キャンプ画面のときはFriendCardもスプライトグループに追加
        if self.areaid == cw.AREA_CAMP:
            self.add_fcardsprites(status="hidden")

    def add_fcardsprites(self, status: str, alpha: Optional[int] = None) -> None:
        """cardgrpに同行NPCのスプライトを追加する。"""
        self._is_showingfcards = False
        seq = list(enumerate(self.get_fcards()))
        if cw.cwpy.sct.lessthan("1.30", cw.cwpy.sdata.get_versionhint()):
            seq.reverse()
        for index, fcard in seq:
            self._is_showingfcards = True
            if cw.cwpy.sct.lessthan("1.30", cw.cwpy.sdata.get_versionhint()):
                index = 5 - index
            else:
                index = 6 - len(seq) + index
            pos = (95 * index + 9 * (index + 1), 5)
            fcard.set_pos_noscale(pos)
            fcard.status = status
            fcard.set_alpha(alpha)
            if fcard.status == "hidden":
                fcard.clear_image()
                cw.add_layer(self.cardgrp, fcard, layer=cw.layer_val(fcard.tlayer_t))
                self.mcards.append(fcard)
            else:
                cw.add_layer(self.cardgrp, fcard, layer=cw.layer_val(fcard.tlayer_t))
                self.mcards.append(fcard)
                if alpha is not None:
                    fcard.update_image()
                fcard.deal()
            self.add_lazydraw(clip=fcard.rect)
        self.list = []
        self.list.extend(self.get_mcards("selectable"))
        self.index = -1

    def clear_fcardsprites(self) -> None:
        """cardgrpから同行NPCのスプライトを取り除く。"""
        self._is_showingfcards = False
        fcards: List[cw.sprite.card.FriendCard] = []
        for fcard in self.mcards[:]:
            if isinstance(fcard, cw.character.Friend):
                assert isinstance(fcard, cw.sprite.card.FriendCard)
                self.add_lazydraw(clip=fcard.rect)
                fcard.set_alpha(None)
                fcard.hide()
                fcard.tlayer = (cw.LAYER_FCARDS, cw.LTYPE_FCARDS, fcard.index, 0)
                fcards.append(fcard)
                self.mcards.remove(fcard)
                self.mcards_expandspchars.discard(fcard)
                self.file_updates.discard(fcard)
        self.cardgrp.remove(*fcards)
        self.list = []
        self.list.extend(self.get_mcards("selectable"))
        self.index = -1

    def update_mcardnames(self) -> None:
        for mcard in self.mcards_expandspchars:
            if mcard.is_initialized():
                name = mcard.get_showingname()
                mcard.update_name()
                if mcard.get_showingname() != name:
                    self.add_lazydraw(mcard.rect)

    def set_autospread(self, mcards: Sequence[Union["cw.sprite.card.EnemyCard", "cw.sprite.card.MenuCard"]],
                       maxcol: int, campwithfriend: bool = False, anime: bool = False) -> None:
        """自動整列設定時のメニューカードの配置位置を設定する。
        mcards: MenuCard or EnemyCardのリスト。
        maxcol: この値を超えると改行する。
        campwithfriend: キャンプ画面時＆FriendCardが存在しているかどうか。
        anime: カードを一旦消去してから再配置するならTrue。
        """
        def get_size_noscale(mcard: Union[cw.sprite.card.EnemyCard, cw.sprite.card.MenuCard]) -> Tuple[int, int]:
            assert mcard.cardimg

            if isinstance(mcard.cardimg, cw.image.CharacterCardImage) or\
               isinstance(mcard.cardimg, cw.image.LargeCardImage):
                return cw.setting.get_resourcesize("CardBg/LARGE")
            elif isinstance(mcard.cardimg, cw.image.CardImage):
                return cw.setting.get_resourcesize("CardBg/NORMAL")
            else:
                assert False

        def set_mcardpos_noscale(mcards: Sequence[Union[cw.sprite.card.EnemyCard, cw.sprite.card.MenuCard]],
                                 maxsize: Tuple[int, int], y: int) -> None:
            (maxw, maxh) = maxsize
            n = maxw + 5
            x = (632 - n * len(mcards) + 5) // 2

            grpidx: Dict[Union[Tuple[str, int], str], int] = {}
            for mcard in mcards:
                if mcard.cardgroup:
                    gi = grpidx.get(mcard.cardgroup, 0)
                # カード再配置の対象になっている場合は整列しない
                if not mcard.cardgroup or not (mcard.cardgroup, gi) in self.sdata.moved_mcards:
                    w, h = get_size_noscale(mcard)
                    if mcard.scale != 100:
                        mcard.set_scale(100)
                    mcard.set_pos_noscale((x + maxw - w, y + maxh - h))
                if mcard.cardgroup:
                    grpidx[(mcard.cardgroup, gi)] = gi + 1
                x += n

        maxw = 0
        maxh = 0

        for mcard in mcards:
            w, h = get_size_noscale(mcard)

            maxw = max(w, maxw)
            maxh = max(h, maxh)

            if anime:
                cw.animation.animate_sprite(mcard, "hide")

        n = len(mcards)

        if campwithfriend:
            y = (145 - maxh) // 2 + 140 - 2
            set_mcardpos_noscale(mcards, (maxw, maxh), y)
        elif n <= maxcol:
            y = (285 - maxh) // 2 - 2
            set_mcardpos_noscale(mcards, (maxw, maxh), y)
        else:
            y = (285 - 10 - maxh * 2) // 2
            y2 = y + maxh + 5
            p = n // 2 + n % 2
            set_mcardpos_noscale(mcards[:p], (maxw, maxh), y)
            set_mcardpos_noscale(mcards[p:], (maxw, maxh), y2)

        if anime:
            for mcard in mcards:
                cw.animation.animate_sprite(mcard, "deal")

        if self.battle:
            self.battle.numenemy = len(cw.cwpy.get_mcards("flagtrue"))

    def set_mcards(self, stype_and_elements: Tuple[str, Iterable[cw.data.CWPyElement]], dealanime: bool = True,
                   addgroup: bool = True, setautospread: bool = True,
                   splayer: Optional[bool] = None) -> List[Union["cw.sprite.card.MenuCard",
                                                                 "cw.sprite.card.EnemyCard"]]:
        """メニューカードスプライトを構成する。
        生成されたカードのlistを返す。
        (stype, elements): (spreadtype, MenuCardElementのリスト)のタプル
        dealanime: True時はカードを最初から表示している。
        addgroup: True時は現在の画面に即時反映する。
        """
        (stype, elements) = stype_and_elements
        if stype == "Auto":
            autospread = True
        else:
            autospread = False

        old_autospread = self._autospread
        self._autospread = autospread

        status = "hidden" if dealanime else "normal"
        seq: List[Union[cw.sprite.card.MenuCard, cw.sprite.card.EnemyCard]] = []

        grpidx: Dict[str, int] = {}
        moved_mcards: Dict[Tuple[str, int], Tuple[int, int, int, int]] = {}  # 不要な再配置情報を削除するため再構築する
        for i, e in enumerate(elements):
            cardgroup = e.gettext("Property/CardGroup", "")
            if stype == "Auto":
                pos_noscale = (0, 0)
            else:
                left = e.getint("Property/Location", "left")
                top = e.getint("Property/Location", "top")
                pos_noscale = (left, top)

            status2 = status
            if status2 != "hidden":
                if not cw.sprite.card.CWPyCard.is_flagtrue_static(e):
                    status2 = "hidden"

            if cardgroup:
                gi = grpidx.get(cardgroup, 0)
                moveddata = self.sdata.moved_mcards.get((cardgroup, gi), None)
            else:
                moveddata = None

            mcard: Union[cw.sprite.card.MenuCard, cw.sprite.card.EnemyCard]
            if e.tag == "EnemyCard":
                if self.sdata.get_castname(e.getint("Property/Id", -1)) is None:
                    continue
                mcard = cw.sprite.card.EnemyCard(e, pos_noscale, status2, addgroup, i,
                                                 moveddata=moveddata)
            else:
                mcard = cw.sprite.card.MenuCard(e, pos_noscale, status2, addgroup, i,
                                                moveddata=moveddata, splayer=splayer)

            if not mcard.is_flagtrue():
                mcard.status = "hidden"

            if cardgroup:
                if moveddata:
                    moved_mcards[(cardgroup, gi)] = moveddata
                grpidx[cardgroup] = gi + 1

            seq.append(mcard)

        if not setautospread:
            self._autospread = old_autospread
        if 0 <= self.areaid:
            self.sdata.moved_mcards = moved_mcards

        return seq

    def disposition_pcards(self) -> None:
        """プレイヤーカードの位置を補正する。
        対象消去が発生した場合や解散直後に適用。
        """
        if self.ydata and self.ydata.party:
            # キャンセル可能な対象消去状態だったメンバを完全に消去する(互換動作)
            for pcard in self.ydata.party.vanished_pcards:
                pcard.commit_vanish()
            self.ydata.party.vanished_pcards = []

        for index, pcard in enumerate(self.get_pcards()):
            self.add_lazydraw(clip=pcard.rect)
            x = 9 + 95 * index + 9 * index
            y = pcard.get_pos_noscale()[1]
            pcard.get_baserect().x = cw.s(x)
            y2 = pcard.rect.top
            size = pcard.rect.size
            baserect = pcard.get_baserect()
            if pcard.rect.size == (0, 0):
                pcard.rect.size = baserect.size
            pcard.rect.center = baserect.center
            pcard.rect.top = y2
            pcard.cardimg.rect.x = cw.s(x)
            pcard._pos_noscale = (x, y)
            pcard.rect.size = size
            for i, t in enumerate(pcard.zoomimgs):
                img, rect = t
                rect.center = pcard.rect.center
                pcard.zoomimgs[i] = (img, rect)
            pcard.index = index
            pcard.update_personalownerindex()
            self.add_lazydraw(clip=pcard.rect)
        self._need_disposition = False

    def change_area(self, areaid: int, eventstarting: bool = True, bginhrt: bool = False,
                    ttype: Tuple[str, Union[str, int]] = ("Default", "Default"), quickdeal: bool = False,
                    specialarea: bool = False, startbattle: bool = False, doanime: bool = True,
                    data: Optional[cw.data.CWPyElement] = None, nocheckvisible: bool = False,
                    clear_curtain: bool = False, force_updatebg: bool = False, silent: bool = False,
                    newparty: bool = False) -> None:
        """ゲームエリアチェンジ。
        eventstarting: Falseならエリアイベントは起動しない。
        bginhrt: 背景継承を行うかどうかのbool値。
        ttype: トランジション効果のデータのタプル((効果名, 速度))
        """
        if self.ydata and not self.is_playingscenario():
            oldchanged = self.ydata.is_changed()
        else:
            oldchanged = True

        # 宿にいる時は常に高速切替有効
        force_dealspeed = self.force_dealspeed
        if self.setting.all_quickdeal and not self.is_playingscenario():
            quickdeal = True
            self.force_dealspeed = self.setting.dealspeed if self.setting.quickdeal else -1

        # デバッガ等で強制的にエリア移動するときは特殊エリアを解除する
        if not specialarea:
            self.clean_specials(silent=silent)

        # 背景継承を行うかどうかのbool値
        bginhrt |= bool(self.areaid < 0 and self.areaid != cw.AREA_BREAKUP)
        bginhrt &= not force_updatebg
        oldareaid = self.areaid
        self.areaid = areaid

        if 0 <= oldareaid and 0 <= self.areaid:
            # カード再配置情報を破棄
            self.moved_mcards: Dict[Tuple[str, int], Tuple[int, int, int, int]] = {}

        if not self.sdata.change_data(areaid, data=data):
            raise cw.event.EffectBreakError()
        bginhrt |= bool(self.areaid < 0)
        bginhrt &= not force_updatebg
        if self.sdata.in_f9:
            self.hide_cards(True, quickhide=quickdeal, silent=silent)
        else:
            self.hide_cards(True, quickhide=quickdeal, silent=silent)
            if clear_curtain:
                self.clear_curtain(redraw=not silent)
            self.set_sprites(bginhrt=bginhrt, ttype=ttype, doanime=doanime, data=data,
                             nocheckvisible=nocheckvisible, silent=silent, newparty=newparty)

        if not self.is_playingscenario() and not self.is_showparty and self.status != "GameOver":
            # 宿にいる場合は常に全回復状態にする
            for pcard in self.get_pcards():
                pcard.set_fullrecovery()
                pcard.update_image()
                if not silent:
                    self.add_lazydraw(clip=pcard.rect)

        if not self.is_playingscenario() and not silent:
            self.disposition_pcards()

        if 0 <= oldareaid and self.ydata and self.is_playingscenario():
            self.ydata.changed()

        # エリアイベントを開始(特殊エリアからの帰還だったら開始しない)
        if eventstarting and oldareaid >= 0 and not self.is_updating_skin:
            if not self.wait_showcards:
                self.deal_cards(quickdeal=quickdeal, startbattle=startbattle, silent=silent)
            self.force_dealspeed = force_dealspeed

            if self.is_playingscenario() and self.sdata.in_f9:
                # カード描画中にF9された場合はここへ来る
                return

            if self.areaid >= 0 and self.status == "Scenario":
                self.elapse_time(playeronly=True)

            if self._need_disposition and not silent:
                self.disposition_pcards()

            self.sdata.start_event(keynum=1)
        elif not self.sdata.in_f9:
            self.deal_cards(quickdeal=quickdeal, startbattle=startbattle, silent=silent)
            self.force_dealspeed = force_dealspeed
            if not startbattle and not pygame.event.peek(pygame.USEREVENT):
                self.show_party()

            if self._need_disposition and not silent:
                self.disposition_pcards()

        if self.ydata and not self.is_playingscenario():
            self.ydata._changed = oldchanged

    def change_battlearea(self, areaid: int) -> None:
        """
        指定するIDの戦闘を開始する。
        """
        data = self.sdata.get_resdata(True, areaid)
        if data is None:
            raise cw.event.EffectBreakError()

        # 対象選択中であれば中止
        self.lock_menucards = True
        self.clean_specials()

        self.play_sound("battle", from_scenario=True, material_override=True)
        self.statusbar.change(False, encounter=True)

        path = cw.util.validate_filepath(data.gettext("Property/MusicPath", ""))
        volume = data.getint("Property/MusicPath", "volume", 100)
        loopcount = data.getint("Property/MusicPath", "loopcount", 0)
        channel = data.getint("Property/MusicPath", "channel", 0)
        fade = data.getint("Property/MusicPath", "fadein", 0)
        continue_bgm = data.getbool("Property/MusicPath", "continue", False)

        music = self.music[channel]
        self.set_battle()

        # 戦闘開始アニメーション
        sprite = cw.sprite.background.BattleCardImage()
        cw.animation.animate_sprite(sprite, "battlestart")
        oldareaid = self.areaid
        oldbgmpath = (music.path, music.subvolume, music.loopcount, channel)
        if self.sdata.pre_battleareadata:
            oldareaid = self.sdata.pre_battleareadata[0]
            oldbgmpath = self.sdata.pre_battleareadata[1]

        # 戦闘音楽を流す
        if not continue_bgm:
            music.play(path, subvolume=volume, loopcount=loopcount, fade=fade)

        self.change_area(areaid, False, bginhrt=True, ttype=("None", "Default"), startbattle=True)
        cw.animation.animate_sprite(sprite, "hide")
        sprite.remove(cw.cwpy.cardgrp)

        self.sdata.pre_battleareadata = (oldareaid, oldbgmpath, (music.path, music.subvolume, music.loopcount,
                                                                 music.channel))
        cw.battle.BattleEngine(data)
        self.lock_menucards = False

    def clear_battlearea(self, areachange: bool = True, eventkeynum: int = 0, startnextbattle: bool = False,
                         is_battlestarting: bool = False) -> None:
        """戦闘状態を解除して戦闘前のエリアに戻る。
        areachangeがFalseだったら、戦闘前のエリアには戻らない
        (戦闘イベントで、エリア移動コンテント等が発動した時用)。
        """
        if not cw.cwpy.is_playingscenario():
            cw.cwpy.battle = None
            return

        if self.status == "ScenarioBattle":
            if isinstance(self.event.get_selectedmember(), cw.character.Enemy):
                self.event.clear_selectedmember()

            # 勝利イベントを保持しておく
            assert self.sdata.events
            battleevents = self.sdata.events
            if eventkeynum:
                self.winevent_areaid = self.areaid

            cw.cwpy.battle = None

            for pcard in self.get_pcards():
                pcard.deck.clear(pcard)
                pcard.remove_timedcoupons(True)

            for fcard in self.get_fcards():
                fcard.deck.clear(fcard)
                fcard.remove_timedcoupons(True)

            assert self.sdata.pre_battleareadata
            areaid, bgmpath, _battlebgmpath = self.sdata.pre_battleareadata
            if not startnextbattle:
                self.sdata.pre_battleareadata = None
            self.set_scenario()

            # BGMを最後に指定されたものに戻す
            self.music[bgmpath[3]].play(bgmpath[0], subvolume=bgmpath[1], loopcount=bgmpath[2])

            # 一部ステータスは回復
            for pcard in self.get_pcards():
                if pcard.is_bind() or pcard.mentality != "Normal":
                    if pcard.status == "hidden":
                        pcard.set_bind(0)
                        pcard.set_mentality("Normal", 0)
                        pcard.update_image()
                    else:
                        self.play_sound("harvest")
                        pcard.set_bind(0)
                        pcard.set_mentality("Normal", 0)
                        cw.animation.animate_sprite(pcard, "hide", battlespeed=True)
                        pcard.update_image()
                        cw.animation.animate_sprite(pcard, "deal", battlespeed=True)

            if (areachange or (startnextbattle and not is_battlestarting)) and not eventkeynum == 3 and\
                    not cw.cwpy.sct.lessthan("1.20", cw.cwpy.sdata.get_versionhint()):
                # 勝利・逃走成功時に時間経過
                # 戦闘中のエリア移動・敗北イベント・1.20以下は時間経過しない
                self.elapse_time(playeronly=True)
                if self.is_gameover() and cw.cwpy.sdata.can_gameover():
                    self.set_gameover()
                    return

            # NPCの状態を回復
            self.sdata.fullrecovery_fcards()

            if areachange:
                # 戦闘前のエリアに戻る
                try:
                    self.change_area(areaid, False, ttype=("None", "Default"), bginhrt=True)
                except cw.event.EffectBreakError:
                    # バトル開始時のエリアが削除されていたケース
                    s = "エリア%sの読込に失敗しました。" % (areaid)
                    cw.cwpy.call_modaldlg("ERROR", text=s)
                    self.f9()
                    return
                self.statusbar.change(False)

            if eventkeynum:
                # 勝利イベント開始
                self.event.clear_selectedmember()
                self.event.clear_selectedcard()
                battleevents.start(keynum=eventkeynum)
                self.winevent_areaid = None

    def change_specialarea(self, areaid: int, silent: bool = False) -> None:
        """特殊エリア(エリアIDが負の数)に移動する。"""
        updatestatusbar = True
        if areaid < 0:
            assert self.sdata.data is not None
            self.pre_areaids.append((self.areaid, self.sdata.data))

            # パーティ解散・キャンプエリア移動の場合はエリアチェンジ
            if areaid in (cw.AREA_BREAKUP, cw.AREA_CAMP):
                if cw.cwpy.ydata:
                    changed = cw.cwpy.ydata.is_changed()
                self.clear_fcardsprites()
                force_dealspeed = self.force_dealspeed
                self.force_dealspeed = self.setting.dealspeed if self.setting.quickdeal else -1
                try:
                    self.change_area(areaid, quickdeal=True, specialarea=True, silent=silent)
                finally:
                    self.force_dealspeed = force_dealspeed
                if cw.cwpy.ydata:
                    cw.cwpy.ydata._changed = changed
                if areaid == cw.AREA_BREAKUP:
                    self._store_partyrecord()
                    self.create_poschangearrow()
            else:
                if areaid not in self.sdata.sparea_mcards:
                    cw.cwpy.call_dlg("ERROR", text="指定された特殊エリア(ID=%s)は存在しません。" % areaid)
                    self.pre_areaids.pop()
                    return
                self.areaid = areaid
                self.sdata.change_data(areaid)
                self.pre_mcards.append(self.get_mcards())
                self.cardgrp.remove(*self.mcards)
                self.mcards = []
                self.mcards_expandspchars.clear()
                self.file_updates.clear()
                for mcard in self.sdata.sparea_mcards[areaid]:
                    cw.add_layer(self.cardgrp, mcard, layer=cw.layer_val(mcard.tlayer))
                    self.mcards.append(mcard)
                    if mcard.spchars:
                        self.mcards_expandspchars.add(mcard)
                # 特殊エリアのカードはデバッグモードによって
                # 表示が切り替わる場合がある
                assert self.sdata.data is not None
                for mcard in self.sdata.sparea_mcards[areaid]:
                    if not mcard.is_initialized():
                        mcard.initialize()
                    if (mcard.debug_only and not self.is_debugmode()) or not mcard.is_flagtrue():
                        mcard.hide()
                    else:
                        mcard.deal()
                spreadtype = self.sdata.data.getattr("MenuCards", "spreadtype", "Auto")
                if spreadtype == "Auto":
                    mcards = self.get_mcards("flagtrue")
                    self.set_autospread(mcards, 6, False, anime=False)

                if self.areaid in cw.AREAS_TRADE:
                    if self.selectedheader:
                        self.set_testaptitude(self.selectedheader)

                self.clear_selection()
                self.list = []
                self.list.extend(self.get_mcards("selectable"))
                self.index = -1
                self.set_curtain(curtain_all=True)
            self.lock_menucards = False

        # ターゲット選択エリア
        elif self.selectedheader:
            self.clear_fcardsprites()
            self.clear_selection()
            header = self.selectedheader
            owner = header.get_owner()
            assert isinstance(owner, cw.character.Character)
            cardtarget = header.target
            if isinstance(owner, cw.sprite.card.EnemyCard):
                # 敵の行動を選択する時はターゲットの敵味方を入れ替える
                if cardtarget == "Enemy":
                    cardtarget = "Party"
                elif cardtarget == "Party":
                    cardtarget = "Enemy"

            if cardtarget in ("Both", "Enemy", "Party"):
                if self.status == "Scenario":
                    self.set_curtain(target=cardtarget)
                elif self.is_battlestatus():
                    if header.allrange:
                        targets: List[cw.sprite.card.CWPyCard] = []
                        if cardtarget == "Party":
                            targets.extend(self.get_pcards("unreversed"))
                        elif cardtarget == "Enemy":
                            targets.extend(self.get_ecards("unreversed"))
                        else:
                            if isinstance(owner, cw.character.Enemy):
                                targets.extend(self.get_ecards("unreversed"))
                                targets.extend(self.get_pcards("unreversed"))
                            else:
                                targets.extend(self.get_pcards("unreversed"))
                                targets.extend(self.get_ecards("unreversed"))

                        owner.set_action(targets, header)
                        self.clear_specialarea()
                    else:
                        self.set_curtain(target=cardtarget)

                self.lock_menucards = False

            elif cardtarget in ("User", "None"):
                if self.status == "Scenario":
                    assert isinstance(owner, cw.sprite.card.CWPyCard)
                    if cw.cwpy.setting.confirm_beforeusingcard:
                        owner.image = owner.get_selectedimage()

                    def func(owner: cw.sprite.card.CWPyCard) -> None:
                        if cw.cwpy.setting.confirm_beforeusingcard:
                            self.change_selection(owner)
                        self.call_modaldlg("USECARD")
                    self.exec_func(func, owner)
                elif self.is_battlestatus():
                    owner_c = owner
                    assert isinstance(owner_c, cw.sprite.card.CWPyCard)
                    owner.set_action(owner_c, header)
                    self.clear_specialarea()
                    self.lock_menucards = False
                updatestatusbar = False

            else:
                self.lock_menucards = False

        if updatestatusbar:
            self.exec_func(self.statusbar.change, True)
        if not silent:
            self.disposition_pcards()

    def set_testaptitude(self, header: Optional["cw.header.CardHeader"]) -> None:
        if self.areaid not in cw.AREAS_TRADE:
            return
        # 能力適性表示
        for pcard in self.get_pcards("unreversed"):
            pcard.test_aptitude = header
            pcard.update_image()
            # カード種類アイコン表示切替
            if pcard.inusecardimg:
                pcard.inusecardimg.update_scale()
            self.add_lazydraw(clip=pcard.rect)
        # 枚数表示
        if header and cw.cwpy.ydata and cw.cwpy.ydata.party:
            self.show_numberofcards(header.type)
        else:
            self.clear_numberofcards()
        # 売却価格表示
        self.remove_pricesprites()
        if header:
            for mcard in self.get_mcards("visible"):
                if mcard.command == "MoveCard" and mcard.arg == "PAWNSHOP":
                    poc = cw.sprite.background.PriceOfCard(mcard, header, self.cardgrp)
                    self.pricesprites.append(poc)
                    self.add_lazydraw(clip=poc.rect)

    def update_tradecards(self) -> None:
        if self.areaid in cw.AREAS_TRADE and self.status == "Yado":
            self.remove_pricesprites()
            cw.data.redraw_cards(self.is_debugmode(), silent=True)
            self.set_testaptitude(self.selectedheader)

    def remove_pricesprites(self) -> None:
        """価格表示のスプライトを削除する。"""
        for poc in self.pricesprites:
            self.add_lazydraw(clip=poc.rect)
        self.cardgrp.remove(*self.pricesprites)
        self.pricesprites = []

    def clear_specialarea(self, redraw: bool = True, silent: bool = False) -> None:
        """特殊エリアに移動する前のエリアに戻る。
        areaidが-3(パーティ解散)の場合はエリアチェンジする。
        """
        if redraw:
            self.clear_inusecardimg()
            self.clear_guardcardimg()
        self._stored_partyrecord = None
        targetselectionarea = False
        callpredlg = False

        oldareaid = self.areaid
        if self.areaid < 0:
            self.selectedheader = None
            areaid, data = self.pre_areaids.pop()

            if areaid == cw.AREA_CAMP:
                # キャンプ解除でスキルカードの使用回数消滅を確定
                self.sdata.uselimit_table.clear()

            # キャンプ時以外であればカーテン解除
            clear_curtain = areaid != cw.AREA_CAMP

            # パーティ解散エリア解除の場合
            if self.areaid == cw.AREA_BREAKUP:
                self.topgrp.empty()
                for i, pcard in enumerate(self.get_pcards()):
                    pcard.index = i
                    pcard.tlayer = (pcard.tlayer[0], pcard.tlayer[1], i, pcard.tlayer[3])
                    pcard.update_personalownerindex()
                    self.cardgrp.change_layer(pcard, cw.layer_val(pcard.tlayer))
                    self.add_lazydraw(clip=pcard.rect)
                if not silent:
                    self.disposition_pcards()

            # カード移動操作エリアを解除の場合
            if oldareaid in cw.AREAS_TRADE:
                self.areaid = areaid
                self.sdata.change_data(areaid, data=data)
                self.cardgrp.remove(*self.mcards)
                self.mcards = []
                self.mcards_expandspchars.clear()
                self.remove_pricesprites()
                self.file_updates.clear()
                if clear_curtain:
                    self.clear_curtain(redraw=not silent)
                for mcard in self.pre_mcards.pop():
                    if areaid == cw.AREA_CAMP and isinstance(mcard, cw.sprite.card.FriendCard):
                        cw.add_layer(self.cardgrp, mcard, layer=cw.layer_val(mcard.tlayer_t))
                    else:
                        cw.add_layer(self.cardgrp, mcard, layer=cw.layer_val(mcard.tlayer))
                    self.mcards.append(mcard)
                    if mcard.spchars:
                        self.mcards_expandspchars.add(mcard)
                self.deal_cards()
                self.clear_selection()
                self.list = []
                self.list.extend(self.get_mcards("selectable"))
                self.index = -1
            else:
                if cw.cwpy.ydata:
                    changed = cw.cwpy.ydata.is_changed()
                if not silent:
                    force_dealspeed = self.force_dealspeed
                    self.force_dealspeed = self.setting.dealspeed if self.setting.quickdeal else -1
                    try:
                        self.change_area(areaid, data=data, quickdeal=True, specialarea=True,
                                         clear_curtain=clear_curtain)
                    finally:
                        self.force_dealspeed = force_dealspeed
                else:
                    self.areaid = areaid
                    self.sdata.change_data(areaid)
                    if clear_curtain:
                        self.clear_curtain(redraw=redraw)
                if cw.cwpy.ydata:
                    cw.cwpy.ydata._changed = changed
        elif self.is_battlestatus():
            assert self.battle
            self.clear_curtain()
            self.selectedheader = None
            if self.battle.is_ready():
                self.battle.update_showfcards()
            callpredlg = True
        else:
            # ターゲット選択エリアを解除の場合
            self.selectedheader = None
            self.update_tradecards()
            targetselectionarea = True
            if self.is_curtained():
                self.clear_curtain()
            if self.pre_dialogs:
                callpredlg = True

        def func() -> None:
            showbuttons = bool(not self.is_playingscenario() or
                               (self.areaid not in cw.AREAS_TRADE and self.areaid in cw.AREAS_SP) or
                               oldareaid == cw.AREA_CAMP or
                               (targetselectionarea and not self.is_runningevent()) or
                               (self.is_battlestatus() and self.battle and self.battle.is_ready()))
            self.statusbar.change(showbuttons)
        self.exec_func(func)

        if not silent:
            self.disposition_pcards()

        if not callpredlg:
            self.change_selection(self.selection)

        if oldareaid != cw.AREA_CAMP and redraw and not silent:
            self.add_lazydraw(clip=self.background.rect)

        if callpredlg:
            self.call_predlg()

    def clean_specials(self, redraw: bool = True, silent: bool = False) -> None:
        """デバッガやF9で強制的なエリア移動等を発生させる時、
        特殊エリアにいたりバックログを開いていたりした場合は
        クリアして通常状態へ戻す。
        """
        if self.is_showingbacklog():
            self._is_showingbacklog = False
        if self.is_curtained():
            self.pre_dialogs = []
            if self.areaid in cw.AREAS_TRADE:
                self.topgrp.empty()
            self.clear_specialarea(redraw=redraw, silent=silent)

    def check_level(self, fromscenario: bool) -> None:
        """PCの経験点を確認し、条件を満たしていれば
        レベルアップ・ダウン処理を行う。
        fromscenarioがTrueであれば同時に完全回復も行う。
        """
        for pcard in self.get_pcards():
            pcard.adjust_level(fromscenario)

    def create_poschangearrow(self) -> None:
        """パーティ解散エリアにメンバ位置入替用の
        クリック可能スプライトを配置する。
        """
        assert self.ydata
        assert self.ydata.party
        if self.areaid != cw.AREA_BREAKUP:
            return

        if 0 <= self.index and isinstance(self.selection, cw.sprite.background.ClickableSprite):
            index = self.index
            self.clear_selection()
        else:
            index = -1

        self.topgrp.empty()

        def get_image() -> pygame.surface.Surface:
            return self.rsrc.pygamedialogs["REPLACE_POSITION"]

        def get_selimage() -> pygame.surface.Surface:
            bmp = self.rsrc.pygamedialogs["REPLACE_POSITION"].convert_alpha()
            return cw.imageretouch.add_lightness(bmp, 64)

        bmp = self.rsrc.pygamedialogs["REPLACE_POSITION_noscale"]
        w, h = bmp.get_size()
        if isinstance(bmp, cw.util.Depth1Surface):
            scr_scale = bmp.scr_scale
        else:
            scr_scale = 1.0
        w = int(w // scr_scale)
        h = int(h // scr_scale)
        size_noscale = (w, h)
        pcards = self.get_pcards()

        class Replace(object):
            def __init__(self, outer: CWPy, i: int) -> None:
                self.outer = outer
                self.index1 = i
                self.index2 = i+1

            def replace(self) -> None:
                self.outer.replace_pcardorder(self.index1, self.index2)

        seq: List[cw.sprite.base.SelectableSprite] = []
        for i, pcard in enumerate(pcards[0:-1]):
            replace = Replace(self, i)
            pos_noscale = pcard.get_pos_noscale()
            x_noscale = pos_noscale[0] + 95+9//2 - size_noscale[0]//2
            y_noscale = pos_noscale[1] - size_noscale[1] - 5
            sprite = cw.sprite.background.ClickableSprite(get_image, get_selimage,
                                                          (x_noscale, y_noscale),
                                                          self.topgrp, replace.replace)
            seq.append(sprite)
            self.add_lazydraw(clip=sprite.rect)

        if index != -1:
            self.index = index
            self.list = seq
            self.change_selection(self.list[index])

    def replace_pcardorder(self, index1: int, index2: int) -> None:
        """パーティメンバの位置を入れ替える。"""
        assert self.ydata
        assert self.ydata.party
        self.ydata.party.replace_order(index1, index2)
        self.create_poschangearrow()

    def show_numberofcards(self, ctype: str) -> None:
        """カードの所持枚数とカード交換スプライトを表示する。"""
        assert self.ydata
        assert self.ydata.party
        assert self.selectedheader
        self.topgrp.empty()
        if ctype == "SkillCard":
            cardtype = cw.POCKET_SKILL
        elif ctype == "ItemCard":
            cardtype = cw.POCKET_ITEM
        elif ctype == "BeastCard":
            cardtype = cw.POCKET_BEAST
        else:
            assert False
        for pcard in self.get_pcards("unreversed"):
            cw.sprite.background.NumberOfCards(pcard, cardtype, self.topgrp)

        # カード交換用スプライト
        def get_image() -> pygame.surface.Surface:
            return self.rsrc.pygamedialogs["REPLACE_CARDS"]

        def get_selimage() -> pygame.surface.Surface:
            bmp = self.rsrc.pygamedialogs["REPLACE_CARDS"].convert_alpha()
            return cw.imageretouch.add_lightness(bmp, 64)
        bmp = self.rsrc.pygamedialogs["REPLACE_CARDS_noscale"]
        w, h = bmp.get_size()
        if isinstance(bmp, cw.util.Depth1Surface):
            scr_scale = bmp.scr_scale
        else:
            scr_scale = 1.0
        w = int(w // scr_scale)
        h = int(h // scr_scale)
        size_noscale = (w, h)
        pcards = self.get_pcards()

        class ReplaceCards(object):
            def __init__(self, outer: CWPy, pcard: cw.sprite.card.CWPyCard) -> None:
                self.outer = outer
                self.pcard = pcard

            def replace_cards(self) -> None:
                self.outer.change_selection(self.pcard)
                self.outer.call_modaldlg("CARDPOCKET_REPLACE")

        seq: List[cw.sprite.background.ClickableSprite] = []
        for pcard in pcards:
            if pcard.is_reversed():
                continue
            if ctype == "BeastCard":
                if not [c for c in pcard.get_pocketcards(cardtype) if c.attachment]:
                    continue
            else:
                if not pcard.get_pocketcards(cardtype):
                    continue

            replace = ReplaceCards(self, pcard)
            pos_noscale = pcard.get_pos_noscale()
            x_noscale = pos_noscale[0] + cw.setting.get_resourcesize("CardBg/LARGE")[0] - size_noscale[0] - 2
            y_noscale = pos_noscale[1] - size_noscale[1] - 2
            sprite = cw.sprite.background.ClickableSprite(get_image, get_selimage,
                                                          (x_noscale, y_noscale),
                                                          self.topgrp, replace.replace_cards)
            seq.append(sprite)

        if cw.cwpy.setting.show_personal_cards:
            # 荷物袋カード私有スプライト
            def get_image_personal() -> pygame.surface.Surface:
                return self.rsrc.pygamedialogs["TO_PERSONAL_POCKET"]

            def get_selimage_personal() -> pygame.surface.Surface:
                bmp = self.rsrc.pygamedialogs["TO_PERSONAL_POCKET"].convert_alpha()
                return cw.imageretouch.add_lightness(bmp, 64)
            bmp = self.rsrc.pygamedialogs["TO_PERSONAL_POCKET_noscale"]
            w, h = bmp.get_size()
            if isinstance(bmp, cw.util.Depth1Surface):
                scr_scale = bmp.scr_scale
            else:
                scr_scale = 1.0
            w = int(w // scr_scale)
            h = int(h // scr_scale)
            size_noscale = (w, h)

            class AddPersonalPocket(object):
                def __init__(self, outer: CWPy, pcard: cw.sprite.card.PlayerCard, header: cw.header.CardHeader) -> None:
                    self.outer = outer
                    self.pcard = pcard
                    self.header = header

                def add_personalpocket(self) -> None:
                    assert self.outer.ydata
                    assert self.outer.ydata.party
                    if len(self.pcard.personal_pocket) < self.pcard.get_personalpocketspace():
                        if self.header.personal_owner:
                            self.header.personal_owner.remove_personalpocket(self.header)
                        self.outer.trade("BACKPACK", header=self.header, from_event=False, sound=False, sort=False,
                                         call_predlg=False)
                        self.pcard.add_personalpocket(self.header)
                        self.outer.ydata.party.sort_backpack()
                        self.outer.call_predlg()
                    else:
                        self.outer.change_selection(self.pcard)
                        self.outer.call_modaldlg("CARDPOCKET_REPLACE", personal_cards=True)

            seq = []
            for pcard in pcards:
                adp = AddPersonalPocket(self, pcard, self.selectedheader)
                pos_noscale = pcard.get_pos_noscale()
                x_noscale = pos_noscale[0] - 5
                y_noscale = pos_noscale[1] + cw.setting.get_resourcesize("CardBg/LARGE")[1] - size_noscale[1] - 5
                sprite = cw.sprite.background.ClickableSprite(get_image_personal, get_selimage_personal,
                                                              (x_noscale, y_noscale),
                                                              self.topgrp, adp.add_personalpocket)
                sprite.clickable_group = 1
                seq.append(sprite)
                cw.sprite.background.NumberOfCards(pcard, cw.POCKET_PERSONAL, self.topgrp)

    def clear_numberofcards(self) -> None:
        """所持枚数表示を消去する。"""
        sprites = self.topgrp.sprites()
        for sprite in sprites:
            assert sprite.rect is not None
            self.add_lazydraw(clip=sprite.rect)
        self.topgrp.empty()

# ------------------------------------------------------------------------------
# 選択操作用メソッド
# ------------------------------------------------------------------------------

    def clear_selection(self) -> None:
        """全ての選択状態を解除する。"""
        if self.selection:
            self.change_selection(None)

        self.update_mousepos()
        self.update_groups((self.sbargrp,))

    def change_selection(self, sprite: Optional["cw.sprite.base.SelectableSprite"],
                         forceredraw: Optional["cw.sprite.base.SelectableSprite"] = None) -> None:
        """引数のスプライトを選択状態にする。
        sprite: SelectableSprite
        """
        self.has_inputevent = True
        sbarbtn1 = self.selection and self.selection.is_statusctrl
        sbarbtn2 = sprite and sprite.is_statusctrl

        # 現在全員の戦闘行動を表示中か
        show_allselectedcards = self._show_allselectedcards
        if sprite and not isinstance(sprite, cw.sprite.background.Curtain):
            # 特定の誰かが選択された場合は表示を更新
            show_allselectedcards = False
        elif not self._in_partyarea(self.mousepos):
            # パーティ領域より上へマウスカーソルが行ったら表示をクリア
            show_allselectedcards = False

        if self.selection:
            self.selection.image = self.selection.get_unselectedimage()
        oldsel = self.selection

        # カードイベント中にtargetarrow, inusecardimgを消さないため
        if not self.is_runningevent():
            self.clear_targetarrow()
            self.clear_inusecardimg()

        if sprite:
            sprite.image = sprite.get_selectedimage()
        else:
            self.index = -1

        self.selection = sprite

        if oldsel != sprite:
            if oldsel:
                self.add_lazydraw(clip=oldsel.rect)
            if sprite:
                self.add_lazydraw(clip=sprite.rect)
        if forceredraw:
            self.add_lazydraw(clip=forceredraw.rect)

        if (not self.is_runningevent()
                and isinstance(sprite, cw.character.Character)
                and (not self.selectedheader or self.is_battlestatus())
                and sprite.is_analyzable())\
                or show_allselectedcards:
            seq: Iterable[Union[cw.sprite.base.SelectableSprite, List[cw.header.CardHeader]]] =\
                itertools.chain(self.get_pcards("unreversed"),
                                self.get_ecards("unreversed"),
                                self.get_fcards("unreversed"))
        elif not self.is_runningevent() and self.selectedheader and self.selectedheader.get_owner():
            howner = self.selectedheader.get_owner()
            assert isinstance(howner, (cw.sprite.base.SelectableSprite, list))
            seq = [howner]
        elif forceredraw:
            seq = [forceredraw]
        else:
            seq = []

        for sprite2 in seq:
            if not self.selectedheader or sprite2 != self.selectedheader.get_owner():
                if not (isinstance(sprite2, cw.character.Character)
                        and sprite2.actiondata
                        and sprite2.is_analyzable()
                        and sprite2.status != "hidden"):
                    continue

            selowner = self.selectedheader and self.selectedheader.get_owner() == sprite2
            assert self.ydata
            if self.ydata.party and self.ydata.party.backpack == sprite2:
                mcards = self.get_mcards("visible")
                for mcard in mcards:
                    if isinstance(mcard, cw.sprite.card.MenuCard) and mcard.is_backpack():
                        sprite2 = mcard
                        break
                else:
                    continue
            elif self.ydata.storehouse == sprite2:
                mcards = self.get_mcards("visible")
                for mcard in mcards:
                    if isinstance(mcard, cw.sprite.card.MenuCard) and mcard.is_storehouse():
                        sprite2 = mcard
                        break
                else:
                    continue

            assert isinstance(sprite2, cw.sprite.card.CWPyCard), sprite2
            assert isinstance(sprite2, (cw.character.Character, cw.sprite.card.MenuCard)), sprite2
            self.clear_inusecardimg(sprite2)

            if selowner:
                header = self.selectedheader
                targets: List[cw.sprite.card.CWPyCard] = []
            elif isinstance(sprite2, cw.character.Character) and sprite2.actiondata:
                targets_c, header, _beasts = sprite2.actiondata
                if isinstance(targets_c, cw.character.Character):
                    assert isinstance(targets_c, cw.sprite.card.CWPyCard)
                    targets = [targets_c]
                else:
                    targets = targets_c
            else:
                targets = []
                header = None

            if self.selection == sprite2 and not selowner:
                if cw.cwpy.setting.show_lifebar_on_selection and isinstance(sprite2, cw.character.Character) and\
                        sprite2.is_analyzable() and not sprite2.is_unconscious():
                    assert isinstance(sprite2, (cw.sprite.card.PlayerCard,
                                                cw.sprite.card.EnemyCard,
                                                cw.sprite.card.FriendCard))
                    cw.sprite.background.LifeBar(sprite2)

            if header:
                if self.selection == sprite2 and not selowner:
                    # カーソル下のカード。常に手前に表示
                    self.set_inusecardimg(sprite2, header, fore=True)
                    if header.target == "None":
                        self.set_targetarrow([sprite2])
                    elif targets:
                        self.set_targetarrow(targets)
                elif self.setting.show_allselectedcards or selowner:
                    if header.personal_owner and self.areaid in cw.AREAS_TRADE and header is self.selectedheader and\
                            self.setting.show_personal_cards:
                        owner: cw.sprite.card.CWPyCard = header.personal_owner
                    else:
                        assert isinstance(sprite2, cw.sprite.card.CWPyCard)
                        owner = sprite2
                    assert isinstance(owner, cw.sprite.card.CWPyCard)
                    if self.setting.show_aim and self.selection and not selowner and self.selection in targets:
                        alpha = 255  # カーソル下のカードを狙っている場合は不透明表示
                        fore = True
                    else:
                        alpha = cw.cwpy.setting.get_inusecardalpha(owner)
                        fore = header == self.selectedheader
                    self.set_inusecardimg(owner, header, alpha=alpha, fore=fore)

                if self.setting.show_allselectedcards and isinstance(sprite2, cw.sprite.card.PlayerCard):
                    show_allselectedcards = True

        self._show_allselectedcards = show_allselectedcards

        # ステータスボタン上であれば必ず矢印カーソルとする
        if bool(sbarbtn1) != bool(sbarbtn2):
            self.change_cursor(self.cursor, force=True)

    def set_inusecardimg(self, owner: "cw.sprite.card.CWPyCard", header: "cw.header.CardHeader", status: str = "normal",
                         center: bool = False, alpha: int = 255, fore: bool = False) -> None:
        """PlayerCardの前に使用中カードの画像を表示。"""
        if center or (not owner.inusecardimg and self.background.rect.colliderect(owner.rect) and
                      owner.status != "hidden"):
            inusecard = cw.sprite.background.InuseCardImage(owner, header, status, center, alpha=alpha, fore=fore)
            owner.inusecardimg = inusecard
            self.add_lazydraw(clip=inusecard.rect)
            self.inusecards.append(inusecard)

    def clear_inusecardimg(self, user: Optional["cw.sprite.card.CWPyCard"] = None) -> None:
        """PlayerCardの前の使用中カードの画像を削除。"""
        self._show_allselectedcards = False
        lifebars = self.cardgrp.get_sprites_from_layer(cw.layer_val(cw.LAYER_FRONT_LIFEBAR))
        if lifebars:
            lifebar = lifebars[0]
            assert isinstance(lifebar, cw.sprite.background.LifeBar)
            if user is None or lifebar.ccard is user:
                self.cardgrp.remove_sprites_of_layer(cw.layer_val(cw.LAYER_FRONT_LIFEBAR))
        if user:
            if user.inusecardimg:
                user.inusecardimg.group.remove(user.inusecardimg)
                self.inusecards.remove(user.inusecardimg)
                self.add_lazydraw(user.inusecardimg.rect)
                user.inusecardimg = None
        else:
            for pcard in self.get_pcards():
                pcard.inusecardimg = None
            for mcard in self.get_mcards():
                mcard.inusecardimg = None
            for fcard in self.get_fcards():
                fcard.inusecardimg = None

            for inusecard in self.inusecards:
                inusecard.group.remove(inusecard)
                self.add_lazydraw(inusecard.rect)
            self.inusecards = []

    def clear_inusecardimgfromheader(self, header: "cw.header.CardHeader") -> None:
        """表示中の使用中カードの中にheaderのものが
        含まれていた場合は削除。
        """
        for card in list(self.inusecards):
            if card.header == header:
                if card.user:
                    self.clear_inusecardimg(card.user)
                    if card.user.status != "hidden":
                        cw.animation.animate_sprite(card.user, "hide")
                        cw.animation.animate_sprite(card.user, "deal")
                else:
                    card.group.remove(card)
                    self.inusecards.remove(card)
                    self.add_lazydraw(card.rect)

    def set_guardcardimg(self, owner: "cw.sprite.card.CWPyCard", header: "cw.header.CardHeader") -> None:
        """PlayerCardの前に回避・抵抗ボーナスカードの画像を表示。"""
        if not self.get_guardcardimg() and self.background.rect.colliderect(owner.rect) and owner.status != "hidden":
            card = cw.sprite.background.InuseCardImage(owner, header, status="normal", center=False)
            self.add_lazydraw(clip=card.rect)
            self.guardcards.append(card)

    def clear_guardcardimg(self) -> None:
        """PlayerCardの前の回避・抵抗ボーナスカードの画像を削除。"""
        for card in self.guardcards:
            card.group.remove(card)
            self.add_lazydraw(clip=card.rect)
        self.guardcards = []

    def set_targetarrow(self, targets: Iterable["cw.sprite.card.CWPyCard"]) -> None:
        """targets(PlayerCard, MenuCard, CastCard)の前に
        対象選択の指矢印の画像を表示。
        """
        if not self.cardgrp.get_sprites_from_layer(cw.layer_val(cw.LAYER_TARGET_ARROW)):
            for target in targets:
                if target.status != "hidden":
                    arrow = cw.sprite.background.TargetArrow(target)
                    cw.cwpy.add_lazydraw(clip=arrow.rect)

    def clear_targetarrow(self) -> None:
        """対象選択の指矢印の画像を削除。"""
        arrows = self.cardgrp.get_sprites_from_layer(cw.layer_val(cw.LAYER_TARGET_ARROW))
        for arrow in arrows:
            assert arrow.rect is not None
            cw.cwpy.add_lazydraw(clip=arrow.rect)
        self.cardgrp.remove_sprites_of_layer(cw.layer_val(cw.LAYER_TARGET_ARROW))

    def update_selectablelist(self) -> None:
        """状況に応じて矢印キーで選択対象となる
        カードのリストを更新する。"""
        self.list = []
        if self.is_pcardsselectable:
            self.list.extend(self.get_pcards("selectable"))
        elif self.is_mcardsselectable:
            self.list.extend(self.get_mcards("selectable"))
        self.index = -1

    def set_curtain(self, target: str = "Both", curtain_all: bool = False, redraw: bool = True) -> None:
        """Curtainスプライトをセットする。"""
        if not self.is_curtained():
            self.is_pcardsselectable = target in ("Both", "Party")
            self.is_mcardsselectable = not self.is_battlestatus() or target in ("Both", "Enemy")
            self.update_selectablelist()

            # カード上のカーテン
            if not self.is_pcardsselectable:
                for pcard in self.get_pcards():
                    cw.sprite.background.Curtain(pcard, self.cardgrp)
            if not self.is_mcardsselectable:
                for mcard in self.get_mcards("visible"):
                    cw.sprite.background.Curtain(mcard, self.cardgrp)

            # 背景上のカーテン
            self.background.set_curtain(curtain_all=curtain_all)

            self._curtained = True

    def clear_curtain(self, redraw: bool = True) -> None:
        """Curtainスプライトを解除する。"""
        if self.is_curtained():
            self.background.clear_curtain()
            self.cardgrp.remove(*self.curtains)
            self.curtains = []
            self._curtained = False
            self.is_pcardsselectable = bool(self.ydata and self.ydata.party)
            self.is_mcardsselectable = True

    def cancel_cardcontrol(self) -> None:
        """カードの移動や使用の対象選択をキャンセルする。"""
        if self.is_curtained():
            self.play_sound("click", )

            if self.areaid in cw.AREAS_TRADE:
                # カード移動選択エリアだったら、事前に開いていたダイアログを開く
                self.selectedheader = None
                self.update_tradecards()
                self.call_predlg()
            else:
                # それ以外だったら特殊エリアをクリアする
                self.clear_specialarea(redraw=False)

    def is_lockmenucards(self, sprite: Optional["cw.sprite.base.SelectableSprite"]) -> bool:
        """メニューカードをクリック出来ない状態か。"""
        if (isinstance(sprite, cw.sprite.animationcell.AnimationCell) or
            (sprite and sprite.is_statusctrl)) and\
                sprite.selectable_on_event:
            return False
        return bool(
            self.lock_menucards or self.is_showingdlg() or
            pygame.event.peek(pygame.USEREVENT) or
            self.is_showingbacklog()
        )

# ------------------------------------------------------------------------------
# プレイ用メソッド
# ------------------------------------------------------------------------------

    def elapse_time(self, playeronly: bool = False, fromevent: bool = False) -> None:
        """時間経過。"""
        cw.cwpy.advlog.start_timeelapse()
        self._elapse_time = True

        ccards: List[cw.character.Character] = []
        ccards.extend(self.get_pcards("unreversed"))
        if not playeronly:
            ccards.extend(self.get_ecards("unreversed"))
            ccards.extend(self.get_fcards("unreversed"))

        try:
            for ccard in ccards:
                try:
                    if fromevent:
                        if cw.cwpy.event.has_selectedmember():
                            selmember: Optional[cw.character.Character] = cw.cwpy.event.get_selectedmember()
                            cw.cwpy.event.set_selectedmember(None)
                        else:
                            selmember = None
                        selcard = cw.cwpy.event.get_selectedcard()
                        cw.cwpy.event.set_selectedcard(cw.cwpy.event.get_inusecard())

                    ccard.set_timeelapse(fromevent=fromevent)

                    if fromevent:
                        cw.cwpy.event.set_selectedmember(selmember)
                        cw.cwpy.event.set_selectedcard(selcard)

                except cw.event.EffectBreakError:
                    if fromevent:
                        raise
                    else:
                        # 時間経過コンテント以外で時間経過が起きている場合、
                        # 効果中断されても以降のキャラクターの処理は継続
                        pass
        finally:
            self._elapse_time = False

        if self.is_gameover() and cw.cwpy.sdata.can_gameover() and self.is_playingscenario():
            if cw.cwpy.is_battlestatus():
                if fromevent:
                    raise cw.event.EffectBreakError()
                else:
                    raise cw.battle.BattleDefeatError()
            else:
                cw.cwpy.set_gameover()
                if fromevent:
                    raise cw.event.ScenarioBadEndError()

    def interrupt_adventure(self) -> None:
        """冒険の中断。宿画面に遷移する。"""
        assert self.ydata
        assert self.ydata.party
        if self.status == "Scenario":
            assert isinstance(self.sdata, cw.data.ScenarioData)
            self.sdata.sleep_timekeeper()
            self.sdata.update_log()
            for music in self.music:
                music.stop()
            cw.util.remove(cw.util.join_paths(cw.tempdir, "ScenarioLog"))
            self.ydata.load_party(None)

            if not self.areaid >= 0:
                self.areaid, _data = self.pre_areaids[0]

            self.set_yado()

    def load_party(self, header: Optional["cw.header.PartyHeader"] = None, chgarea: bool = True, newparty: bool = False,
                   loadsprites: bool = True) -> None:
        """パーティデータをロードする。
        header: PartyHeader。指定しない場合はパーティデータを空にする。
        """
        assert self.ydata
        self.ydata.load_party(header)

        if chgarea:
            if header:
                areaid = 2
            else:
                areaid = 1
            self.change_area(areaid, bginhrt=False, newparty=newparty)
        elif newparty:
            for pcard in self.pcards:
                pcard.hide()
            self.cardgrp.remove(*self.pcards)
            self.pcards = []
            if loadsprites:
                assert self.ydata.party
                for i, e in enumerate(self.ydata.party.members):
                    pos_noscale = (9 + 95 * i + 9 * i, 285)
                    pcard = cw.sprite.card.PlayerCard(e, pos_noscale=pos_noscale, index=i)
                    pcard.restore_personalpocket()
                self.show_party()
        else:
            # 新規パーティ結成
            for pcard in self.pcards:
                pcard.hide()
            self.cardgrp.remove(*self.pcards)
            self.pcards = []
            if loadsprites and self.ydata.party:
                e = self.ydata.party.members[0]
                pcardsnum = len(self.ydata.party.members) - 1
                pos_noscale = (9 + 95 * pcardsnum + 9 * pcardsnum, 285)
                pcard = cw.sprite.card.PlayerCard(e, pos_noscale=pos_noscale, index=pcardsnum)
                pcard.set_pos_noscale(pos_noscale)
                pcard.restore_personalpocket()
                self.ydata.party.sort_backpack()
                cw.animation.animate_sprite(pcard, "deal")

        clip = cw.cwpy.update_statusimgs(is_runningevent=False)
        if clip:
            cw.cwpy.add_lazydraw(clip=clip)

        self.is_pcardsselectable = bool(self.ydata and self.ydata.party)

    def dissolve_party(self, pcard: Optional["cw.sprite.card.PlayerCard"] = None, cleararea: bool = True) -> None:
        """現在選択中のパーティからpcardを削除する。
        pcardがない場合はパーティ全体を解散する。
        """
        assert self.ydata
        assert self.ydata.party

        breakuparea = (self.areaid == cw.AREA_BREAKUP)

        if pcard:
            if not breakuparea:
                return
            self.play_sound("page")
            pcard.remove_numbercoupon()
            pcards = self.get_pcards()
            index = pcards.index(pcard)
            arrows = self.topgrp.sprites()
            sprites: List[cw.sprite.base.CWPySprite] = [pcard]
            if index < len(arrows):
                arrow = arrows[index]
                assert isinstance(arrow, cw.sprite.base.CWPySprite)
                sprites.append(arrow)
            elif 0 < index and index == len(arrows):
                arrow = arrows[-1]
                assert isinstance(arrow, cw.sprite.base.CWPySprite)
                sprites.append(arrow)
            cw.animation.animate_sprites(sprites, "delete")
            if breakuparea and pcards:
                self.create_poschangearrow()
            pcard.data.write_xml()
            self.ydata.add_standbys(pcard.data.fpath)

            if not self.get_pcards():
                self.dissolve_party()

        else:
            pcards = self.get_pcards()
            sprites = []
            sprites.extend(pcards)
            if breakuparea:
                sprites.extend([sprite for sprite in self.topgrp.sprites()
                                if isinstance(sprite, cw.sprite.base.CWPySprite)])
            cw.animation.animate_sprites(sprites, "hide")
            if breakuparea:
                self.topgrp.empty()

            for pcard in pcards:
                pcard.remove_numbercoupon()
                pcard.store_personalpocket()
                pcard.hide()
                self.cardgrp.remove(pcard)
                pcard.data.write_xml()
            self.pcards = []

            p_money = int(self.ydata.party.data.getint("Property/Money", 0))
            p_members = [member.fpath for member in self.ydata.party.members]
            p_backpack = self.ydata.party.backpack[:]
            p_backpack.reverse()
            for header in p_backpack:
                self.trade("STOREHOUSE", header=header, from_event=True, sort=False)
            self.ydata.sort_storehouse()

            self.ydata.deletedpaths.add(os.path.dirname(self.ydata.party.data.fpath))
            self.ydata.party.members = []
            self.ydata.load_party(None)
            self.ydata.environment.edit("Property/NowSelectingParty", "")
            self.ydata.set_money(p_money)

            for path in reversed(p_members):
                self.ydata.add_standbys(path, sort=False)
            self.ydata.sort_standbys()

            if breakuparea:
                self._save_partyrecord()
                if cleararea:
                    self.pre_areaids[-1] = (1, None)
                    self.clear_specialarea()

    def get_partyrecord(self) -> "StoredParty":
        """現在のパーティ情報の記録を生成して返す。"""
        assert self.ydata
        assert self.ydata.party
        return StoredParty(self.ydata.party)

    def _store_partyrecord(self) -> None:
        """解散操作前にパーティ情報を記録する。"""
        self._stored_partyrecord = self.get_partyrecord()

    def _save_partyrecord(self) -> None:
        """解散時にパーティ情報をファイルへ記録する。"""
        assert self.ydata
        if not self._stored_partyrecord:
            return
        if not self.setting.autosave_partyrecord:
            return

        if self.setting.overwrite_partyrecord:
            self.ydata.replace_partyrecord(self._stored_partyrecord)
        else:
            self.ydata.add_partyrecord(self._stored_partyrecord)

    def save_partyrecord(self) -> None:
        """現在のパーティ情報を記録する。"""
        assert self.ydata
        assert self.ydata.party
        if not self.setting.autosave_partyrecord:
            return

        partyrecord = self.get_partyrecord()
        if self.setting.overwrite_partyrecord:
            self.ydata.replace_partyrecord(partyrecord)
        else:
            self.ydata.add_partyrecord(partyrecord)

    def set_mastervolume(self, volume: int) -> None:
        for music in cw.cwpy.music:
            if not cw.cwpy.frame.is_iconized:
                music.set_mastervolume(volume)
            music.set_volume()
        for sound in cw.cwpy.lastsound_scenario:
            if sound:
                sound.set_mastervolume(True, volume)
                sound.set_volume(True)
        if cw.cwpy.lastsound_system:
            cw.cwpy.lastsound_system.set_mastervolume(False, volume)
            cw.cwpy.lastsound_system.set_volume(False)

    def play_sound(self, name: str, from_scenario: bool = False, subvolume: int = 100, loopcount: int = 1,
                   channel: int = 0, fade: int = 0, material_override: bool = False) -> None:
        if channel < 0 or cw.bassplayer.MAX_SOUND_CHANNELS <= channel:
            return
        if self != threading.currentThread():
            self.exec_func(self.play_sound, name, from_scenario, subvolume, loopcount, channel, fade)
            return

        if material_override:
            # シナリオ側でスキン付属効果音を上書きする
            sound = self.sounds[name]
            path = os.path.basename(sound.get_path())
            path = cw.util.splitext(path)[0]
            path = cw.util.join_paths(self.sdata.scedir, path)
            path = os.path.basename(cw.util.find_resource(path, cw.M_SND))
            inusecard = self.event.get_inusecard()
            if self._play_sound_with(path, from_scenario, inusecard=inusecard, subvolume=subvolume,
                                     loopcount=loopcount, channel=channel, fade=fade):
                return

        self.sounds[name].copy().play(from_scenario, subvolume=subvolume, loopcount=loopcount, channel=channel,
                                      fade=fade)

    def _play_sound_with(self, path: str, from_scenario: bool, inusecard: Optional["cw.header.CardHeader"] = None,
                         subvolume: int = 100, loopcount: int = 1, channel: int = 0, fade: int = 0) -> bool:
        if not path:
            return False
        inusesoundpath = cw.util.get_inusecardmaterialpath(path, cw.M_SND, inusecard)
        if os.path.isfile(inusesoundpath):
            path = inusesoundpath
        else:
            path = cw.util.get_materialpath(path, cw.M_SND, system=self.areaid < 0)
        if os.path.isfile(path):
            cw.util.load_sound(path).play(from_scenario, subvolume=subvolume, loopcount=loopcount, channel=channel,
                                          fade=fade)
            return True
        return False

    def play_sound_with(self, path: str, inusecard: Optional["cw.header.CardHeader"] = None, subvolume: int = 100,
                        loopcount: int = 1, channel: int = 0, fade: int = 0) -> None:
        """効果音を再生する。
        シナリオ効果音・スキン効果音を適宜使い分ける。
        """
        if not path:
            return
        if channel < 0 or cw.bassplayer.MAX_SOUND_CHANNELS <= channel:
            return
        if self._play_sound_with(path, True, inusecard, subvolume=subvolume, loopcount=loopcount, channel=channel,
                                 fade=fade):
            return

        name = cw.util.splitext(os.path.basename(path))[0]

        if name in self.skinsounds:
            self.skinsounds[name].copy().play(True, subvolume=subvolume, loopcount=loopcount, channel=channel,
                                              fade=fade)

    def has_sound(self, path: str) -> bool:
        if not path:
            return False

        path = cw.util.get_materialpath(path, cw.M_SND, system=self.areaid < 0)

        if os.path.isfile(path):
            return True
        else:
            name = cw.util.splitext(os.path.basename(path))[0]
            return name in self.skinsounds

# ------------------------------------------------------------------------------
# データ編集・操作用メソッド。
# ------------------------------------------------------------------------------

    def trade(self, targettype: str,
              target: Optional[Union[List["cw.header.CardHeader"], "cw.character.Character"]] = None,
              header: Optional["cw.header.CardHeader"] = None,
              from_event: bool = False, parentdialog: wx.Dialog = None, toindex: int = -1,
              insertorder: int = -1, sort: bool = True, sound: bool = True, party: Optional[cw.data.Party] = None,
              from_getcontent: bool = False, call_predlg: bool = True,
              clearinusecard: bool = True, update_image: bool = True) -> None:
        """
        カードの移動操作を行う。
        Getコンテントからこのメソッドを操作する場合は、
        ownerはNoneにする。
        """
        assert self.ydata

        # カード移動操作用データを読み込む
        if self.selectedheader and not header:
            assert self.selectedheader
            header = self.selectedheader
        assert header

        if not party:
            party = self.ydata.party

        if party:
            is_playingscenario = party.is_adventuring()
        else:
            is_playingscenario = self.is_playingscenario()

        if header.is_backpackheader() and party:
            owner: Optional[Union[List[cw.header.CardHeader], cw.character.Character]] = party.backpack
        else:
            owner = header.get_owner()

        # 荷物袋<=>カード置場のため
        # ファイルの移動だけで済む場合
        move = (targettype in ("BACKPACK", "STOREHOUSE")) and\
            ((owner is self.ydata.storehouse) or (party and owner is party.backpack)) and\
            (not is_playingscenario)

        # カード置場・荷物袋内での位置の移動の場合
        toself = (targettype == "BACKPACK" and party and owner is party.backpack) or\
                 (targettype == "STOREHOUSE" and owner is self.ydata.storehouse)

        if not toself and targettype not in ("PAWNSHOP", "TRASHBOX"):
            header.do_write()

        # 移動先を設定。
        if targettype == "PLAYERCARD":
            target = target
        elif targettype == "BACKPACK":
            assert party
            target = party.backpack
        elif targettype == "STOREHOUSE":
            target = self.ydata.storehouse
        elif targettype in ("PAWNSHOP", "TRASHBOX"):

            # プレミアカードは売却・破棄できない(イベントからの呼出以外)
            if not self.debug and self.setting.protect_premiercard and\
                    header.premium == "Premium" and not from_event:
                if targettype == "PAWNSHOP":
                    s = self.msgs["error_sell_premier_card"]
                    self.call_modaldlg("NOTICE", text=s, parentdialog=parentdialog)
                elif targettype == "TRASHBOX":
                    s = self.msgs["error_dump_premier_card"] % (header.name)
                    self.call_modaldlg("NOTICE", text=s, parentdialog=parentdialog)

                return

            # スターつきのカードは売却・破棄できない(イベントからの呼出以外)
            if self.setting.protect_staredcard and header.star and not from_event:
                if targettype == "PAWNSHOP":
                    s = self.msgs["error_sell_stared_card"]
                    self.call_modaldlg("NOTICE", text=s, parentdialog=parentdialog)
                elif targettype == "TRASHBOX":
                    s = self.msgs["error_dump_stared_card"]
                    self.call_modaldlg("NOTICE", text=s, parentdialog=parentdialog)

                return

            if targettype == "PAWNSHOP":
                price = header.sellingprice
                if not from_event and (self.setting.confirm_dumpcard == cw.setting.CONFIRM_DUMPCARD_ALWAYS or
                                       (self.setting.confirm_dumpcard == cw.setting.CONFIRM_DUMPCARD_SENDTO and
                                        parentdialog)):
                    if sound:
                        cw.cwpy.play_sound("page")
                    s = cw.cwpy.msgs["confirm_sell"] % (header.name, price)
                    self.call_modaldlg("YESNO", text=s, parentdialog=parentdialog)
                    if self.get_yesnoresult() != wx.ID_OK:
                        return
            else:
                if not from_event and (self.setting.confirm_dumpcard == cw.setting.CONFIRM_DUMPCARD_ALWAYS or
                                       (self.setting.confirm_dumpcard == cw.setting.CONFIRM_DUMPCARD_SENDTO and
                                        parentdialog)):
                    if sound:
                        cw.cwpy.play_sound("page")
                    s = cw.cwpy.msgs["confirm_dump"] % (header.name)
                    self.call_modaldlg("YESNO", text=s, parentdialog=parentdialog)
                    if self.get_yesnoresult() != wx.ID_OK:
                        return

            target = None
        else:
            cw.cwpy.call_dlg("ERROR", text="「%s」は不正なカード移動先です。" % targettype)
            return

        # 手札カードダイアログ用のインデックスを取得する
        if header.type == "SkillCard":
            index = 0
        elif header.type == "ItemCard":
            index = 1
        elif header.type == "BeastCard":
            index = 2
        else:
            raise ValueError("CARDPOCKET Index in trade method is incorrect.")

        # もし移動先がPlayerCardだったら、手札の枚数判定を行う
        if targettype == "PLAYERCARD" and target != owner:
            assert isinstance(target, cw.character.Character)
            n = len(target.cardpocket[index])
            maxn = target.get_cardpocketspace()[index]

            # 手札が一杯だったときの処理
            if n + 1 > maxn:
                if from_event:
                    # 互換動作: 1.20以前では手札が一杯でも荷物袋に入らない
                    if not (from_getcontent and cw.cwpy.sdata and
                            cw.cwpy.sct.lessthan("1.20", cw.cwpy.sdata.get_versionhint(frompos=cw.HINT_AREA))):
                        self.trade("BACKPACK", header=header, from_event=True, sort=sort, party=party,
                                   from_getcontent=from_getcontent)

                else:
                    s = cw.cwpy.msgs["error_hand_be_full"] % target.name
                    self.call_modaldlg("NOTICE", text=s, parentdialog=parentdialog)

                return

        # 音を鳴らす
        if not from_event:
            if targettype == "TRASHBOX":
                self.play_sound("dump")
            elif targettype == "PAWNSHOP":
                self.play_sound("signal")
            elif sound:
                self.play_sound("page")

        # 宿状態の変化を通知
        if cw.cwpy.ydata:
            cw.cwpy.ydata.changed()

        # ----------------------------------------------------------------------
        # 移動元からデータを削除
        # ----------------------------------------------------------------------

        hold = header.hold
        fromplayer = isinstance(owner, cw.character.Player)

        selectedcard = cw.cwpy.event.get_selectedcard()
        if selectedcard and header.ref_original() == selectedcard.ref_original():
            cw.cwpy.event.set_selectedcard(cw.cwpy.event.get_inusecard())

        # 移動元がCharacterだった場合
        if isinstance(owner, cw.character.Character):
            assert not move
            assert header.personal_owner is None
            assert header.personal_owner_index == (-1, -1)
            # 移動元のCardHolderからCardHeaderを削除
            orig = header.ref_original()
            assert orig
            owner.cardpocket[index].remove(orig)
            # 移動元からカードのエレメントを削除
            path = "%ss" % header.type
            owner.data.remove(path, header.carddata)
            # 戦闘中だった場合はデッキからも削除
            owner.deck.remove(owner, header)
            if target != owner and clearinusecard and self.areaid not in cw.AREAS_TRADE:
                self.clear_inusecardimgfromheader(header)

            # 行動予定に入っていればキャンセル
            action = owner.actiondata
            if action:
                targets, aheader, beasts = action
                if aheader and aheader.ref_original() == header.ref_original():
                    aheader = None
                    targets = None
                beasts2 = []
                for targets_b, beast in beasts:
                    if beast.ref_original() != header.ref_original():
                        beasts2.append((targets_b, beast))
                owner.set_action(targets, aheader, beasts2, True)

            # スキルの場合は使用回数を0にする
            if header.type == "SkillCard" and owner != target:
                if self.is_playingscenario() and not from_event and self.areaid == cw.AREA_TRADE3 and\
                        isinstance(owner, cw.character.Player) and header.uselimit and\
                        not (owner, header) in self.sdata.uselimit_table:
                    # キャンプ中は元々の使用回数を記憶しておき、
                    # 元の所有者の手許に戻ったら使用回数を復元する
                    assert isinstance(owner, cw.sprite.card.PlayerCard)
                    self.sdata.uselimit_table[(owner, header)] = header.uselimit
                header.maxuselimit = 0
                header.uselimit = 0
                assert header.carddata is not None
                header.carddata.find_exists("Property/UseLimit").text = "0"

            # ホールドをFalseに
            header.hold = False

            if not header.type == "BeastCard":
                assert header.carddata is not None
                header.carddata.find_exists("Property/Hold").text = "False"

            header.set_owner(None)

        # 移動元が荷物袋だった場合
        elif party and owner is party.backpack:
            # 移動元のリストからCardHeaderを削除
            orig = header.ref_original()
            assert orig
            owner.remove(orig)
            if header.personal_owner and (not toself or self.setting.show_personal_cards):
                header.personal_owner.remove_personalpocket(orig)

            if toself:
                # 荷物袋内の位置のみ変更
                pass
            elif header.scenariocard:
                # シナリオで入手したカードはそのまま削除してよい
                header.contain_xml(load=targettype not in ("PAWNSHOP", "TRASHBOX"))
            else:
                if is_playingscenario:
                    if not header.carddata:
                        e = cw.data.yadoxml2etree(header.fpath)
                        header.carddata = e.getroot()
                        header.flags = cw.data.init_flags(header.carddata, True)
                        header.steps = cw.data.init_steps(header.carddata, True)
                        header.variants = cw.data.init_variants(header.carddata, True)
                    # シナリオプレイ中であれば削除フラグを立てて削除を保留
                    # (F9時に復旧する必要があるため)
                    if targettype in ("PAWNSHOP", "TRASHBOX"):
                        # 移動先がゴミ箱・下取りだったら完全削除予約
                        moved = 2
                    else:
                        # どこかに残る場合
                        moved = 1

                    etree = cw.data.xml2etree(element=header.carddata)
                    etree.edit("Property", str(moved), "moved")
                    etree.write_xml()
                    header.moved = moved
                    header2 = cw.header.CardHeader(carddata=header.carddata)
                    header2.fpath = header.fpath
                    party.backpack_moved.append(header2)
                    header.fpath = ""
                elif move:
                    # ファイルの移動のみ
                    self.ydata.deletedpaths.add(header.fpath, header.scenariocard)
                else:
                    # 宿にいる場合はそのまま削除する
                    header.contain_xml()

        # 移動元がカード置場だった場合
        elif owner is self.ydata.storehouse:
            assert header.personal_owner is None
            assert header.personal_owner_index == (-1, -1)
            # 移動元のリストからCardHeaderを削除
            orig = header.ref_original()
            assert orig
            owner.remove(orig)
            if toself:
                # カード置場内の位置のみ変更
                pass
            elif move:
                # ファイルの移動のみ
                self.ydata.deletedpaths.add(header.fpath, header.scenariocard)
            else:
                header.contain_xml()

        # 移動元が存在しない場合(get or loseコンテンツから呼んだ場合)
        else:
            assert not move
            assert header.personal_owner is None
            assert header.personal_owner_index == (-1, -1)
            header.contain_xml()

        if header == self.selectedheader:
            self.clear_inusecardimg()

        # ----------------------------------------------------------------------
        # ファイル削除
        # ----------------------------------------------------------------------

        # 移動先がゴミ箱・下取りだったら
        if targettype in ("PAWNSHOP", "TRASHBOX"):
            assert not move
            # 付帯以外の召喚獣カードの場合
            if header.type == "BeastCard" and not header.attachment and\
                    isinstance(owner, cw.character.Character):
                if update_image:
                    assert isinstance(owner, cw.sprite.card.CWPyCard)
                    owner.update_image()
            # シナリオで取得したカードじゃない場合、XMLの削除
            elif not header.scenariocard and header.moved == 0:
                self.remove_xml(header)
                if fromplayer and is_playingscenario:
                    # PCによってシナリオへ持ち込まれたカードを破棄する際は
                    # デバッグログに出すために記録しておく
                    # (荷物袋からの破棄・移動はbackpack_movedに入るため不要)
                    dcpath = cw.util.join_paths(cw.tempdir, "ScenarioLog/Party/Deleted" + header.type)
                    if not os.path.isdir(dcpath):
                        os.makedirs(dcpath)
                    dfpath = cw.util.join_paths(dcpath, cw.util.repl_dischar(header.name) + ".xml")
                    dfpath = cw.util.dupcheck_plus(dfpath, yado=False)
                    etree = cw.data.xml2etree(element=header.carddata)
                    etree.write_file(dfpath)
            elif not header.scenariocard and header.moved == 1:
                # 荷物袋からPCへ移動してそこから除去した場合
                assert self.ydata.party
                assert header.carddata is not None
                header.contain_xml()
                etree = cw.data.xml2etree(element=header.carddata)
                etree.edit("Property", "2", "moved")
                header.moved = 2
                header.set_owner("BACKPACK")
                header.write(party=party)
                header.set_owner(None)
                self.ydata.party.backpack_moved.append(header)

        # ----------------------------------------------------------------------
        # 移動先にデータを追加する
        # ----------------------------------------------------------------------

        # 移動先がPlayerCardだった場合
        if targettype == "PLAYERCARD":
            assert not move
            assert isinstance(target, cw.character.Character)
            # cardpocketにCardHeaderを追加
            header.set_owner(target)
            header.set_hold(hold)
            # 使用回数を設定
            header.get_uselimit()
            if header.type == "SkillCard":
                if from_event:
                    header.set_uselimit(header.maxuselimit)
                elif self.is_playingscenario() and self.areaid == cw.AREA_TRADE3 and\
                        (target, header) in self.sdata.uselimit_table:
                    assert isinstance(target, cw.sprite.card.PlayerCard)
                    uselimit = self.sdata.uselimit_table[(target, header)]
                    header.set_uselimit(uselimit-header.uselimit)
            # カードのエレメントを追加
            assert header.carddata is not None
            path = "%ss" % header.type
            if toindex == -1:
                target.cardpocket[index].append(header)
                target.data.append(path, header.carddata)
            else:
                target.cardpocket[index].insert(toindex, header)
                target.data.find_exists(path).insert(toindex, header.carddata)
            # ～1.1まではDBにwsnversion列が無いため、
            # header.wsnversionがNoneの場合がある
            header.wsnversion = header.carddata.getattr(".", "dataVersion", "")

            # 戦闘中の場合、Deckの手札・山札に追加
            if cw.cwpy.is_battlestatus():
                target.deck.add(target, header, is_replace=toindex != -1)

        # 移動先が荷物袋だった場合
        elif targettype == "BACKPACK":
            # 移動先のリストにCardHeaderを追加
            assert isinstance(target, list)
            assert party
            if toindex == -1:
                header.order = cw.util.new_order(target, mode=1)
                target.insert(0, header)
            else:
                if insertorder == -1:
                    header.order = cw.util.new_order(target, mode=1)
                else:
                    header.order = insertorder
                target.insert(toindex, header)
            header.set_owner("BACKPACK")
            if sort:
                party.sort_backpack()

        # 移動先がカード置場だった場合
        elif targettype == "STOREHOUSE":
            # 移動先のリストにCardHeaderを追加
            assert isinstance(target, list)
            if toindex == -1:
                header.order = cw.util.new_order(target, mode=1)
                target.insert(0, header)
            else:
                if insertorder == -1:
                    header.order = cw.util.new_order(target, mode=1)
                else:
                    header.order = insertorder
                target.insert(toindex, header)
            header.set_owner("STOREHOUSE")
            if sort:
                self.ydata.sort_storehouse()

        # 下取りに出した場合
        elif targettype == "PAWNSHOP":
            assert not move
            # パーティの所持金または金庫に下取金を追加
            if party:
                self.exec_func(party.set_money, price, False, True)
            else:
                self.exec_func(self.ydata.set_money, price, True)
            self.exec_func(self.draw)

        if targettype in ("BACKPACK", "STOREHOUSE") and not toself:
            # 移動先が荷物袋かカード置場だったら
            if move:
                header.write(party, move=True)
                header.carddata = None
                header.flags = {}
                header.steps = {}
                header.variants = {}
            else:
                header.fpath = ""
                etree = cw.data.xml2etree(element=header.carddata)
                if not from_getcontent:
                    # 削除フラグを除去
                    if etree.getint("Property", "moved", 0) != 0:
                        etree.remove("Property", attrname="moved")
                        header.moved = 0
                header.write(party, from_getcontent=from_getcontent)
                header.carddata = None
                header.flags = {}
                header.steps = {}
                header.variants = {}

        if header == self.selectedheader:
            self.selectedheader = None
            self.update_tradecards()

        if not sort and targettype == "BACKPACK" and self.ydata.party:
            self.ydata.party.sorted_backpack_by_order = False

        # カード選択ダイアログを再び開く(イベントから呼ばれたのでなかったら)
        if not from_event and call_predlg:
            self.call_predlg()

    def remove_xml(self, target: Union["cw.character.Player", "cw.header.AdventurerHeader",
                                       "cw.header.CardHeader", cw.data.Party, str]) -> None:
        """xmlファイルを削除する。
        target: AdventurerHeader, PlayerCard, CardHeader, XMLFilePathを想定。
        """
        assert self.ydata
        if isinstance(target, cw.character.Player):
            self.ydata.deletedpaths.add(target.data.fpath)
            self.remove_materials(target.data.find_exists("Property"))
        elif isinstance(target, cw.header.AdventurerHeader):
            self.ydata.deletedpaths.add(target.fpath)
            data = cw.data.yadoxml2element(target.fpath, "Property")
            self.remove_materials(data)
        elif isinstance(target, cw.header.CardHeader):
            if target.fpath:
                self.ydata.deletedpaths.add(target.fpath)

            if target.carddata is not None:
                data = target.carddata
            else:
                data = cw.data.yadoxml2element(target.fpath)

            self.remove_materials(data)
        elif isinstance(target, cw.data.Party):
            self.ydata.deletedpaths.add(target.data.fpath)
            self.remove_materials(target.data)
        elif isinstance(target, str):
            if target.endswith(".xml"):
                self.ydata.deletedpaths.add(target)
                data = cw.data.yadoxml2element(target)
                self.remove_materials(data)

    def remove_materials(self, data: Union[cw.data.CWPyElementTree, cw.data.CWPyElement]) -> None:
        """XMLElementに記されている
        素材ファイルを削除予定リストに追加する。
        """
        assert self.ydata
        e = data.find("Property/Materials")
        if e is not None:
            path = cw.util.join_paths(self.yadodir, e.text)
            temppath = cw.util.join_paths(self.tempdir, e.text)
            if os.path.isdir(path):
                self.ydata.deletedpaths.add(path)
            if os.path.isdir(temppath):
                self.ydata.deletedpaths.add(temppath)
        else:
            # Property/Materialsが無かった頃の互換動作
            for e in data.iter():
                etext = cw.util.validate_filepath(e.text)
                if e.tag == "ImagePath" and etext and not cw.binary.image.path_is_code(etext):
                    path = cw.util.join_paths(self.yadodir, etext)
                    temppath = cw.util.join_paths(self.tempdir, etext)
                    if cw.fsync.is_waiting(path):
                        cw.fsync.sync()
                    elif cw.fsync.is_waiting(temppath):
                        cw.fsync.sync()

                    if os.path.isfile(path):
                        self.ydata.deletedpaths.add(path)

                    if os.path.isfile(temppath):
                        self.ydata.deletedpaths.add(temppath)

    def copy_materials(self, data: Union[cw.data.CWPyElement, cw.data.CWPyElementTree], dstdir: str,
                       from_scenario: bool = True, scedir: str = "", yadodir: Optional[str] = None,
                       toyado: Optional[str] = None, adventurer: bool = False,
                       imgpaths: Optional[Dict[str, str]] = None, importimage: bool = False,
                       can_loaded_scaledimage: bool = False) -> None:
        """
        from_scenario: Trueの場合は開いているシナリオから、
                       Falseの場合は開いている宿からコピーする
        XMLElementに記されている
        素材ファイルをdstdirにコピーする。
        """
        if imgpaths is None:
            imgpaths = {}

        orig_scedir = scedir
        if isinstance(data, cw.data.CWPyElementTree):
            data = data.getroot()

        r_specialfont = re.compile("#.")  # 特殊文字(#)
        if data.tag == "Property":
            prop = data
        else:
            prop = data.find_exists("Property")

        if toyado:
            yadodir2 = toyado
            dstdir2 = dstdir.replace(toyado + "/", "", 1)
        else:
            yadodir2 = self.yadodir
            dstdir2 = dstdir.replace(yadodir2 + "/", "", 1)

        if adventurer:
            mdir = ""
            emp = None
        else:
            emp = prop.find("Materials")
            if emp is None:
                mdir = ""
                e = cw.data.make_element("Materials", dstdir2)
                prop.append(e)
            else:
                if not scedir:
                    scedir = cw.util.join_yadodir(emp.text)
                mdir = emp.text
                if mdir in imgpaths:
                    emp.text = imgpaths[mdir]
                else:
                    emp.text = dstdir2
                    imgpaths[mdir] = dstdir2

        if yadodir and mdir:
            from_scenario = True
            scedir = cw.util.join_paths(yadodir, mdir)

        if not scedir and from_scenario:
            scedir = self.sdata.scedir

        for e in data.iter():
            if hasattr(e, "content"):
                e.content = None  # イベントコンテントのキャッシュは削除しておく
            if e.tag == "ImagePath" and importimage:
                # ImagePathはcarddata無しでの表示に必要となるので取り込んでおく
                etext = cw.util.validate_filepath(e.text)
                if etext and not cw.binary.image.path_is_code(etext):
                    path = cw.util.join_paths(orig_scedir, etext)
                    if os.path.isfile(path):
                        with open(path, "rb") as f:
                            imagedata = f.read()
                            f.close()
                        e.text = cw.binary.image.data_to_code(imagedata)
            elif e.tag in ("ImagePath", "SoundPath", "SoundPath2"):
                path = cw.util.validate_filepath(e.text)
                if path:
                    if yadodir and mdir:
                        path = cw.util.relpath(path, mdir)

                    def set_material(text: str) -> None:
                        e.text = text
                    self._copy_material(data, dstdir, from_scenario, scedir, imgpaths, e, path, set_material, yadodir,
                                        toyado, can_loaded_scaledimage=can_loaded_scaledimage)
            elif e.tag in ("Play", "Talk"):
                path = cw.util.validate_filepath(e.get("path"))
                if path:
                    if yadodir and mdir:
                        path = cw.util.relpath(path, mdir)

                    def set_material(text: str) -> None:
                        e.attrib["path"] = text
                    self._copy_material(data, dstdir, from_scenario, scedir, imgpaths, e, path, set_material, yadodir,
                                        toyado, can_loaded_scaledimage=can_loaded_scaledimage)
            elif e.tag == "Text" and e.text:
                for spchar in r_specialfont.findall(e.text):
                    c = "font_" + spchar[1:]

                    def set_material(text: str) -> None:
                        pass
                    for ext in cw.EXTS_IMG:
                        self._copy_material(data, dstdir, from_scenario, scedir, imgpaths, e, c + ext, set_material,
                                            yadodir, toyado, can_loaded_scaledimage=can_loaded_scaledimage)

            elif e.tag == "Effect":
                path = cw.util.validate_filepath(e.get("sound"))
                if path:
                    if yadodir and mdir:
                        path = cw.util.relpath(path, mdir)

                    def set_material(text: str) -> None:
                        e.attrib["sound"] = text
                    self._copy_material(data, dstdir, from_scenario, scedir, imgpaths, e, path, set_material, yadodir,
                                        toyado, can_loaded_scaledimage=can_loaded_scaledimage)

            elif e is not data and e.tag == "BeastCard" and from_scenario:
                self.sdata.copy_carddata(e, dstdir, from_scenario, scedir, imgpaths)

    def _copy_material(self, data: cw.data.CWPyElement, dstdir: str, from_scenario: bool, scedir: str,
                       imgpaths: Dict[str, str], e: Optional[cw.data.CWPyElement], materialpath: str,
                       set_material: Callable[[str], None], yadodir: Optional[str], toyado: Optional[str],
                       can_loaded_scaledimage: bool) -> None:
        pisc = e is not None and e.tag == "ImagePath" and cw.binary.image.path_is_code(materialpath)
        if pisc:
            imgpath = materialpath
        else:
            if from_scenario:
                if not scedir:
                    scedir = self.sdata.scedir
                imgpath = cw.util.join_paths(cw.tempdir, "ScenarioLog/TempFile", materialpath)
                if cw.fsync.is_waiting(imgpath):
                    cw.fsync.sync()
                if not os.path.isfile(imgpath):
                    imgpath = cw.util.join_paths(scedir, materialpath)
                    if not os.path.isfile(self.rsrc.get_filepath(imgpath)):
                        return
            elif yadodir:
                imgpath = cw.util.join_paths(yadodir, materialpath)
            else:
                imgpath = cw.util.join_yadodir(materialpath)
            if not yadodir:
                imgpath = cw.util.get_materialpathfromskin(imgpath, cw.M_IMG)

            # 吉里吉里形式音声ループ情報
            sli = imgpath + ".sli"
            if not os.path.isfile(sli):
                sli = ""

        if not (pisc or os.path.isfile(imgpath)):
            return

        # Jpy1から参照しているイメージを再帰的にコピーする
        if from_scenario and cw.util.splitext(imgpath)[1].lower() == ".jpy1":
            try:
                config = cw.effectbooster.EffectBoosterConfig(imgpath, "init")
                dirdepth = config.get_int("init", "dirdepth", 0)
                for section in config.sections():
                    jpy1innnerfile = config.get(section, "filename", "")
                    if not jpy1innnerfile:
                        continue
                    dirtype = config.get_int(section, "dirtype", 1)
                    innerfpath = cw.effectbooster.get_filepath_s(config.path, dirdepth, jpy1innnerfile, dirtype,
                                                                 scedir=scedir)[0]
                    if not innerfpath.startswith(scedir + "/"):
                        continue
                    innerfpath = innerfpath.replace(scedir + "/", "", 1)

                    def func(text: str) -> None:
                        pass
                    self._copy_material(data, dstdir, from_scenario, scedir, imgpaths, None, innerfpath, func, yadodir,
                                        toyado, can_loaded_scaledimage=can_loaded_scaledimage)

            except Exception:
                cw.util.print_ex()

        # 重複チェック。既に処理しているimgpathかどうか
        keypath = imgpath
        if yadodir:
            keypath = cw.util.relpath(keypath, yadodir)
        if not pisc and keypath in imgpaths:
            # ElementTree編集
            set_material(imgpaths[keypath])
        else:
            # 対象画像のコピー先を作成
            if pisc:
                idata = cw.binary.image.code_to_data(imgpath)
                ext = cw.util.get_imageext(idata)
                dname = cw.util.repl_dischar(data.gettext("Property/Name", "simage")) + ext
            elif from_scenario:
                dname = materialpath
            else:
                dname = os.path.basename(imgpath)
            imgdst = cw.util.join_paths(dstdir, dname)
            imgdst = cw.util.dupcheck_plus(imgdst, yado=not yadodir)

            if not yadodir and imgdst.startswith("Yado"):
                imgdst = imgdst.replace(self.yadodir, self.tempdir, 1)

            # 対象画像コピー
            if not os.path.isdir(os.path.dirname(imgdst)):
                os.makedirs(os.path.dirname(imgdst))

            if pisc:
                imgdst = cw.util.dupcheck_plus(imgdst, False)
                cw.util.write_file(imgdst, idata, cw.fsync)
            else:
                cw.util.copy_scaledimagepaths(imgpath, imgdst, can_loaded_scaledimage)
                if sli:
                    shutil.copy2(sli, imgdst + ".sli")
            # ElementTree編集
            if yadodir:
                assert toyado is not None
                materialpath = imgdst.replace(toyado + "/", "", 1)
            else:
                materialpath = imgdst.replace(self.tempdir + "/", "", 1)
            set_material(materialpath)
            if not pisc:
                # 重複して処理しないよう辞書に登録
                imgpaths[keypath] = materialpath

# ------------------------------------------------------------------------------
# 状態取得用メソッド
# ------------------------------------------------------------------------------

    def is_running(self) -> bool:
        """CWPyスレッドがアクティブかどうかbool値を返す。
        アクティブでない場合は、CWPyRunningErrorを投げて、
        CWPyスレッドを終了させる。
        """
        if not self._running or cw.quit_app:
            if threading.currentThread() == self:
                raise CWPyRunningError()

        return self._running or cw.quit_app

    def is_runningstatus(self) -> bool:
        return self._running

    def is_playingscenario(self) -> bool:
        return bool(isinstance(self.sdata, cw.data.ScenarioData)
                    and self.sdata.is_playing and self.ydata and self.ydata.party)

    def is_runningevent(self) -> bool:
        return bool(self.event.get_event() or
                    self.event.get_effectevent() or
                    pygame.event.peek(USEREVENT) or
                    (self.is_battlestatus() and not (self.battle and self.battle.is_ready())) or
                    self.is_decompressing or self._elapse_time)

    def is_reloading(self) -> bool:
        return self._reloading

    def is_statusbarmask(self) -> bool:
        return bool(cw.cwpy.setting.statusbarmask and cw.cwpy.is_playingscenario() and
                    not self.is_processing and self.ydata and self.ydata.party and not self.ydata.party.is_loading())

    @synclock(_dlg_mutex)
    def is_showingdlg(self) -> bool:
        return 0 < self._showingdlg

    def is_expanded(self) -> bool:
        return self.setting.is_expanded

    def is_curtained(self) -> bool:
        return self._curtained

    def is_dealing(self) -> bool:
        return self._dealing

    def is_autospread(self) -> bool:
        return self._autospread

    def is_gameover(self) -> bool:
        if self.is_playingscenario() and not self._forcegameover:
            self._gameover = True
            pcards = self.get_pcards("unreversed")
            for pcard in pcards:
                if pcard.is_alive():
                    self._gameover = False
                    break
            self._gameover |= not bool(pcards)

        return self._gameover

    def is_forcegameover(self) -> bool:
        return self._forcegameover

    def is_showingmessage(self) -> bool:
        return bool(self.get_messagewindow())

    def is_showingdebugger(self) -> Optional["cw.debug.debugger.Debugger"]:
        return self.frame.debugger

    def is_showingbacklog(self) -> bool:
        return self._is_showingbacklog

    def is_debugmode(self) -> bool:
        return self.debug

    def is_battlestatus(self) -> bool:
        """現在のCWPyのステータスが、シナリオバトル中かどうか返す。
        if cw.cwpy.battle:と使い分ける。
        """
        return cw.cwpy.is_playingscenario() and self.status == "ScenarioBattle"

# ------------------------------------------------------------------------------
# 各種スプライト取得用メソッド
# ------------------------------------------------------------------------------

    def get_inusecardimg(self) -> Optional["cw.sprite.background.InuseCardImage"]:
        """InuseCardImageインスタンスを返す(使用カード)。"""
        if self.inusecards:
            return self.inusecards[0]
        else:
            return None

    def get_guardcardimg(self) -> Optional["cw.sprite.background.InuseCardImage"]:
        """InuseCardImageインスタンスを返す(防御・回避ボーナスカード)。"""
        if self.guardcards:
            return self.guardcards[0]
        else:
            return None

    def get_messagewindow(self) -> Optional["cw.sprite.message.MessageWindow"]:
        """MessageWindow or SelectWindowインスタンスを返す。"""
        sprites = self.cardgrp.get_sprites_from_layer(cw.layer_val(cw.LAYER_MESSAGE))
        if sprites:
            mwin = sprites[0]
            assert isinstance(mwin, cw.sprite.message.MessageWindow)
            return mwin
        sprites = self.cardgrp.get_sprites_from_layer(cw.layer_val(cw.LAYER_SPMESSAGE))
        if sprites:
            mwin = sprites[0]
            assert isinstance(mwin, cw.sprite.message.MessageWindow)
            return mwin
        return None

    @typing.overload
    def get_mcards(self, mode: Literal["", "visible", "invisible", "visiblemenucards", "selectable"] = "",
                   flag: str = "") ->\
        List[Union["cw.sprite.card.MenuCard", "cw.sprite.card.EnemyCard", "cw.sprite.card.FriendCard"]]: ...

    @typing.overload
    def get_mcards(self, mode: Literal["flagtrue"], flag: str = "") ->\
        List[Union["cw.sprite.card.MenuCard", "cw.sprite.card.EnemyCard"]]: ...

    def get_mcards(self, mode: Literal["", "visible", "invisible", "visiblemenucards", "flagtrue", "selectable"] = "",
                   flag: str = "") -> Union[List[Union["cw.sprite.card.MenuCard", "cw.sprite.card.EnemyCard",
                                                       "cw.sprite.card.FriendCard"]],
                                            List[Union["cw.sprite.card.MenuCard", "cw.sprite.card.EnemyCard"]]]:
        """MenuCardインスタンスのリストを返す。
        mode: "visible" or "invisible" or "visiblemenucards" or "flagtrue" or "selectable"
        """
        if mode == "visible":
            mcards = [m for m in self.get_mcards(flag=flag) if not m.status == "hidden"]
        elif mode == "invisible":
            mcards = [m for m in self.get_mcards(flag=flag) if m.status == "hidden"]
        elif mode == "visiblemenucards":
            mcards = [m for m in self.get_mcards(flag=flag) if not m.status == "hidden"
                      and isinstance(m, cw.sprite.card.MenuCard)]
        elif mode == "flagtrue":
            mcards = [m for m in self.get_mcards(flag=flag)
                      if not isinstance(m, cw.character.Friend)
                      and m.is_flagtrue()]
        elif mode == "selectable":
            if self.is_battlestatus():
                mcards = []
                mcards.extend(self.get_ecards("selectable"))
                mcards.extend(self.get_fcards("selectable"))
            else:
                mcards = self.get_mcards("visible")
                if not self.is_debugmode():
                    mcards = [m for m in mcards
                              if not (isinstance(m, cw.character.Friend) and m.is_reversed())]
        elif flag:
            defvalue: List[Union[cw.sprite.card.EnemyCard, cw.sprite.card.MenuCard]] = []
            return self._mcardtable.get(flag, defvalue)
        else:
            if self.is_battlestatus() and self.battle and self.battle.is_running():
                # 戦闘行動中はNPCを除外(一時的に表示されている可能性があるため)
                mcards = [m for m in self.mcards
                          if not isinstance(m, (cw.character.Friend, cw.sprite.background.InuseCardImage))]
            else:
                mcards = [m for m in self.mcards
                          if not isinstance(m, cw.sprite.background.InuseCardImage)]

        return mcards

    def get_ecards(self,
                   mode: Literal["", "unreversed", "active", "selectable"] = "") -> List["cw.sprite.card.EnemyCard"]:
        """現在表示中のEnemyCardインスタンスのリストを返す。
        mode: "unreversed" or "active"
        """
        if not self.is_battlestatus():
            return []

        ecards: List["cw.sprite.card.EnemyCard"] = [m for m in self.mcards
                                                    if isinstance(m, cw.sprite.card.EnemyCard) and
                                                    not m.status == "hidden"]
        if mode == "unreversed":
            ecards = [ecard for ecard in ecards if not ecard.is_reversed()]
        elif mode == "active":
            ecards = [ecard for ecard in ecards if ecard.is_active()]
        elif mode == "selectable":
            if self.is_debugmode():
                ecards = cw.cwpy.get_ecards()
            else:
                ecards = cw.cwpy.get_ecards("unreversed")

        ecards = [ecard for ecard in ecards if isinstance(ecard, cw.character.Enemy)]

        return ecards

    def get_pcards(self,
                   mode: Literal["", "unreversed", "active", "selectable"] = "") -> List["cw.sprite.card.PlayerCard"]:
        """PlayerCardインスタンスのリストを返す。
        mode: "unreversed" or "active" or "selectable"
        """
        if mode == "unreversed":
            pcards = [pcard for pcard in self.get_pcards() if not pcard.is_reversed()]
        elif mode == "active":
            pcards = [pcard for pcard in self.get_pcards() if pcard.is_active()]
        elif mode == "selectable":
            if (self.is_debugmode() and not self.selectedheader) or (self.setting.show_personal_cards and
                                                                     self.areaid == cw.AREA_CAMP):
                pcards = self.get_pcards()
            else:
                pcards = self.get_pcards("unreversed")
        else:
            pcards = self.pcards
            pcards = [m for m in pcards
                      if not isinstance(m, (cw.character.Friend, cw.sprite.background.InuseCardImage))]

        return pcards

    def get_fcards(self,
                   mode: Literal["", "unreversed", "active", "selectable"] = "") -> List["cw.sprite.card.FriendCard"]:
        """FriendCardインスタンスのリストを返す。
        シナリオプレイ中以外は空のリストを返す。
        mode: "unreversed" or "active"
        """
        if not self.is_playingscenario():
            return []

        fcards = self.sdata.friendcards
        if mode == "unreversed":
            fcards = [fcard for fcard in fcards if not fcard.is_reversed()]
        elif mode == "active":
            fcards = [fcard for fcard in fcards if fcard.is_active()]
        elif mode == "selectable":
            if self.is_debugmode():
                fcards = self.get_fcards()
            else:
                fcards = self.get_fcards("unreversed")

        return fcards

    def find_backpackcard(self) -> Optional["cw.sprite.card.MenuCard"]:
        """画面上に荷物袋のカードがあれば返す。"""
        for mcard in self.get_mcards("visiblemenucards"):
            assert isinstance(mcard, cw.sprite.card.MenuCard)
            if mcard.is_backpack():
                return mcard
        else:
            return None

    def find_storehousecard(self) -> Optional["cw.sprite.card.MenuCard"]:
        """画面上にカード置場のカードがあれば返す。"""
        for mcard in self.get_mcards("visiblemenucards"):
            assert isinstance(mcard, cw.sprite.card.MenuCard)
            if mcard.is_storehouse():
                return mcard
        else:
            return None


_mutex_postevent = threading.Lock()


@synclock(_mutex_postevent)
def post_pygameevent(event: pygame.event.Event) -> None:
    """pygameイベントをキューへ投入する。
    投入に失敗した場合は一度だけ入力イベントを
    クリアしてからの再投入を試みる。
    """
    try:
        pygame.event.post(event)
    except Exception:
        try:
            # 入力イベントが輻輳している場合はクリアする
            cw.cwpy.clear_inputevents()
            pygame.event.post(event)
        except Exception:
            return


class StoredParty(object):
    def __init__(self, party: cw.data.Party) -> None:
        self.fpath = ""
        self.name = party.name
        self.money = party.money
        self.members = party.members[:]
        self.backpack = party.backpack[:]
        self.is_suspendlevelup = party.is_suspendlevelup
        cw.util.sort_by_attr(self.backpack, "order")


def main() -> None:
    pass


if __name__ == "__main__":
    main()
