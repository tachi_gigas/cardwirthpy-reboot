#!/usr/bin/env python
# -*- coding: utf-8 -*-

from . import debugger
from . import edit
from . import cardedit
from . import charaedit
from . import statusedit
from . import recording
from . import event
from . import logging
from . import selectedcard

__all__ = ["debugger", "edit", "cardedit", "charaedit", "statusedit", "recording", "event", "logging", "selectedcard"]


def main() -> None:
    pass


if __name__ == "__main__":
    main()
