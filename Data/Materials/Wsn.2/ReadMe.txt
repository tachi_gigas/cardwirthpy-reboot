
Wsn.2標準効果音
========================================

`Sound`及び`BgmAndSound`フォルダに、Wsn.2対応エンジンで必ず使用可能な効果音が入っています。

これらのデータは全てハルキゲニア(@tekitoudesu)により作成・提供されました。ライセンスは[CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.ja)で、パブリックドメイン同様自由に扱う事ができます。`CC0.txt`も参照してください。

ファイル名は、提供されたものから以下のように変更されています。

 * `1s_お知らせ01.ogg` → `s_information_start.ogg`
 * `1s_お知らせ終了01.ogg` → `s_information_end.ogg`
 * `1s_カカン.ogg` → `s_sliding_cornice.ogg`
 * `1s_テニス.ogg` → `s_hitting_tennis_ball.ogg`
 * `1s_仏壇.ogg` → `s_bell_of_buddhist_altar.ogg`
 * `1s_入店.ogg` → `s_chime_of_store.ogg`
 * `1s_擬音03.ogg` → `s_soft.ogg`
 * `1s_攻撃02.ogg` → `s_attack_1.ogg`
 * `1s_攻撃03.ogg` → `s_attack_2.ogg`
 * `1s_攻撃04.ogg` → `s_attack_3.ogg`
 * `1s_攻撃05.ogg` → `s_attack_4.ogg`
 * `1s_楽器（拍子木Ａ).ogg` → `l_wooden_clappers.ogg`
 * `1s_楽器（琴）.ogg` → `s_koto.ogg`
 * `1s_楽器（鼓）.ogg` → `s_hand_drum.ogg`
 * `1s_歩く（砂）.ogg` → `l_walk_on_sand.ogg`
 * `1s_決定02.ogg` → `s_decide_1.ogg`
 * `1s_決定03.ogg` → `s_decide_2.ogg`
 * `1s_疑問02.ogg` → `s_question.ogg`
 * `1s_金属バット.ogg` → `s_metal_bat.ogg`
 * `1s_閃き01.ogg` → `s_idea.ogg`
 * `lp_虫(合唱).ogg` → `l_chirr.ogg`
 * `lp_鐘Ａ.ogg` → `l_church_bell.ogg`
 * `lp_雨.ogg` → `l_rain.ogg`
 * `lp_鳥(囀り).ogg` → `l_sing.ogg`
 * `lp_滝.ogg` → `l_waterfall.ogg`
 * `lp_風01.ogg` → `l_windstorm.ogg`
 * `lp_風02.ogg` → `l_wind_1.ogg`
 * `lp_風04.ogg` → `l_wind_2.ogg`
 * `vo_あ.ogg` → `s_breath_a.ogg`
 * `vo_い.ogg` → `s_breath_i.ogg`
 * `vo_う.ogg` → `s_breath_u.ogg`
 * `vo_え.ogg` → `s_breath_e.ogg`
 * `vo_お.ogg` → `s_breath_o.ogg`
